//
//  SideMenuTableViewCell.swift
//  treegala
//
//  Created by PC on 7/30/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet private weak var lbl_MenuTitle: UILabel!
    @IBOutlet private weak var img_MenuIcon: UIImageView!
    @IBOutlet weak var btnIndicator: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK:- setupData
    func setTheData(theTitle:String,icon:UIImage) {
        lbl_MenuTitle.text = theTitle
        img_MenuIcon.image = icon
    }
    
}
