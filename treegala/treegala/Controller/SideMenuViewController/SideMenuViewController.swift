//
//  SideMenuViewController.swift
//  treegala
//
//  Created by PC on 7/30/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SideMenuViewController: ParentViewController {
    
    // MARK:- Variables & Proprties
    
    @IBOutlet weak var lblUserName: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnDashBoard: UIButton!
    
//    var titleItems = ["Search all listing nearby","Did we miss a listing?","Suggestions?","Rate Us","About","Terms & Conditions","Logout"]
    var titleItems = ["Search all listings","Did we miss a listing?","Suggestions?","Rate Us","About","Terms & Conditions","Frequently asked questions","Logout"]
    var iconItems = [#imageLiteral(resourceName: "ic_search_listing_sidemenu"),#imageLiteral(resourceName: "ic_add_missing_listing_sidemenu.png"),#imageLiteral(resourceName: "ic_suggestions_sidemenu.png"),#imageLiteral(resourceName: "ic_rate_us_sidemenu.png"),#imageLiteral(resourceName: "ic_obstacles_event_list_category"),#imageLiteral(resourceName: "ic_terms_conditions"),#imageLiteral(resourceName: "ic_question_mark"),#imageLiteral(resourceName: "ic_logout_sidemenu")]
    
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        
        btnDashBoard.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(redirectToMyListPage), name: NSNotification.Name.init("redirectHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadHomeViewController), name: NSNotification.Name.init("loadHome"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isUserLogin() {
            lblUserName.setTitle(getUserDetail("username"), for: .normal)
            btnDashBoard.isHidden = false
        } else {
            lblUserName.setTitle("Login or Signup", for: .normal)
            btnDashBoard.isHidden = true
        }
    }
    
    @objc func redirectToMyListPage() {
        let vc = SB.main.dashboardNavigationController()!
        let nestVC = vc.viewControllers[0] as! MyDashboardViewController
        nestVC.isMyListing = true
        self.slideMenuController()?.changeMainViewController(vc, close: true)
    }
    
    func setupUI() {
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        tableView.rowHeight = 50.0
        tableView.estimatedRowHeight = 50.0
        tableView.register(UINib.init(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableViewCell")
    }
    
    enum SideMenu: Int {
        case Home = 0
        case AddList = 1
        case Suggestion = 2
        case RateUs = 3
        case AboutUs = 4
        case Terms = 5
        case Questions = 6
        case Logout = 7
    }
    
    func navigateToViewController(_ index: Int) {
        
        switch index {
            
        case SideMenu.Home.rawValue:
            loadHomeViewController()
            
        case SideMenu.AddList.rawValue:
            let vc = SB.addListing.optionNavigationViewController()!
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case SideMenu.Suggestion.rawValue:
            loadSuggestionViewController()
            
        case SideMenu.RateUs.rawValue:
            loadRateUsViewController()
            break
            
        case SideMenu.AboutUs.rawValue:
            loadAboutUsViewController()
            
        case SideMenu.Logout.rawValue:
            break
         
        case SideMenu.Terms.rawValue:
            loadTermsConditionViewController()
            break
            
        case SideMenu.Questions.rawValue:
            btnFrequentlyQuestionAction()
            break
            
        default:
            break
        }
        
    }
    
    @IBAction func btnDashboardTapped(_ sender: UIButton) {
        let vc = SB.main.dashboardNavigationController()!
        self.slideMenuController()?.changeMainViewController(vc, close: true)
    }
    
    @IBAction func btnNameTapped(_ sender: UIButton) {
        if isUserLogin() {
            let vc = SB.main.dashboardNavigationController()!
            let nestVC = vc.viewControllers[0] as! MyDashboardViewController
            nestVC.isRedirectToProfile = true
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        } else {
            let vc = SB.signin.signinNavigationController()!
            appDelegate.configure(vc)
        }
    }
    
    // Other Methods
    @objc func loadHomeViewController() {
        let mainVC = HomeNavigationController.instantiateFromAppStoryboard(appStoryboard: .home)
        self.slideMenuController()?.changeMainViewController(mainVC, close: true)
    }
    
    func loadSuggestionViewController() {
        let suggestionVC = SB.main.suggestionNavigationController()!
        self.slideMenuController()?.changeMainViewController(suggestionVC, close: true)
    }
    
    func loadAboutUsViewController() {
        let aboutVC = SB.main.aboutUsNavigationController()!
        self.slideMenuController()?.changeMainViewController(aboutVC, close: true)
    }
    
    func loadTermsConditionViewController() {
        let termsVC = SB.main.termsConditionNavigationController()!
        self.slideMenuController()?.changeMainViewController(termsVC, close: true)
    }
    
    func btnFrequentlyQuestionAction() {
        let vc = SB.newUpdates.frequentlyAskedQuestionNavigationController()!
        self.slideMenuController()?.changeMainViewController(vc, close: true)
    }
    
    func loadRateUsViewController() {
        //https://apps.apple.com/ca/app/treegala/id1462254194
        
//        https://itunes.apple.com/us/app/itunes-u/id\(appID)?ls=1&mt=8&action=write-review
        let appID = "id1462254194"
        guard let url = URL(string: "https://itunes.apple.com/us/app/itunes-u/\(appID)?ls=1&mt=8&action=write-review") else {
            return
        }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isUserLogin() ? titleItems.count : (titleItems.count - 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        cell.btnIndicator.isHidden = selectedIndex != indexPath.row
        let color = selectedIndex == indexPath.row ? UIColor.init(red: 204.0/255.0, green: 193.0/255.0, blue: 209.0/255.0, alpha: 1.0) : nil
        cell.contentView.backgroundColor = color
        cell.setTheData(theTitle: titleItems[indexPath.row], icon: iconItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        if indexPath.row == 7 {
            
            popupAlert(title: keys.appTitle(), message: "Are you sure you want to logout?", actionTitles: ["OK","Cancel"], actions: [{ okAction in
                self.logout()
                }, { cancelAction in
                    
                }])
        } else {
            navigateToViewController(indexPath.row)
        }
        tableView.reloadData()
    }   
    
    func logout() {
        let kURL = webUrls.baseUrl() + webUrls.logOutURL()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "device_token" : UIDevice.current.identifierForVendor!.uuidString]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            if json["flag"].stringValue == "1" {
                UserDefaults.standard.set(false, forKey: R.string.keys.isUserLogin())
                UserDefaults.standard.removeObject(forKey: R.string.keys.userDetail())
                UserDefaults.standard.removeObject(forKey: "locationName")
                self.selectedIndex = 0
                self.tableView.reloadData()
                print("isUserLogin => \(isUserLogin())")
                self.slideMenuController()?.toggleLeft()
                self.loadHomeViewController()
            }
        }
    }
}

class SideMenuNavigationController: UINavigationController {
    
}
