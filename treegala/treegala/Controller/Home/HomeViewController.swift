//
//  HomeViewController.swift
//  treegala
//
//  Created by PC on 7/30/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import DropDown
import CoreLocation
import Alamofire
import SwiftyJSON
//import FBAudienceNetwork
//import AdSupport

class HomeViewController: ParentViewController {
    
    @IBOutlet weak var btnSearchHeader: UIBarButtonItem!
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnSearchCategory: UIButton!
    
    @IBOutlet weak var lblDistnce: UILabel!
    @IBOutlet weak var btnDistance: UIButton!
    
    @IBOutlet weak var btnNightLife: UIButton!
    @IBOutlet weak var btnFestival: UIButton!
    @IBOutlet weak var btnEvent: UIButton!
    
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet weak var btnUpComing: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var heightOfVwAds: NSLayoutConstraint!
    @IBOutlet weak var vwAds: UIView!
    
    //MARK:-Variable Declaration
    
    var disposeBag = DisposeBag()
    
    var distanceDD = DropDown()
    var distanceType = "Km"
    var distanceItems = [10,25,50,100]
    
    var selectedPlace: GMSPlace?
    
    let locationManager = CLLocationManager()
    
    var filterData = FilterData()
    
    var isSearchBarHidden = true {
        didSet {
            self.view.endEditing(isSearchBarHidden)
            navigationItem.titleView = isSearchBarHidden ? nil : searchBar
            navigationItem.titleView?.tintColor = UIColor.black
            btnSearchHeader.image = isSearchBarHidden ? #imageLiteral(resourceName: "ic_search_white") : #imageLiteral(resourceName: "ic_cancel_large")
        }
    }
    
//    var nativeAd : FBAdView!

    //MARK:- Viewlife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        distanceDD.dataSource = distanceItems.map { "\($0) \(distanceType)" }
        configDropdown(dropdown: distanceDD, sender: btnDistance)
        
        setupUI()
        
        setObserver()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        printFonts()

        /*nativeAd = FBAdView.init(placementID: "YOUR_PLACEMENT_ID", adSize: kFBAdSizeHeight250Rectangle, rootViewController: self)
        nativeAd.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        nativeAd.delegate = self
        nativeAd.loadAd()
        self.vwAds.addSubview(nativeAd)
        
        print("Ads uuidString \(ASIdentifierManager.shared().advertisingIdentifier.uuidString)")
        print("Ads Id \(ASIdentifierManager.shared().advertisingIdentifier)")*/
    }

    
    func setupUI() {
        setSideMenu()

        isSearchBarHidden = true
        txtLocation.delegate = self
        searchBar.placeholder = "Search"
        searchBar.delegate = self

        setLocationData()
        
        if let textField = self.searchBar.subviews.first?.subviews.flatMap({ $0 as? UITextField }).first {
            textField.subviews.first?.isHidden = true
            textField.textColor = .black
            textField.layer.backgroundColor = UIColor.white.cgColor
            textField.layer.cornerRadius = 5
            textField.layer.masksToBounds = true
        }
    }
    
    func setLocationData() {
        if getUserDetail("is_location_flag") == "1" {
        
//            print(UserDefaults.standard.value(forKey: "locationName") as? String)
//            if let name = (UserDefaults.standard.value(forKey: "locationName") as? String) {
//                txtLocation.text =  name
//            }
            
            guard let data = UserDefaults.standard.value(forKey: isUserLogin() ? R.string.keys.userDetail() : R.string.keys.guestDetail()) as? Data else { return }
            
            let json = JSON(data)
            
            if userPickedLocation == nil {
                startLoader()
                GMSPlacesClient.shared().lookUpPlaceID(json["place_id"].stringValue, callback: { (place, error) in
                    self.dismissLoader()
                    if let place = place {
                        if self.selectedPlace == nil {
                            self.selectedPlace = place
                            self.filterData.selectedPlace = place
                            self.txtLocation.text =  place.name
                        }
                        
                        UserDefaults.standard.set(place.name, forKey: "locationName")
                        userPickedLocation = place
                        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                            self.filterData.distanceType = "1"
                        } else {
                            self.filterData.distanceType = "0"
                        }
                    }
                })
            } else {
                let place = userPickedLocation!
                
                if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                    self.filterData.distanceType = "1"
                } else {
                    self.filterData.distanceType = "0"
                }
                
                if self.selectedPlace == nil {
                    self.selectedPlace = place
                    self.filterData.selectedPlace = place
                    self.txtLocation.text =  place.name
                }
//                self.filterData.selectedPlace = place
//                self.selectedPlace = place
//                print(userPickedLocation!)
//                self.txtLocation.text = place.name
            }
        } else if getUserDetail("is_location_flag") == "3" {
            selectedPlace = nil
            txtLocation.text = "Enter Location"
        }
    }
    
    func setObserver() {
        btnSearchCategory.rx
            .tap
            .subscribe(onNext: {
                let vc = SB.home.categoryListPopupViewController()!
                vc.tempCategory = self.filterData.category
                vc.selectedCategory.subscribe(onNext: {
                    print($0)
                    self.filterData.category = $0
                    
                    let categoriesArray = $0.map { $0.subCategories.filter { $0.isSelected }.map { $0.name } }
                    let categories = categoriesArray.filter { !$0.isEmpty }.map { $0.joined(separator: ", ") }.joined(separator: ", ")
                    self.lblCategory.text = "\(categories.isEmpty ? "Search Everything" : categories)"
                    
                }).disposed(by: self.disposeBag)
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                
                self.present(vc, animated: true, completion: nil)
            }).disposed(by: disposeBag)
        
        btnDistance.rx
            .tap
            .subscribe(onNext: {
                self.distanceDD.show()
                self.distanceDD.selectionAction = { (index, item) in
                    self.lblDistnce.text = item + " Km"
                }
            }).disposed(by: disposeBag)
        
        btnSearch.rx.tap.subscribe(onNext: {
            
            if Rechability.isConnectedToNetwork() {
                self.validateLocation()
                let vc = SB.home.eventListViewController()!
                self.filterData.mainCategory = ""
                vc.filterData = self.filterData
               // self.filterData = FilterData()
                self.setLocationData()
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                self.showAlert(keys.noInternet())
                return
            }
        }).disposed(by: disposeBag)
        
        btnUpComing.rx.tap.subscribe(onNext: {
            let vc = SB.home.eventListViewController()!
            self.filterData.isUpcomingEvent = "1"
            vc.filterData = self.filterData
            self.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    @IBAction func btnSearchTapped(_  sender: UIBarButtonItem) {
        searchBar.text = ""
        isSearchBarHidden = !isSearchBarHidden
    }
    
    @IBAction func btnMainCategoryTapped(_ sender: UIButton) {
        
        self.validateLocation()
       // self.setLocationData()
        
        let vc = SB.home.eventListViewController()!
        filterData.isMainCategorySelected = true
        filterData.selectedPlace = self.selectedPlace
        if sender.tag == 0 {
            //Nightlife
            filterData.mainCategory = "1"
        } else if sender.tag == 1 {
            // Festival
            filterData.mainCategory = "2"
        } else {
            // Events
            filterData.mainCategory = "3"
        }
        vc.filterData = self.filterData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validateLocation() {
        
        print("Location Stategey")
        
        if getUserDetail("is_location_flag") == "3" && self.selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        } else if getUserDetail("is_location_flag") == "2" && self.selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        } else if getUserDetail("is_location_flag") == "1" && self.selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        }
        
        if userLocation != nil {
            self.filterData.latitude = userLocation?.coordinate.latitude
            self.filterData.longitude = userLocation?.coordinate.longitude
        }
        
//        if self.selectedPlace == nil && userLocation == nil {
//            self.popupAlert(title: "Treegala", message: "Please Turn on the location or select the location", actionTitles: ["Pick Place", "Setting"], actions: [ { action1 in
//                self.txtLocation.becomeFirstResponder()
//                }, { action2 in
//                    guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
//                        return
//                    }
//                    if #available(iOS 10.0, *) {
//                        UIApplication.shared.open(urlGeneral, options: [:], completionHandler: nil)
//                    } else {
//                        UIApplication.shared.openURL(urlGeneral)
//                    }
//                }])
//            return
//        }
    }
    
}

extension HomeViewController: UITextFieldDelegate, UISearchBarDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchBarHidden = true
        self.txtLocation.text = searchBar.text ?? ""
        let vc = SB.home.eventListViewController()!
        filterData.searchText = searchBar.text ?? ""
        self.filterData.latitude = userLocation?.coordinate.latitude
        self.filterData.longitude = userLocation?.coordinate.longitude
        vc.filterData = self.filterData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .notDetermined || status == .denied {
            locationManager.requestWhenInUseAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("location manager")
        if let location = locations.first {
            userLocation = location
            if getUserDetail("is_location_flag") == "2" || !isUserLogin() {
                getPlaceId(of: location)
            }
            locationManager.stopUpdatingLocation()
        }
    }
    
    func getPlaceId(of location: CLLocation) {
        
       let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=\(webUrls.googleAPIKey())"
        startLoader()
        request(url).responseSwiftyJSON { (response) in
            self.dismissLoader()
            if let json = response.result.value {
                print(json)
                if let item = json["results"].arrayValue.first {
                    print("Current Place Id")
                    for i in item["address_components"].arrayValue {
                        if i["types"].arrayValue.contains("country") {
                            if i["long_name"].stringValue == "United Status" {
                                self.filterData.distanceType = "1"
                                print("United State Country")
                            } else {
                                self.filterData.distanceType = "0"
                                print("Other Country")
                            }
                        }
                        
                        if i["types"].arrayValue.contains("locality") {
                           self.filterData.locationCity = i["long_name"].stringValue
                           self.txtLocation.text = i["long_name"].stringValue
                        }
                    }
                    self.filterData.currentPlaceID = item["place_id"].stringValue
                    self.getPlaceFromID(item["place_id"].stringValue)
                }
            }
        }
    }
    
    func getPlaceFromID(_ placeId: String) {
        GMSPlacesClient.shared().lookUpPlaceID(placeId, callback: { (place, error) in
            if let place = place {
                print(place.name)
                self.selectedPlace = place
                self.filterData.selectedPlace = place
            }
        })
    }
}

extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white

        self.selectedPlace = place
       
        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "United States" }) {
            filterData.distanceType = "1"
        } else {
            filterData.distanceType = "0"
        }
        filterData.selectedPlace = place
        txtLocation.text =  place.name
        filterData.locationCity = place.name ?? ""
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}


class HomeNavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
}

/*extension HomeViewController:FBAdViewDelegate{
    
    func adView(_ adView: FBAdView, didFailWithError error: Error) {
        print("error \(error)")
        self.heightOfVwAds.constant = 0
    }
    
    func adViewDidLoad(_ adView: FBAdView) {
        print("load")
        self.heightOfVwAds.constant = 50
    }
}*/
