    //
    //  EventListViewController.swift
    //  treegala
    //
    //  Created by om on 8/7/18.
    //  Copyright © 2018 PC. All rights reserved.
    //
    
    import UIKit
    import RxSwift
    import SwiftyJSON
    import MXScroll
//    import FBAudienceNetwork
    import AdSupport
    
    enum checkControllerID : String {
        case all = "0"
        case nightclub = "1"
        case allFestival = "2"
        case music = "3"
        case streetFestivals = "4"
        case enduranceEvents = "5"
        case localEvents = "6"
        case upcomingEvents = "7"
    }
    
    class EventListViewController: UIViewController {
        
        @IBOutlet weak var tableView: UITableView!
        
        @IBOutlet weak var btnMap: UIButton!
        @IBOutlet weak var btnFilter: UIButton!
        
        @IBOutlet weak var txtSearch: UITextField!
        
        var btnSearchHeader: UIBarButtonItem!
        lazy var searchBar = UISearchBar(frame: CGRect.zero)
        
        var disposeBag = DisposeBag()
        
        var offset = 0
        var isPullRefresh = false
        
        var filterData: FilterData!
        var filterJSON: JSON?
        
        var eventList: [JSON]?
        var errorMessage = ""
        var eventType: EventType = .nightClub
        var isLoadedApi = true
        
        
        var isSearchBarHidden = true {
            didSet {
                
                self.view.endEditing(isSearchBarHidden)
                btnSearchHeader.image = isSearchBarHidden ? #imageLiteral(resourceName: "ic_search_white") : #imageLiteral(resourceName: "ic_cancel_large")
                navigationItem.titleView = isSearchBarHidden ? setLogoTitle() : searchBar
            }
        }
        
        var spinner = UIActivityIndicatorView()
        
        let refreshControl = UIRefreshControl()
        
        var paramDict: [String : String] = [:]
        var categoryId = checkControllerID.all
        var isFilterData = false
        
        var selectedGenreType: [JSON] = [] {
            didSet {
//                controls.genresCollection.reloadData()
            }
        }
        
        let adRowStep = 5
//        var adsManager: FBNativeAdsManager!
//        var adsCellProvider: FBNativeAdTableViewCellProvider!

        
        //MARK:- ViewLifeCycle
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setupUI()
            
            btnFilter.rx.tap.subscribe(onNext: {
                let vc = SB.home.eventFilterViewController()!
                vc.selectedCategory = self.filterData.category
                vc.filterData.lgbtqType = self.filterData.lgbtqType
                vc.filterData.free = self.filterData.free
                vc.filterData.isFamilyFriendly = vc.filterData.isFamilyFriendly
                vc.filterData.genreType = self.filterData.festivalType
                
                if setGlobalFilterCheckMark {
                    setGlobalFilterCheckMark = false
                    
                    self.filterJSON = JSON()
                    self.filterJSON?["is_family_friendly"].stringValue = self.filterData.isFamilyFriendly
                    self.filterJSON?["is_free"].stringValue = self.filterData.free
//                    self.filterJSON?["filter_data"].stringValue = self.fil
                    
                    var prm = JSON()
                    prm["event_type"].stringValue = "\(self.filterData.eventType)"
                    prm["difficulty_level"].stringValue = "\(self.filterData.isDifficultyLevel)"
                    prm["prize_award"].stringValue = "\(self.filterData.prizeAward)"
                    prm["genres"].stringValue = "\(self.filterData.eventType)"
                    prm["lgbtq"].stringValue = "\(self.filterData.lgbtqType)"
                    prm["festival_type"].stringValue = "\(self.filterData.eventType)"
                    
                    self.filterJSON?["filter_data"].stringValue = prm.rawString() ?? ""
                    print("PARAM:", self.filterJSON)
//                    self.filterData.isLGBTQ = self.filterData.lgbtqType == "0" ? false : true
//                    self.filterData.genreType = self.filterData.eventType
                }
                
                vc.filterJSON = self.filterJSON ?? JSON()
                vc.filterObserver.subscribe(onNext: { (filter) in
                    print(filter)
                    self.isFilterData = true
                    self.offset = 0
                    self.filterJSON = filter
                    self.getEventList(filterParam: filter)
                }).disposed(by: self.disposeBag)
                vc.dateFilter = { firstDate, lastDate in
                    self.filterData.toDateStr = lastDate
                    self.filterData.fromDateStr = firstDate
                }
                vc.filtercategory.subscribe(onNext: { (category) in
                    self.filterData.category = category
                }).disposed(by: self.disposeBag)
                
                vc.resetFilter.subscribe(onNext: {
                    self.filterJSON = nil
                    self.paramDict["filter_data"] = ""
                    self.getEventList(filterParam: self.filterJSON)
                }).disposed(by: self.disposeBag)
                
//                if self.categoryId == .nightclub {
//                    vc.selectedGenreType = self.selectedGenreType
//                }
//                vc.selectedGenreType = self.selectedGenreType
                
                vc.filterData.eventType = self.filterData.eventType
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            }).disposed(by: disposeBag)
            
            btnMap.rx.tap.subscribe(onNext: {
                let vc = SB.home.eventMapViewController()!
                vc.paramObserver.subscribe(onNext: { (param) in
                    print("From Map Param")
                    self.paramDict = param
                    self.offset = 0
                    let str = param["filter_data"]
                    self.filterJSON = str?.data(using: String.Encoding.utf8).flatMap({try? JSON(data: $0)})
                    self.getEventList(filterParam: self.filterJSON)
                }).disposed(by: self.disposeBag)
                vc.filterData = self.filterData
                vc.filterJSON = self.filterJSON ?? JSON()
                vc.offset = self.offset
                vc.param = self.paramDict
                vc.eventList = self.eventList ?? []
                self.navigationController?.pushViewController(vc, animated: true)
            }).disposed(by: disposeBag)
            
            refreshControl.tintColor = .white
            refreshControl.addTarget(self, action: #selector(refreshUpdates(_:)), for: .valueChanged)
            if #available(iOS 10.0, *) {
                tableView.refreshControl = refreshControl
            } else {
                tableView.addSubview(refreshControl)
            }

            tableView.register(UINib(nibName: "AdsCell", bundle: nil), forCellReuseIdentifier: "AdsCell")
            getEventList(filterParam: filterJSON)
            
            /*adsManager = FBNativeAdsManager(placementID: "YOUR_PLACEMENT_ID", forNumAdsRequested: 5)
            adsManager.delegate = self
            adsManager.loadAds()*/
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            if searchBar.text != "" {
                 isSearchBarHidden = false
            }
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            filterData.searchText = ""
            if self.isMovingFromParentViewController {
                //filterData.selectedPlace = nil
            }
            navigationItem.titleView = setLogoTitle()
        }
        
        func setLogoTitle() -> UIButton {
            
            let button =  UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
            button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
            button.addTarget(self, action: #selector(self.logoClick), for: .touchUpInside)
            return button
        }
        
        @objc func logoClick() {
            
            self.navigationController?.popToRootViewController(animated: false)
        }
        
        @objc private func refreshUpdates(_ sender: Any) {
            
            guard eventList != nil else {
                refreshControl.endRefreshing()
                return
            }
            
            offset = 0
            isPullRefresh = true
           // self.eventList = []
            getEventList(filterParam: filterJSON)
        }
        
        func setupUI() {
            
            tableView.register(nib.eventListIemCell(), forCellReuseIdentifier: "cell")
            tableView.register(nib.eventTimeListCell(), forCellReuseIdentifier: "timeCell")
            tableView.separatorStyle =  .none
            
//            spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//            spinner.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//            spinner.color = color.grayAccent()
//            spinner.hidesWhenStopped = true
//            tableView.tableFooterView = spinner
            
            txtSearch.delegate = self
            
            setSideMenu()
            
            let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search_white"), style: .plain, target: self, action: #selector(btnSearchTapped(_:)))
            btnSearchHeader = btn
            btnSearchHeader.tintColor = .white
            self.navigationItem.rightBarButtonItem = btn
            
            isSearchBarHidden = true
            searchBar.placeholder = "Search"
            searchBar.delegate = self
            
            print("Location Stategey")
            print(filterData.selectedPlace)
            print(filterData.locationCity)
            
            if filterData.selectedPlace != nil {
                //txtSearch.text = filterData.selectedPlace?.name
//                txtSearch.text = filterData.locationCity
                txtSearch.text = filterData.locationCity.isEmpty ? "Location" : filterData.locationCity
            } else {
                txtSearch.text = filterData.locationCity.isEmpty ? "Location" : filterData.locationCity
            }
            
            searchBar.tintColor = .blue
            if let textField = self.searchBar.subviews.first?.subviews.flatMap({ $0 as? UITextField }).first {
                textField.subviews.first?.isHidden = true
                textField.textColor = .black
                textField.layer.backgroundColor = UIColor.white.cgColor
                textField.layer.cornerRadius = 5
                
                textField.layer.masksToBounds = true
            }
        }
        
        @objc func btnSearchTapped(_  sender: UIBarButtonItem) {
            if !isSearchBarHidden {
                self.view.endEditing(true)
                searchBar.text = ""
                filterData.searchText = searchBar.text ?? ""
                offset = 0
                paramDict["search_title"] = filterData.searchText
                getEventList(filterParam: filterJSON)
            }
            isSearchBarHidden = !isSearchBarHidden
        }
        
        @IBAction func btnBackTapped(_ sender: UIButton) {
            searchBar.text = ""
            self.navigationController?.popToRootViewController(animated: true)
//          self.navigationController?.popViewController(animated: true)
        }
    }
    
    extension EventListViewController: UITextFieldDelegate, UISearchBarDelegate {
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            let vc = GMSAutocompleteViewController()
            UINavigationBar.appearance().tintColor = color.themePurple()
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            return false
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.endEditing(true)
            filterData.searchText = searchBar.text ?? ""
            offset = 0
            paramDict["search_title"] = filterData.searchText
            getEventList(filterParam: filterJSON)
        }
    }
    
    extension EventListViewController: GMSAutocompleteViewControllerDelegate {
        
        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
            UINavigationBar.appearance().tintColor = .white

            print("google map")
            print(place.placeID)
            print(place.coordinate)
            filterData.selectedPlace = place

            if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "United States" }) {
                filterData.distanceType = "1"
            } else {
                filterData.distanceType = "0"
            }
            txtSearch.text = place.name
            offset = 0
            
            paramDict["distance_type"] = filterData.distanceType
            paramDict["latitude"] = "\(place.coordinate.latitude)"
            paramDict["longtitude"] = "\(place.coordinate.longitude)"
            paramDict["place_id"] = place.placeID
            
            getEventList(filterParam: filterJSON)
            viewController.dismiss(animated: true, completion: nil)
        }
        
        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
            UINavigationBar.appearance().tintColor = .white
            viewController.dismiss(animated: true, completion: nil)
        }
        
        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            UINavigationBar.appearance().tintColor = .white
            viewController.dismiss(animated: true, completion: nil)
        }
    }
    
    extension EventListViewController {
        
        func getEventList(filterParam: JSON? = nil) {            
            
            if offset < 0{
                refreshControl.endRefreshing()
                self.isLoadedApi = true
                return
            }
            
            
            
            let kURL = webUrls.baseUrl() + webUrls.getEventList()
            
            let categoriesArray = filterData.category.map { $0.subCategories.filter { $0.isSelected }.map { $0.id } }
            
            let categories = categoriesArray.filter { !$0.isEmpty }.map { $0.joined(separator: ",") }.joined(separator: ",")
            
            print("Categories \(categories)")
            
            let latitude = filterData.selectedPlace?.coordinate.latitude ?? filterData.latitude ?? 0
            
            let longitude = filterData.selectedPlace?.coordinate.longitude ?? filterData.longitude ?? 0
            
            var param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "timezone" : TimeZone.current.identifier,
                         "lang" : "0",
                         "current_latitude" : "\(userLocation?.coordinate.latitude ?? 0)",
                "current_longtitude" : "\(userLocation?.coordinate.longitude ?? 0)",
                "current_place_id" : filterData.currentPlaceID,
                "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                "latitude" : "\(latitude)",
                "longtitude" : "\(longitude)",
                "distance_type" : filterData.distanceType,
                "place_id" :  filterData.selectedPlace?.placeID ?? "",
                "search_title" : filterData.searchText,
                "main_category" : filterData.mainCategory,
//                "category_id" : categories.isEmpty ? "0" : categories,
                "sort_by" : "",
                "sort_type" : "",
                "is_family_friendly" : "",
                "is_free" : "",
                "offset" : "\(offset)",
                "upcoming_event" : "\(filterData.isUpcomingEvent)",
                "from_date" : self.filterData.fromDateStr,
                "to_date" : self.filterData.toDateStr]
            
//            {"lgbtq": "1",    "difficulty_level": "",    "event_type": "",    "prize_award": "",    "genres": "1,2,3",    "festival_type": ""}
            var prm = JSON()
            if !isFilterData {
                if self.categoryId == .nightclub {
                    param["category_id"] = "1"
                    //                param["main_category"] = "1"
                    prm["lgbtq"].stringValue = "\(filterData.lgbtqType)"
                    prm["event_type"].stringValue = ""
                    prm["event_type"].stringValue = ""
                    prm["prize_award"].stringValue = ""
                    prm["genres"].stringValue = "\(filterData.eventType)"
                    prm["festival_type"].stringValue = ""
                }
                else if self.categoryId == .allFestival {
                    param["category_id"] = "3,4"
                    param["main_category"] = "2"
                    param["from_date"] = self.filterData.fromDateStr
                    param["to_date"] = self.filterData.toDateStr
                }
                else if self.categoryId == .music {
                    param["category_id"] = "3"
                    //                param["main_category"] = ""
                    param["from_date"] = self.filterData.fromDateStr
                    param["to_date"] = self.filterData.toDateStr
                    param["is_family_friendly"] = self.filterData.isFamilyFriendly
//                    prm["genres"].stringValue = "\(filterData.eventType)"
                    prm["festival_type"].stringValue = "\(filterData.eventType)"
                }
                else if self.categoryId == .streetFestivals {
                    param["category_id"] = "4"
                    //                param["main_category"] = ""
                    param["from_date"] = self.filterData.fromDateStr
                    param["to_date"] = self.filterData.toDateStr
                    param["is_family_friendly"] = self.filterData.isFamilyFriendly
                    param["is_free"] = filterData.free
//                    prm["genres"].stringValue = "\(filterData.eventType)"
                    prm["festival_type"].stringValue = "\(filterData.eventType)"
                }
                else if self.categoryId == .enduranceEvents {
                    param["category_id"] = "5"
                    param["from_date"] = self.filterData.fromDateStr
                    param["to_date"] = self.filterData.toDateStr
                    prm["event_type"].stringValue = "\(self.filterData.eventType)"
                    prm["difficulty_level"].stringValue = "\(self.filterData.isDifficultyLevel)"
                    prm["prize_award"].stringValue = "\(self.filterData.prizeAward)"
                    param["is_family_friendly"] = self.filterData.isFamilyFriendly
                }
                else if self.categoryId == .localEvents {
                    param["category_id"] = "6"
                    param["from_date"] = self.filterData.fromDateStr
                    param["to_date"] = self.filterData.toDateStr
                    param["is_family_friendly"] = self.filterData.isFamilyFriendly
                    param["is_free"] = filterData.free
                }
                else if self.categoryId == .upcomingEvents {
                    param["category_id"] = ""
                    param["current_latitude"] = ""
                    param["current_longtitude"] = ""
                    param["current_place_id"] = ""
                    param["device_token"] = UIDevice.current.identifierForVendor!.uuidString
                    param["latitude"] = ""
                    param["longtitude"] = ""
                    param["distance_type"] = ""
                    param["place_id"] =  ""
                    param["search_title"] = ""
                    param["main_category"] = ""
                    param["offset"] = ""
                    param["upcoming_event"] = "\(filterData.isUpcomingEvent)"
                }
                else {
                    param["category_id"] = categories.isEmpty ? "0" : categories
                }
                
                param["filter_data"] = prm.rawString()
            }
            
            if !paramDict.isEmpty {
                param = paramDict
                param["offset"] = "\(offset)"
            }
            
            if let filterParam = filterParam {
                for i in filterParam {
                    param[i.0] = i.1.stringValue
                }
                param["offset"] = "\(offset)"
            }

            print("param:", param)

            spinner.startAnimating()
            
            GeneralRequest.makeRequest(kURL, param: param, context: self, isShowLoader: false) { (json) in
                self.spinner.stopAnimating()
                self.refreshControl.endRefreshing()
                print(json)
                self.isLoadedApi = true
                
                if json["flag"].exists() {
                if json["flag"].stringValue == "1" {
                    
                    if self.offset == 0  {
                        self.eventList = []
                    }
                    if  self.offset < 0 {
                        self.spinner.stopAnimating()
                        return
                    }
                    self.offset = json["next_offset"].intValue
                    if self.isPullRefresh {
                        self.isPullRefresh = false
                        self.eventList = json["data"].arrayValue
                    } else {
                        var tempArray = self.eventList ?? []
                        tempArray.append(contentsOf: json["data"].arrayValue)
                        self.eventList = tempArray
                    }
                    self.paramDict = param
                } else {
                    self.errorMessage = json["msg"].stringValue
                    self.eventList = []
                }
                } else {
                    self.errorMessage = R.string.keys.somethingWentWrong()
                    self.eventList = []
                }
                self.tableView.reloadData()
                self.paramDict = param
            }
        }
        
    }
    
    //MARK: - Pagiantion
    /*
    extension EventListViewController {
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            if scrollView == tableView {
                if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) >= Int(scrollView.contentSize.height)) {
                    
                    if offset == -1 {
                        tableView.tableFooterView = nil
                        return
                    } else {
                        if eventList != nil {
                            getEventList()
                        }
                    }
                }
            }
        }
    }
     */
    
    extension EventListViewController {
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            if self.offset != -1 && self.offset != 0 {
                if (Double(self.tableView.contentOffset.y).rounded(toPlaces: 2))+1 >= Double(((Double(self.tableView.contentSize.height).rounded(toPlaces: 2)) - (Double(self.tableView.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2)) {
                    if self.isLoadedApi {
                        self.isLoadedApi = false
                        getEventList()
                    }
                }
            }
        }
    }
    
    extension EventListViewController: UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if eventList == nil {
                return 10
            } else {
                if eventList!.isEmpty {
                    setTableView(errorMessage, tableView: tableView)
                    return 0
                } else {
                    tableView.backgroundView = nil
                    /*if adsCellProvider != nil {
                        return Int(adsCellProvider.adjustCount(UInt(eventList!.count), forStride: UInt(adRowStep)))
                    } else {*/
                        return eventList!.count
//                    }
                }
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if eventList != nil {
                
                /*if adsCellProvider != nil && adsCellProvider.isAdCell(at: indexPath, forStride: UInt(adRowStep)) {
                    // Show an ad
                    let ad = adsManager.nextNativeAd
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "AdsCell", for: indexPath) as! AdsCell
//                    cell.contentView.backgroundColor =
                   
                    cell.message.text = ad?.bodyText
                    cell.title.text = ad?.rawBodyText
                    cell.callToActionButton.setTitle(ad?.callToAction, for: .normal)
                    ad?.loadAd(withMediaCachePolicy: FBNativeAdsCachePolicy.none)
                    ad?.downloadMedia()
                    if let pic = ad?.adChoicesIcon {
                        cell.postImage.sd_setImage(with: pic.url, completed: nil)
                    }
                   /* if let pic = ad?.ima {
                        cell.postImage.imageFromServerURL(urlString: pic.url.absoluteString)
                    }*/
//                    ad?.registerView(forInteraction: cell, with: self)
                    return cell
                }*/
                
                let item = eventList![indexPath.row]
                
                if item["category_id"].stringValue == "1" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EventListItemTableViewCell
                    cell.selectionStyle = .none
                    cell.hideAnimation()
                    cell.setEventList(item) 
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! EventListItemTableViewCell
                    cell.selectionStyle = .none
                    cell.hideAnimation()
                    cell.setEventList(item, isDisplayTime: true)
                    return cell
                }
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EventListItemTableViewCell
                return cell
            }
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && offset != -1 && self.offset != 0 {
                // print("this is the last cell")
                spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                spinner.color = color.grayAccent()
                spinner.hidesWhenStopped = true
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
            }
            else {
                tableView.tableFooterView?.isHidden = true
                tableView.tableFooterView = nil
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            /*if eventList != nil {
                let item = eventList![indexPath.row]
                let vc = SB.eventDetail.eventDetailViewController()!
                vc.eventType = EventType.type(index: item["category_id"].intValue - 1)
                EventDetailViewController.listID = item["listing_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }*/
           /* if eventList != nil {
                let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
                let vc = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetailVC") as! NewEventDetailVC
                vc.dictData = eventList![indexPath.row]
                vc.eventType = EventType.type(index: eventList![indexPath.row]["category_id"].intValue - 1)
                self.navigationController?.pushViewController(vc, animated: true)
            }*/
            if eventList != nil {
                /*if adsCellProvider != nil && adsCellProvider.isAdCell(at: indexPath, forStride: UInt(adRowStep)) {
                    return
                }*/
                eventType = EventType.type(index: eventList![indexPath.row]["category_id"].intValue - 1)
                getEventDetailData(eventId: eventList?[indexPath.row]["listing_id"].stringValue ?? "0",                               tag: indexPath.row)
            }
            
        }
        
        func jumpToMixVC(json:JSON) {
          
            let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
            let headerScroll = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetail") as! NewEventDetail
            
            var hh:UIViewController!
            hh = headerScroll
            headerScroll.dictData = json
            
           // var child1 : UIViewController!
           // var child2 :  UIViewController!
            
            let child1 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
            child1.dictEventDetailData = json
            
            let child2 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
            child2.dictEventDetailData = json
            
            let child3 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
            child3.dictEventDetailData = json
            child3.selectedIndex = 10
            
            let child4 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
            child4.dictEventDetailData = json
            child4.selectedIndex = 11
            
            var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
            switch eventType {
            case .nightClub:
                child1.selectedIndex = 0 // Headline
               
                child2.selectedIndex = 1 //event
               
                break
            case .clubCrawl:
               headerTitles = ["HEADLINES", "DETAILS", "DESCRIPTION", "RATINGS"]
                child1.selectedIndex = 2 //clubcrawl
               
                child2.selectedIndex = 3 //musiclineup
                break
            case .music:
                headerTitles = ["HEADLINES", "LINEUP", "DESCRIPTION", "RATINGS"]
                child1.selectedIndex = 4 //musicheadline
                
                child2.selectedIndex = 5 //musiclineup
                break
            case .street:
                child1.selectedIndex = 6 //streethealine
                
                child2.selectedIndex = 7 //nightevent
                break
            case .obstacle:
                headerTitles = ["HEADLINES", "RACE DETAILS", "DESCRIPTION", "RATINGS"]
               
                child1.selectedIndex = 8 //obstacle
                
                child2.selectedIndex = 9 //obstalce race
                break
            case .local:
//                headerTitles = ["HEADLINES", "DESCRIPTION", "RATINGS"]
                headerTitles = ["HEADLINES", "DESCRIPTION"]
                
                child1.selectedIndex = 8 //obstacle
                
                child2.selectedIndex = 9 //obstalce race
                break
            }
            let segment = MSSegmentControl(sectionTitles: headerTitles)
            setupSegment(segmentView: segment)
            
            let mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child2,child3,child4], segmentView: segment)
            mx.view.backgroundColor = color.themePurple()
            mx.edgesForExtendedLayout = []
           // mx.title = "Treegala"
            mx.navigationItem.titleView = setMXLogoTitle()
       //     mx.extendedLayoutIncludesOpaqueBars = true
            mx.shouldScrollToBottomAtFirstTime = false
            mx.setBackButton()
            navigationController?.pushViewController(mx, animated: true)
            
        }
        
        
        func setupSegment(segmentView: MSSegmentControl) {
            segmentView.borderType = .none
            segmentView.backgroundColor = color.themePurple()
            segmentView.selectionIndicatorColor = .white
            segmentView.selectionIndicatorHeight = 3
            let Segment_statu_titleNormal = UIFont(name: "UbuntuCondensed-Regular", size: 13)!
            segmentView.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Segment_statu_titleNormal]
            
            segmentView.segmentWidthStyle = .fixed
            segmentView.indicatorWidthPercent = 2
            segmentView.selectionStyle = .fullWidth
        }

    }
    
    
    extension UIViewController{
        private func trigger(selector: Selector) -> Observable<Void> {
            return rx.sentMessage(selector).map { _ in () }.share(replay: 1)
        }
        
        var viewWillAppearTrigger: Observable<Void> {
            return self.trigger(selector: #selector(self.viewWillAppear(_:)))
        }
        
        var viewDidAppearTrigger: Observable<Void> {
            return self.trigger(selector: #selector(self.viewDidAppear(_:)))
        }
        
        var viewWillDisappearTrigger: Observable<Void> {
            return self.trigger(selector: #selector(self.viewWillDisappear(_:)))
        }
        
        var viewDidDisappearTrigger: Observable<Void> {
            return self.trigger(selector: #selector(self.viewDidDisappear(_:)))
        }
        
    }
    
    //MARK:- API
    
    extension EventListViewController
    {
        func getEventDetailData(eventId:String,tag:Int) {
            
            let kURL = webUrls.baseUrl() + webUrls.getEventData()
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "timezone" : TimeZone.current.identifier,
                         "lang" : "0",
                         "listing_id" : eventId]
            
            print(param)
            
            startLoader()
            GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
                self.dismissLoader()
                print(json)
                if json["flag"].stringValue == "1" {
                    
                    let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
                    let headerScroll: MXHomeViewController = eventStoryboard.instantiateViewController(withIdentifier: "MXHomeViewController") as! MXHomeViewController
                    headerScroll.json = json["data"]
                    headerScroll.eventType = self.eventType
                    self.navigationController?.pushViewController(headerScroll, animated: true)
                   // self.jumpToMixVC(json: json["data"])
                    //self.dictEventDetailData = json["data"]
                   // self.tblEventDetail.reloadData()
                }
            }
        }
    }

    
    
    
    /*extension EventListViewController : FBNativeAdDelegate,FBNativeAdsManagerDelegate{
        func nativeAdsLoaded() {
            adsCellProvider = FBNativeAdTableViewCellProvider(manager: adsManager, for: FBNativeAdViewType.genericHeight300)
            adsCellProvider.delegate = self
            
            if tableView != nil {
                tableView.reloadData()
            }
        }
        
        func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
            print("Ad loaded")
        }
        
        func nativeAdDidClick(_ nativeAd: FBNativeAd) {
            print("Ad tapped: \(nativeAd)")
        }
        
        public func nativeAdsFailedToLoadWithError(_ error: Error) {
            print("Ad failed to load with error: \(error)")
        }
        
        func nativeAdWillLogImpression(_ nativeAd: FBNativeAd) {
            print("Native ad impression is being captured.")
        }
        
        func nativeAdDidDownloadMedia(_ nativeAd: FBNativeAd) {
            
        }
    }*/
