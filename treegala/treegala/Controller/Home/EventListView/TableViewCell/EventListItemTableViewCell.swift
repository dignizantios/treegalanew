//
//  EventListItemTableViewCell.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class EventListItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var viewVote: UIView!
    @IBOutlet weak var lblVotePercent: UILabel!
    @IBOutlet weak var lblVotedText: UILabel!
    @IBOutlet weak var lblVoteType: UILabel!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    @IBOutlet weak var viewLGBTQ: UIView!
    
    @IBOutlet weak var lblDate: UILabel?
    @IBOutlet weak var lblDateIcon: UIButton?
    
    @IBOutlet var vwSeasonalBanner: UIView!
    @IBOutlet var vwExpiredBanner: UIView!
    @IBOutlet var vwRepportedBanner: UIView!
    
    var categaryType: [Int] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        imgEvent.showAnimatedSkeleton()
        [lblEventName,lblLocation].forEach { $0?.showAnimatedSkeleton() }
        lblDate?.showAnimatedSkeleton()
        
        [vwSeasonalBanner, vwExpiredBanner, vwRepportedBanner].forEach { (vw) in
            vw?.isHidden = true
        }
    }
    
    override func draw(_ rect: CGRect) {
        lblEventName.minimumScaleFactor = 0.1    //or whatever suits your need
        lblEventName.adjustsFontSizeToFitWidth = true
        lblEventName.lineBreakMode = .byClipping
        lblEventName.numberOfLines = 0
    }
    
    func hideAnimation() {
        imgEvent.hideSkeleton()
        [lblEventName,lblLocation].forEach { $0?.hideSkeleton() }
        lblDate?.hideSkeleton()
        lblVotedText.text = "Voted"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setEventList(_ detail: JSON, isDisplayTime: Bool = false) {
        imgEvent.sd_setImage(with: detail["listing_image"].stringValue.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_image_placeholder"), options: .highPriority, completed: nil)
        self.showBanner(data: detail)
       // imgEvent.image = #imageLiteral(resourceName: "demo1")
        viewVote.isHidden = detail["ratting_rank"].stringValue == "0" ? true : false
        lblEventName.text = detail["title"].stringValue
        lblLocation.text = detail["location"].stringValue.capitalized
        lblVotePercent.text = detail["ratting_per"].stringValue + " %"
        lblVoteType.text = "Fun"
        
        viewLGBTQ.isHidden = detail["lgbtq"].stringValue == "0"
        
        btnCategory.setImage(categoryImage(type: detail["category_id"].stringValue), for: .normal)
        let categoryId = detail["category_id"].stringValue
        
        if isDisplayTime {
            if detail["is_date"].stringValue == ""
            {
                lblDateIcon?.isHidden = true
                lblDate?.text = ""
                lblDate?.isHidden = true
            }
            else if detail["is_date"].stringValue == "1"
            {
                lblDate?.isHidden = false
                lblDate?.text = detail["date_text"].stringValue
                lblDateIcon?.isHidden = false
            }
            else
            {
                lblDate?.isHidden = false
                if categoryId == "2" || categoryId == "5" || categoryId == "6" {
                    let dateFmt = DateFormatter()
                    dateFmt.dateFormat = "yyyy-MM-dd"
                    
                    if let date = dateFmt.date(from: detail["start_date"].stringValue) {
                        print(date)
                        dateFmt.dateFormat = "d MMM yyyy"
                        print(dateFmt.string(from: date))
                        lblDate?.text = dateFmt.string(from: date)
                        lblDateIcon?.isHidden = false
                    } else {
                        lblDateIcon?.isHidden = true
                    }
                } else if categoryId == "3" || categoryId == "4" {
                    
                    let dateFmt = DateFormatter()
                    dateFmt.dateFormat = "yyyy-MM-dd"
                    
                    if detail["end_date"].stringValue == "0000-00-00" {
                        
                        var dateString = ""
                        
                        if let date = dateFmt.date(from: detail["start_date"].stringValue) {
                            dateFmt.dateFormat = "d MMM yyy"
                            dateString += dateFmt.string(from: date)
                        }
                        
                        lblDateIcon?.isHidden = dateString.isEmpty
                        lblDate?.text = dateString
                        
                    } else {
                        
                        var date = detail["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy")
                        
                        if detail["start_date"].stringValue != detail["end_date"].stringValue {
                            date += " - " + detail["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy")
                        }
                        lblDateIcon?.isHidden = date.isEmpty
                        lblDate?.text = date
                    }
                }
            }
        }
    }
    
    func showBanner(data: JSON) {
        [vwSeasonalBanner, vwExpiredBanner, vwRepportedBanner].forEach { (vw) in
            vw?.isHidden = true
        }
        if data["is_expired_banner"].stringValue != "0" {
            self.vwExpiredBanner.isHidden = false
        } else if data["is_season_banner"].stringValue != "0" {
            self.vwSeasonalBanner.isHidden = false
        }
        else if data["is_reported_banner"].stringValue != "0" {
            self.vwRepportedBanner.isHidden = false
        }
    }

    func getEventType(type: String) -> String {
        if type == "1" {
            return "Fun"
        } else if type == "2" {
            return "Satisfactory"
        } else if type == "3" {
            return "Boring"
        } else  {
            return "Dislike"
        }
    }
    
    func categoryImage(type: String) -> UIImage {
        
        if type == "0" {
            lblCategoryName.text = "All"
            return #imageLiteral(resourceName: "ic_big_nightclubs_parpal")
        } else if type == "1" {
            lblCategoryName.text = "Nightclub"
            return #imageLiteral(resourceName: "ic_big_nightclubs_parpal")
        } else if type == "2" {
            lblCategoryName.text = "Club Crawls"
            return #imageLiteral(resourceName: "ic_gitar_mini_parpal")
        } else if type == "3" {
            lblCategoryName.text = "Music Festival"
            return #imageLiteral(resourceName: "ic_gitar_mini_parpal")
        } else if type == "4" {
            lblCategoryName.text = "Street Festival"
            return #imageLiteral(resourceName: "ic_mini_street_festivals_parpal")
        } else if type == "5" {
            lblCategoryName.text = "Run Event"
            return #imageLiteral(resourceName: "ic_big_endurance_events_parpal")
        } else {
            lblCategoryName.text = "Local Event"
            return #imageLiteral(resourceName: "ic_big_vocal_events_parpal")
        }
        
        /*if type == "1" {
            lblCategoryName.text = "Nightclubs"
            return #imageLiteral(resourceName: "ic_night_club_list_category")
        } else if type == "2" {
            lblCategoryName.text = "Club Crawls"
            return #imageLiteral(resourceName: "ic_club_crawls_list_category")
        } else if type == "3" {
            lblCategoryName.text = "Music Festival"
            return #imageLiteral(resourceName: "ic_music_festival_list_category")
        } else if type == "4" {
            lblCategoryName.text = "Street Festival"
            return #imageLiteral(resourceName: "ic_street_festival_list_category")
        }else  {
            lblCategoryName.text = "Run Event"
            return #imageLiteral(resourceName: "ic_obstacles_event_list_category")
        }*/
    }
}
