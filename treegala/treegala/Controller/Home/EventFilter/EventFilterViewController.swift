//
//  EventFilterViewController.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON

class EventFilterViewController: UIViewController {
    
    @IBOutlet weak var controls: EventFilterObjects!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSelectDateRange: xUIButton!
    var filterObserver = PublishSubject<JSON>()
    var filtercategory = PublishSubject<[Category]>()
    var resetFilter = PublishSubject<()>()
    var disposeBag = DisposeBag()
    
    var dateFilter:(String, String) -> Void = { _,_ in}
    
    /*
     toDateStr = last
     self.filterData.fromDateStr*/
    
    var categoryItems: [(String,String)] = [] {
        didSet {
            controls.categoryColletion.reloadData()
            setCategoryOptions()
        }
    }
    
    var runTypes: [JSON] = [] {
        didSet {
            controls.runTypeDD.dataSource = runTypes.map { $0["event_type"].stringValue }
            setFilterData(forType: 1)
        }
    }
    
    var selectedRunType: [JSON] = [] {
        didSet {
            controls.runTypeCollection.reloadData()
        }
    }
    
    var genresItems: [JSON] = [] {
        didSet {
            controls.genresDD.dataSource = genresItems.map { $0["genres_name"].stringValue }
            setFilterData(forType: 2)
        }
    }
    
    var selectedGenreType: [JSON] = [] {
        didSet {
            controls.genresCollection.reloadData()
        }
    }
    
    var festivalItems: [JSON] = [] {
        didSet {
            controls.festivalDD.dataSource = festivalItems.map { $0["festival_type"].stringValue }
            setFilterData(forType: 3)
        }
    }
    
    var selectedFestivalType: [JSON] = [] {
        didSet {
            controls.festivalCollection.reloadData()
        }
    }
    
    var selectCategory : [JSON] = []
    
    
    var selectedCategory: [Category] = []
    var filterJSON = JSON()
    var filterData = FilterData()
    var sortType: SortType = .random
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(filterJSON)
   
        setupUI()
        setObservables()
        controls.setObserver(on: self)
        
        getOptionList()
        
        btnReset.isHidden = !filterJSON["filter_data"].exists()
        
        if globalCategories.count != 0 {
            btnReset.isHidden = false
        }
        
        btnReset.rx.tap.subscribe(onNext: {
            self.btnReset.isHidden = true
            self.filterJSON = JSON()
            self.setFilterData(forType: 1)
            self.selectedCategory = []
            self.categoryItems = []
            self.resetFilter.onNext(())
        }).disposed(by: disposeBag)
        
        btnSelectDateRange.rx.tap.subscribe(onNext: {
            let vc = R.storyboard.newUpdates.selectDateRangeVC()
            vc?.modalTransitionStyle = .coverVertical
            vc?.modalPresentationStyle = .overCurrentContext
            
            vc?.selectDateHandlor = { first, last in
                self.filterData.toDateStr = last
                self.filterData.fromDateStr = first
                self.dateFilter(first, last)
                self.btnSelectDateRange.setTitle("  "+first+" / "+last, for: .normal)
            }
            
            self.present(vc!, animated: false, completion: nil)
        }).disposed(by: disposeBag)
        
//        if self.filterData.lgbtqType == "1" {
//            self.controls.btnLGBTQ.isSelected = true
//        }
//        if self.filterData.isFamilyFriendly == "1" {
//            self.controls.btnFamilyFriendly.isSelected = true
//        }
//        if self.filterData.free == "1" {
//            self.controls.btnFree.isSelected = true
//        }
        controls?.genresCollection?.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        controls.addHeightObserver(on: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        controls.removeHeightObserver(on: self)
    }
    
    func setupUI() {
        controls.setupUI()
        
        getItem(category: selectedCategory)
        
        let btn = controls.btnSort[1]
        btnSegment(btn)
        
        //MARK: VP Setup
        if globalCategories.count != 0 {
            getItem(category: globalCategories)
            selectedCategory = globalCategories
        }
        
    }
    
    func setCategoryOptions() {
        
        let categories = categoryItems.map { $0.1 }
        
        //1. Nightclub, 2. Clubcrawls, 3. Music, 4. Street, 5. Run event
        
        let LGBTQ = ["1","3","4"]
        let genres = ["1","3"]
        let festival = ["4"]
        let runType = ["5"]
        
        controls.viewLGBTQ.isHidden = (Set(categories).intersection(LGBTQ)).isEmpty
        controls.viewGenres.isHidden = (Set(categories).intersection(genres)).isEmpty
        
        if (Set(categories).intersection(genres)).isEmpty {
            selectedGenreType.removeAll()
        }
        
        controls.viewFestival.isHidden = (Set(categories).intersection(festival)).isEmpty
        
        if (Set(categories).intersection(festival)).isEmpty {
            selectedFestivalType.removeAll()
        }
        
        [controls.viewRunType, controls.viewDifficulty].forEach {
            $0?.isHidden = (Set(categories).intersection(runType)).isEmpty
        }
        
        if (Set(categories).intersection(runType)).isEmpty {
            selectedRunType.removeAll()
            controls.btnDifficulties.forEach { $0.isHidden = true }
            controls.selectedDifficulty = nil
        }
    }
    
    func setObservables() {
        controls.btnCategory.rx.tap.subscribe(onNext: {
                self.view.endEditing(true)
                let vc = SB.home.categoryListPopupViewController()!
                vc.tempCategory = self.selectedCategory
                vc.selectedCategory.subscribe(onNext: {
                    print($0)
                    self.selectedCategory = $0
                    self.getItem(category: $0)
                }).disposed(by: self.disposeBag)
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            }).disposed(by: disposeBag)
        
        controls.btnGenres.rx.tap.subscribe(onNext: {
            self.showOptionSelection(type: 1)
        }).disposed(by: disposeBag)
        
        controls.btnRunType.rx.tap.subscribe(onNext: {
            self.showOptionSelection(type: 0)
        }).disposed(by: disposeBag)
        
        controls.btnFestival.rx.tap.subscribe(onNext: {
            self.showOptionSelection(type: 2)
        }).disposed(by: disposeBag)
        
        controls.btnShowResult.rx.tap.subscribe(onNext: {
            
            var json = JSON()
            
            var sortby = 0
            
//            if self.sortType == .cost {
//                sortby = 1
//            } else
            if self.sortType == .distance {
                sortby = 0
            } else if self.sortType == .rating {
                sortby = 2
            } else if self.sortType == .random {
                sortby = 3
            }
            
            json["sort_by"].intValue = sortby
            json["sort_type"].stringValue = self.controls.btnSortOrder.isSelected ? "DESC" : "ASC"
            json["is_family_friendly"].stringValue = self.controls.btnFamilyFriendly.isSelected ? "1" : "0"
            json["is_free"].stringValue = self.controls.btnFree.isSelected ? "1" : "0"
        
            json["category_id"].stringValue = self.categoryItems.map { $0.1 }.joined(separator: ",")
            
            let dict = ["lgbtq" : self.controls.btnLGBTQ.isSelected ? "1" : "0",
                        "difficulty_level" : "\(self.controls.selectedDifficulty ?? 0)" ,
                        "event_type" : self.selectedRunType.map { $0["event_type_id"].stringValue }.joined(separator: ","),
                        "prize_award" : self.controls.btnPrizeAward.isSelected ? "1" : "0",
                        "genres" : self.selectedGenreType.map { $0["genres_id"].stringValue }.joined(separator: ","),
                        "festival_type" : self.selectedFestivalType.map { $0["festival_type_id"].stringValue }.joined(separator: ",")]
            
            let filterData = JSON(dict).rawString() ?? ""
            
            json["filter_data"].stringValue = filterData
            self.filtercategory.onNext(self.selectedCategory)
            self.filterObserver.onNext(json)
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    func setFilterData(forType: Int) {
        
        if let tag = filterJSON["sort_by"].int {
            
            if let index = controls.btnSort.index(where: { $0.tag == tag + 1 }) {
                let btnSeg = controls.btnSort[index]
                btnSegment(btnSeg)
            }
        } else {
            let btn = controls.btnSort[1]
            btnSegment(btn)
        }
 
        self.controls.btnFamilyFriendly.isSelected = filterJSON["is_family_friendly"].stringValue ==  "1"
        self.controls.btnFree.isSelected = filterJSON["is_free"].stringValue ==  "1"
        
        self.controls.btnSortOrder.isSelected = filterJSON["sort_type"].stringValue ==  "DESC"
        
        let str = filterJSON["filter_data"].stringValue
        let json = str.data(using: String.Encoding.utf8).flatMap({try? JSON(data: $0)}) ?? JSON(NSNull())
        self.controls.btnLGBTQ.isSelected = json["lgbtq"].stringValue == "1"
        
        if let _ = json["difficulty_level"].string {
            controls.selectedDifficulty = json["difficulty_level"].intValue
            let btn = UIButton()
            btn.tag = json["difficulty_level"].intValue
            controls.btnDifficultyTapped(btn)
        } else {
            controls.selectedDifficulty = nil
        }

        if forType == 1 { //Run
            let types = json["event_type"].stringValue
            selectedRunType = runTypes.filter { types.contains( $0["event_type_id"].stringValue ) }
        } else if forType == 2 { //Gener
            let types = json["genres"].stringValue
            selectedGenreType = genresItems.filter { types.contains( $0["genres_id"].stringValue ) }
        } else { //festival
            self.controls.btnPrizeAward.isSelected = json["prize_award"].stringValue == "1"
            let types = json["festival_type"].stringValue
            selectedFestivalType = festivalItems.filter { types.contains( $0["festival_type_id"].stringValue ) }
        }
    }
    
    func getItem(category: [Category]) {
        let items = category
        
        var finalItems: [(String, String)] = []
        
        for i in items {
            let temp = (i.subCategories.filter { $0.isSelected }).map { ($0.name, $0.id) }
            finalItems.append(contentsOf: temp)
        }
        print(finalItems)
        self.categoryItems = finalItems
    }
    
    @IBAction func btnSegment(_ sender: UIButton) {
        controls.btnSort.forEach {
            $0.isSelected = false
            $0.backgroundColor = .white
        }
        print(sender.tag)
        sortType = SortType(rawValue: sender.tag - 1)!
        print(sortType)
        sender.isSelected = true
        sender.backgroundColor = color.themePurple()
    }
    
    func getOptionList() {
        
        let kURL = webUrls.baseUrl() + webUrls.getGenreList()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0"]
        GeneralRequest.makeGetRequest(kURL, param: param, context: self, isShowLoader: true) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.runTypes = json["data"]["event_type"].arrayValue
                self.genresItems = json["data"]["genres"].arrayValue
                self.festivalItems = json["data"]["festival_type"].arrayValue
            }
        }
    }
    
    func updateCategoryItem(categoryID:String) {
//        var removeIndex:(Int,Int) = (-1,-1)
        var section = 0
        
        for item in selectedCategory {
//            removeIndex.0 = section
            print("item:=", item)
            if let indx = item.subCategories.index(where: {$0.id == categoryID}) {
//                removeIndex.1 = indx
                
                selectedCategory[section].isSelected = false
                selectedCategory[section].subCategories[indx].isSelected = false
                break
            }
            section += 1
        }
         selectedCategory[0].isSelected = selectedCategory.map({$0.isSelected == false}).count == 0 ? true : false
        
//        if removeIndex.0 != -1 && removeIndex.1 != -1 {
//            selectedCategory[removeIndex.0].subCategories[removeIndex.1].isSelected = false
//        }
        print("selectedCategory:=",selectedCategory)
    }
}

extension EventFilterViewController {
    
    func showOptionSelection(type: Int) {
        
        let vc = SB.home.optionSelectionViewController()!
        vc.optionType = type
        if type == 0 {
            vc.item = runTypes
            vc.selectedType = selectedRunType.map { $0["event_type_id"].stringValue }
        } else if type == 1 {
            vc.item = genresItems
            vc.filterData.eventType = filterData.eventType
            vc.selectedType = selectedGenreType.map { $0["genres_id"].stringValue }
        }  else if type == 2 {
            vc.item = festivalItems
            vc.selectedType = selectedFestivalType.map { $0["festival_type_id"].stringValue }
        }
        vc.itemObserver.subscribe(onNext: { (items) in
            print(items)
            if type == 0 {
               self.selectedRunType = items
            } else if type == 1 {
                self.selectedGenreType = items
            }  else if type == 2 {
                self.selectedFestivalType = items
            }
        }).disposed(by: disposeBag)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension EventFilterViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == controls.categoryColletion {
            return categoryItems.count
        } else if collectionView == controls.runTypeCollection {
            return selectedRunType.count
        } else if collectionView == controls.genresCollection {
            return selectedGenreType.count
        } else if collectionView == controls.festivalCollection {
            return selectedFestivalType.count
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for:
            indexPath) as! EventFilterTagCollectionViewCell
        
        if collectionView == controls.categoryColletion {
            cell.btnCancel.rx.tap.subscribe(onNext: {
                print("SelectCategory:", self.selectedCategory)
                print("remove category \(self.categoryItems[indexPath.row])")
//                print("remove selectedCategory \(self.selectedCategory[indexPath.row])")
                self.updateCategoryItem(categoryID: self.categoryItems[indexPath.row].1)
                self.categoryItems.remove(at: indexPath.row)
//                self.selectedCategory.remove(at: indexPath.row)
                print("category \(self.categoryItems)")
                print("selectedCategory \(self.selectedCategory)")
                //print(self.selectedCategory.map { $0.subCategories.map { $0.name } } )
            }).disposed(by: cell.disposeBag)
        } else if collectionView == controls.runTypeCollection {
            cell.btnCancel.rx.tap.subscribe(onNext: {
                self.selectedRunType.remove(at: indexPath.row)
            }).disposed(by: cell.disposeBag)
        } else if collectionView == controls.genresCollection {
            cell.btnCancel.rx.tap.subscribe(onNext: {
                self.selectedGenreType.remove(at: indexPath.row)
            }).disposed(by: cell.disposeBag)
        } else if collectionView == controls.festivalCollection {
            cell.btnCancel.rx.tap.subscribe(onNext: {
                self.selectedFestivalType.remove(at: indexPath.row)
            }).disposed(by: cell.disposeBag)
        }
        
        cell.lblName.text = getCellItem(collectionView: collectionView, indexPath: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item = getCellItem(collectionView: collectionView, indexPath: indexPath)

        let width = item.width(withConstrainedHeight: 35, font: font.ubuntuCondensedRegular(size: 16)!) + 50
        return CGSize.init(width: width, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func getCellItem(collectionView: UICollectionView, indexPath: IndexPath) -> String {
        var item = ""
        
        if collectionView == controls.categoryColletion {
            item = categoryItems[indexPath.row].0
        } else if collectionView == controls.runTypeCollection {
            item = selectedRunType[indexPath.row]["event_type"].stringValue
        } else if collectionView == controls.genresCollection {
            item = selectedGenreType[indexPath.row]["genres_name"].stringValue
        } else if collectionView == controls.festivalCollection {
            item = selectedFestivalType[indexPath.row]["festival_type"].stringValue
        }
        return item
    }
    
}


