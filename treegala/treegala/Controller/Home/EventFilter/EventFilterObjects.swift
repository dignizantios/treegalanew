//
//  EventFilterObjects.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import DropDown
import RxSwift
import RxCocoa

class EventFilterObjects: NSObject {
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var categoryColletion: UICollectionView!
    @IBOutlet weak var heightOfCategory: NSLayoutConstraint!
    
    @IBOutlet weak var btnSortOrder: UIButton!
    @IBOutlet var btnSort: [UIButton]!
    
    @IBOutlet weak var btnFamilyFriendly: UIButton!
    @IBOutlet weak var btnFree: UIButton!
    
    @IBOutlet weak var viewLGBTQ: UIView!
    @IBOutlet weak var btnLGBTQ: UIButton!
    
    @IBOutlet weak var viewDifficulty: UIView!
    @IBOutlet weak var btnClearDiffculty: UIButton!
    @IBOutlet var btnDifficulties: [UIButton]!
    
    @IBOutlet weak var viewRunType: UIView!
    @IBOutlet weak var btnRunType: UIButton!
    @IBOutlet weak var runTypeCollection: UICollectionView!
    @IBOutlet weak var heightOfRunType: NSLayoutConstraint!
    @IBOutlet weak var btnPrizeAward: UIButton!
    
    @IBOutlet weak var viewGenres: UIView!
    @IBOutlet weak var btnGenres: UIButton!
    @IBOutlet weak var genresCollection: UICollectionView!
    @IBOutlet weak var heightOfGenres: NSLayoutConstraint!
    
    @IBOutlet weak var viewFestival: UIView!
    @IBOutlet weak var btnFestival: UIButton!
    @IBOutlet weak var festivalCollection: UICollectionView!
    @IBOutlet weak var heightOfFestival: NSLayoutConstraint!
    
   
    @IBOutlet weak var btnShowResult: UIButton!
    
    var selectedDifficulty: Int? {
        didSet {
            btnClearDiffculty.isHidden = selectedDifficulty == nil
        }
    }
    
    var disposeBag = DisposeBag()
    
    var genresDD = DropDown()
    var runTypeDD = DropDown()
    var festivalDD = DropDown()
    
    func setupUI() {
//       configDropdown(dropdown: genresDD, sender: txtGenres)
//       configDropdown(dropdown: runTypeDD, sender: txtRunType)
//       configDropdown(dropdown: festivalDD, sender: txtFestival)
        
        [categoryColletion, genresCollection, runTypeCollection, festivalCollection].forEach {
            $0!.register(nib.eventFilterTagCell(), forCellWithReuseIdentifier: "cell")
            let layout = JesLeftAlignLayout()
            $0!.collectionViewLayout = layout
        }
        
        btnClearDiffculty.rx.tap.subscribe(onNext: {
            self.btnDifficulties.forEach { $0.isHidden = true }
            self.selectedDifficulty = nil
        }).disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
    @IBAction func btnDifficultyTapped(_ sender: UIButton) {
        
        btnDifficulties.forEach { $0.isHidden = true }
        
        if let index = btnDifficulties.index(where: { $0.tag == sender.tag }) {
            self.selectedDifficulty = index + 1
            btnDifficulties[index].isHidden = false
        }
    }
    
    func addHeightObserver(on context: UIViewController) {
        [categoryColletion, genresCollection, runTypeCollection, festivalCollection].forEach {
            $0.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        }
    }
    
    func removeHeightObserver(on context: UIViewController) {
        [categoryColletion, genresCollection, runTypeCollection, festivalCollection].forEach {
            $0.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    func setObserver(on context: UIViewController) {
        btnClose.rx.tap.subscribe(onNext: {
            context.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        btnFamilyFriendly.rx.tap.subscribe(onNext: {
            self.btnFamilyFriendly.isSelected = !self.btnFamilyFriendly.isSelected
        }).disposed(by: disposeBag)
        
        btnFree.rx.tap.subscribe(onNext: {
            self.btnFree.isSelected = !self.btnFree.isSelected
        }).disposed(by: disposeBag)
        
        btnLGBTQ.rx.tap.subscribe(onNext: {
            self.btnLGBTQ.isSelected = !self.btnLGBTQ.isSelected
        }).disposed(by: disposeBag)
        
        btnPrizeAward.rx.tap.subscribe(onNext: {
            self.btnPrizeAward.isSelected = !self.btnPrizeAward.isSelected
        }).disposed(by: disposeBag)
        
        btnSortOrder.rx.tap.subscribe(onNext: {
            self.btnSortOrder.isSelected = !self.btnSortOrder.isSelected
        }).disposed(by: disposeBag)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let collectionView = object as? UICollectionView, keyPath == "contentSize" {
            if collectionView == categoryColletion {
                heightOfCategory.constant = categoryColletion.contentSize.height
            } else if collectionView == genresCollection {
                heightOfGenres.constant = genresCollection.contentSize.height
            } else if collectionView == runTypeCollection {
                heightOfRunType.constant = runTypeCollection.contentSize.height
            } else if collectionView == festivalCollection {
                heightOfFestival.constant = festivalCollection.contentSize.height
            }
        }
    }
    
}
