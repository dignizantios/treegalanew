//
//  CheckOptionTableViewCell.swift
//  treegala
//
//  Created by om on 9/5/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class CheckOptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblOptionName: UILabel!
    @IBOutlet weak var btnCheck: UIButton!

    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
