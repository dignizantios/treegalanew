//
//  OptionSelectionViewController.swift
//  treegala
//
//  Created by om on 9/5/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift

class OptionSelectionViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    var selectedType: [String] = []
    var item: [JSON] = []
    var optionType = 0
    var filterData = FilterData()
    var disposeBag = DisposeBag()
    var selectedIndex: [Int] = [] {
        didSet {
            btnAll.isSelected = item.count == selectedIndex.count
        }
    }
    var itemObserver = PublishSubject<[JSON]>()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(nib.checkOptionTableViewCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        
        print(selectedType)
        
        for i in 0..<item.count {
            if selectedType.contains(item[i][getKeyName().id].stringValue) {
                print(i)
                print(item[i])
                if !self.selectedIndex.contains(i) {
                    self.selectedIndex.append(i)
                }
            }
        }
        
        
        
        print(item)
        print(getKeyName())
        
        if optionType == 0 { //Run Type
            lblTitle.text = "Type of run"
        } else if optionType == 1 { //Gener
            lblTitle.text = "Genres"
        } else { // Festival
            lblTitle.text = "Festival Type"
        }

        btnAll.rx.tap.subscribe(onNext: {
            if self.btnAll.isSelected {
                self.selectedIndex.removeAll()
            } else {
                for i in 0..<self.item.count {
                    if !self.selectedIndex.contains(i) {
                        self.selectedIndex.append(i)
                    }
                }
            }
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        btnClose.rx.tap.subscribe(onNext: {
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        btnDone.rx.tap.subscribe(onNext: {
            print(self.selectedIndex)
            var items: [JSON] = []
            for i in self.selectedIndex {
                items.append(self.item[i])
            }
            print(items)
            self.itemObserver.onNext(items)
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
    }
    
    func getKeyName() -> (id: String, name: String) {
        if optionType == 0 { //Run Type
            return ("event_type_id", "event_type")
        } else if optionType == 1 { //Gener
            return ("genres_id", "genres_name")
        } else { // Festival
            return ("festival_type_id", "festival_type")
        }
    }
}

extension OptionSelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CheckOptionTableViewCell

        let option = item[indexPath.row]
        cell.btnCheck.isSelected = selectedIndex.contains(indexPath.row)
        cell.btnCheck.tag = indexPath.row
        
        cell.btnCheck.rx.tap.subscribe(onNext: {
            if let index = self.selectedIndex.index(of: indexPath.row) {
                self.selectedIndex.remove(at: index)
            } else {
                self.selectedIndex.append(indexPath.row)
            }
            self.tableView.reloadData()
        }).disposed(by: cell.disposeBag)
        
        cell.lblOptionName.text = option[getKeyName().name].stringValue
        
        return cell
    }
}

