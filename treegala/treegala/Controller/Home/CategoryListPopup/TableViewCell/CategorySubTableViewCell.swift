//
//  CategorySubTableViewCell.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class CategorySubTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
