//
//  CategoryListPopupViewController.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CategoryListPopupViewController: UIViewController { 
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnDone: UIButton!
    
    var categories: [Category] = []
    
    var disposeBag = DisposeBag()
    var selectedCategory = PublishSubject<[Category]>()
    
    var tempCategory = [Category]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(nib.categorySubCell(), forCellReuseIdentifier: "cell")
        tableView.register(nib.categoryHeaderCell(), forCellReuseIdentifier: "headerCell")
        tableView.separatorStyle = .none
        
        btnClose.rx.tap.subscribe(onNext: {
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        btnDone.rx.tap.subscribe(onNext: {
            self.selectedCategory.onNext(self.categories)
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        setCategoryData()
        
        print(tempCategory)
        
        var count = 0
        
        let item = tempCategory.map { $0.subCategories.filter { $0.isSelected } }
        
        item.forEach { if ($0.map { $0.isSelected }.contains(true)) {
                count += 1
            }
        }
        
        if count > 0 {
            self.categories = tempCategory
            self.tableView.reloadData()
        }
    }
    
    func setCategoryData(_ flag: Bool = false) {
        let item1 = Category(id: "", name: "All", isSelected: flag, subCategories: [])
        let item2 = Category(id: "1", name: "Nightlife", isSelected: flag, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_big_nightclubs_select_category"), name: "Nightclubs", isSelected: flag)])
//        let item2 = Category(id: "1", name: "Nightlife", isSelected: flag, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_nighclub_home_popup"), name: "Nightclubs", isSelected: flag),
//            SubCategories(id: "2", categoryImage: #imageLiteral(resourceName: "ic_club_crawls_home_popup"), name: "Club Crawls", isSelected: flag)])
        let item3 = Category(id: "2", name: "Festival", isSelected: flag, subCategories: [SubCategories(id: "3", categoryImage: #imageLiteral(resourceName: "ic_gitar_mini_select_category"), name: "Music Festival", isSelected: flag),
                                                                                               SubCategories(id: "4", categoryImage: #imageLiteral(resourceName: "ic_street_festivals_home_popup"), name: "Street Festival", isSelected: flag)])
        let item4 = Category(id: "3", name: "Endurance Events", isSelected: flag, subCategories: [SubCategories(id: "5", categoryImage:#imageLiteral(resourceName: "ic_big_endurance_select_category"), name: "Endurance Events", isSelected: flag), SubCategories(id: "6", categoryImage:#imageLiteral(resourceName: "ic_big_vocal_events_select_category"), name: "Local Events", isSelected: flag)])
        categories = [item1, item2, item3, item4]
    }
}

extension CategoryListPopupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories[section].subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategorySubTableViewCell
        cell.selectionStyle = .none
        let item = categories[indexPath.section].subCategories[indexPath.row]
        cell.imgCategory.image = item.categoryImage
        cell.lblSubCategory.text = item.name
        cell.btnCheck.isSelected = item.isSelected
        
//        cell.btnCheck.rx.tap.subscribe(onNext: {
//            self.categories[indexPath.section].subCategories[indexPath.row].isSelected = !self.categories[indexPath.section].subCategories[indexPath.row].isSelected
//
//            let temp = self.categories[indexPath.section].subCategories.map { $0.isSelected }.filter { $0 }
//
//            print(temp)
//
//            if temp.count == self.categories[indexPath.section].subCategories.count {
//                let btn = UIButton()
//                btn.tag = indexPath.section
//                self.btnHeaderCheckTapped(btn)
//            }
//            self.tableView.reloadData()
//        }).disposed(by: cell.disposeBag)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.categories[indexPath.section].subCategories[indexPath.row].isSelected = !self.categories[indexPath.section].subCategories[indexPath.row].isSelected
        
        let temp = self.categories[indexPath.section].subCategories.map { $0.isSelected }.filter { $0 }
    
        if temp.count == self.categories[indexPath.section].subCategories.count {
            let btn = UIButton()
            btn.tag = indexPath.section
            btnHeaderCheckTapped(btn)
        } else {
            self.categories[indexPath.section].isSelected = false
        }
        
        let categoriesArray = self.categories.map { $0.isSelected }.filter { $0 }
        
        print(categoriesArray.count)
        
        /*if categoriesArray.count == 3 || categoriesArray.count == 4 {
            categories[0].isSelected = true
        } else {
            categories[0].isSelected = false
        }*/
        //TODO:- JD's Changes
        
        if categoriesArray.count == 4 {
            categories[0].isSelected = true
        } else {
            categories[0].isSelected = false
        }
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! CategoryHeaderTableViewCell
        cell.lblCategory.text = categories[section].name
        cell.btnCheck.isSelected = categories[section].isSelected
        cell.btnHeader.tag = section
        cell.btnHeader.addTarget(self, action: #selector(btnHeaderCheckTapped(_:)), for: .touchUpInside)
        return cell.contentView
    }
    
    @objc func btnHeaderCheckTapped(_ sender: UIButton) {
        let section = sender.tag
        
        if section == 0 {
            setCategoryData(!self.categories[section].isSelected)
        } else {
            self.categories[section].isSelected = !self.categories[section].isSelected
            
            var subCate = self.categories[section].subCategories
            for i in 0..<subCate.count {
                subCate[i].isSelected = self.categories[section].isSelected
            }
            self.categories[section].subCategories = subCate
            
            let categoriesArray = self.categories.map { $0.isSelected }.filter { $0 }
            
            print(categoriesArray.count)
            
            if categoriesArray.count == 3 || categoriesArray.count == 4 {
                categories[0].isSelected = true
            } else {
                categories[0].isSelected = false
            }
            //TODO: JD's Changes
            
          /*  if categoriesArray.count == 3 {
                self.categories[0].isSelected = true
            }
            else
            {
               categories[0].isSelected = false
            }*/
            
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
}
