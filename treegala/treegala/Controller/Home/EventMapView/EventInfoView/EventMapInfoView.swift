//
//  EventMapInfoView.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation

class EventMapInfoView: UIView {
    
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    static func loadFromNib() -> UIView {
        return UINib(nibName: "EventMapInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
}
