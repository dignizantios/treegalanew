
//
//  EventMapViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import MXScroll

class EventMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var eventList: [JSON] = []
    
    var offset = 0
    var filterData: FilterData!
    var filterJSON = JSON()
    var param: [String : String] = [:]
    var eventType: EventType = .nightClub
    var paramObserver = PublishSubject<[String : String]>()
    
    @IBOutlet weak var btnFilter: UIButton!

    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(param)
        
        mapView.delegate = self

        let button = setLogoTitleView()
        button.addTarget(self, action: #selector(logoClick), for: .touchUpInside)
        setSideMenu()
        
        let listbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_list_view_header_white"), style: .plain, target: self, action: #selector(btnListTapped))
        self.navigationItem.rightBarButtonItem = listbutton
        
        setMarker(eventList, isFirstLoad: true)
        
        btnFilter.rx.tap.subscribe(onNext: {
            let vc = SB.home.eventFilterViewController()!
            vc.selectedCategory = self.filterData.category
            vc.filterJSON = self.filterJSON
            vc.filterObserver.subscribe(onNext: { (filterData) in
                print(filterData)
                self.offset = 0
                self.filterJSON = filterData
                self.getEventList(filterParam: filterData)
            }).disposed(by: self.disposeBag)
            vc.filtercategory.subscribe(onNext: { (category) in
                self.filterData.category = category
            }).disposed(by: self.disposeBag)
            vc.resetFilter.subscribe(onNext: {
                self.filterJSON = JSON()
            }).disposed(by: self.disposeBag)
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParentViewController {
            paramObserver.onNext(param)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.getEventList()
    }
    
    @objc func logoClick() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @objc func btnListTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setMarker(_ list: [JSON], isFirstLoad: Bool = false) {
        
       // mapView.clear()
        
        var markers: [GMSMarker] = []
        
        for i in 0..<list.count {
            let item = list[i]
            let marker = GMSMarker()
            print(item["distance"].stringValue)
            marker.position = CLLocationCoordinate2D(latitude: item["latitude"].doubleValue, longitude: item["longtitude"].doubleValue)
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: -0.1)
            //marker.tracksViewChanges = true
            marker.tracksInfoWindowChanges = true
            marker.icon = #imageLiteral(resourceName: "ic_pin_map_screen")
            marker.userData = i
            markers.append(marker)
            marker.map = mapView
        }
        
        if isFirstLoad {
            let bounds = markers.reduce(GMSCoordinateBounds()) {
                $0.includingCoordinate($1.position)
            }
            mapView.animate(with: .fit(bounds, withPadding: 30.0))
        }
    }
    
    func getEventList(filterParam: JSON? = nil) {
        
        if let filterParam = filterParam {
            for i in filterParam {
                param[i.0] = i.1.stringValue
            }
        }
        
        param["offset"] = "\(offset)"
        
        print(JSON(param))
        
        if offset == -1 {
            return
        }
        
        let kURL = webUrls.baseUrl() + webUrls.getEventList()
        
        GeneralRequest.makeRequest(kURL, param: param, context: self, isShowLoader: false) { (json) in
            print(json)
            
            if json["flag"].exists() {
                if json["flag"].stringValue == "1" {
                    
                    if self.offset == 0 {
                        self.eventList = []
                    }
                    
                    self.offset = json["next_offset"].intValue
                    
                    if filterParam != nil {
                        self.eventList = json["data"].arrayValue
                        self.mapView.clear()
                    } else {
                        self.eventList.append(contentsOf: json["data"].arrayValue)
                    }

                    print("Count \(self.eventList.count)")
                    self.setMarker(json["data"].arrayValue, isFirstLoad: filterParam != nil)
                    self.param["offset"] = "\(self.offset)"
                    self.getEventList(filterParam: filterParam)
                } else {
                    self.eventList = []
                }
            } else {
                self.eventList = []
            }
        }
    }
}


// MARK: - MapView Delegate method
extension EventMapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let infoWindow = EventMapInfoView.loadFromNib() as! EventMapInfoView
        if let tag = marker.userData as? Int {
            let item = eventList[tag]
            infoWindow.imgEvent.sd_setShowActivityIndicatorView(true)
            infoWindow.imgEvent.sd_setImage(with: item["listing_image"].stringValue.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_image_placeholder"), options: .highPriority, completed: nil)
            infoWindow.lblName.text = item["title"].stringValue
            infoWindow.lblAddress.text = item["location"].stringValue
        }
        return infoWindow
    }

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("Window Tapped")
        print(marker.userData)
        
        if let tag = marker.userData as? Int {
            let item = eventList[tag]
           /* let vc = SB.eventDetail.eventDetailViewController()!
            vc.eventType = EventType.type(index: item["category_id"].intValue - 1)
            EventDetailViewController.listID = item["listing_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)*/
            eventType = EventType.type(index: item["category_id"].intValue - 1)
            getEventDetailData(eventId: item["listing_id"].stringValue ?? "0",                               tag: tag)
        }
    }
}

extension EventMapViewController
{
    func jumpToMixVC(json:JSON) {
        
        let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
        let headerScroll = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetail") as! NewEventDetail
        
        var hh:UIViewController!
        hh = headerScroll
        headerScroll.dictData = json
        
        // var child1 : UIViewController!
        // var child2 :  UIViewController!
        
        let child1 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child1.dictEventDetailData = json
        
        let child2 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child2.dictEventDetailData = json
        
        let child3 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child3.dictEventDetailData = json
        child3.selectedIndex = 10
        
        let child4 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child4.dictEventDetailData = json
        child4.selectedIndex = 11
        
        var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
        switch eventType {
        case .nightClub:
            child1.selectedIndex = 0 // Headline
            
            child2.selectedIndex = 1 //event
            
            break
        case .clubCrawl:
            headerTitles = ["HEADLINES", "DETAILS", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 2 //clubcrawl
            
            child2.selectedIndex = 3 //musiclineup
            break
        case .music:
            headerTitles = ["HEADLINES", "LINEUP", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 4 //musicheadline
            
            child2.selectedIndex = 5 //musiclineup
            break
        case .street:
            child1.selectedIndex = 6 //streethealine
            
            child2.selectedIndex = 7 //nightevent
            break
        case .obstacle:
            headerTitles = ["HEADLINES", "RACE DETAILS", "DESCRIPTION", "RATINGS"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        case .local:
//            headerTitles = ["HEADLINES", "DESCRIPTION", "RATINGS"]
            headerTitles = ["HEADLINES", "DESCRIPTION"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        }
        let segment = MSSegmentControl(sectionTitles: headerTitles)
        setupSegment(segmentView: segment)
        
        let mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child2,child3,child4], segmentView: segment)
        mx.headerViewOffsetHeight = 0
        mx.view.backgroundColor = color.themePurple()
        mx.navigationItem.titleView = setMXLogoTitle()
        mx.extendedLayoutIncludesOpaqueBars = true
        mx.shouldScrollToBottomAtFirstTime = false
        mx.setBackButton()
        navigationController?.pushViewController(mx, animated: true)
        
    }
    
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor = color.themePurple()
        segmentView.selectionIndicatorColor = .white
        segmentView.selectionIndicatorHeight = 3
        let Segment_statu_titleNormal = UIFont(name: "UbuntuCondensed-Regular", size: 13)!
        segmentView.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Segment_statu_titleNormal]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 2
        segmentView.selectionStyle = .fullWidth
    }
}

//MARK:- API

extension EventMapViewController
{
    func getEventDetailData(eventId:String,tag:Int) {
        
        let kURL = webUrls.baseUrl() + webUrls.getEventData()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "listing_id" : eventId]
        
        print(param)
        
        startLoader()
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            print(json)
            if json["flag"].stringValue == "1" {
                
                let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
                let headerScroll: MXHomeViewController = eventStoryboard.instantiateViewController(withIdentifier: "MXHomeViewController") as! MXHomeViewController
                headerScroll.json = json["data"]
                headerScroll.eventType = self.eventType
                self.navigationController?.pushViewController(headerScroll, animated: true)
               // self.jumpToMixVC(json: json["data"])
                //self.dictEventDetailData = json["data"]
                // self.tblEventDetail.reloadData()
            }
        }
    }
}

