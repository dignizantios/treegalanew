//
//  ForgotViewController.swift
//  treegala
//
//  Created by PC on 7/25/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class ForgotViewController: ParentViewController {
    
    // MARK: - Variables | Properties
    @IBOutlet private weak var txtEmail: UITextField!
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnSubmit_Action(_ sender: Any) {
        
        self.view.endEditing(true)
        let str = txtEmail.text ?? ""
        
        if ((txtEmail.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            showAlert("Please enter email or username")
            return
        }
        else if str.isValidEmailAddress() == false
        {
            showAlert("Please enter valid address")
            return
        }
        
        /*if !txtEmail.text!.trimmed().isAlphanumeric() {
            showAlert("Please enter email or username")
            return
        }*/
        
        let kURL = webUrls.baseUrl() + webUrls.forgotPassword()
        
        let param = ["username" : txtEmail.text!.trimmed(),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0"]
        
        print(param)
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.popupAlert(title: "Treegala", message: json["msg"].stringValue, actionTitles: ["OK"], actions: [ { _ in
                    self.dismiss(animated: true, completion: nil)
                    }])
            }
        }
    }
    
    
}
