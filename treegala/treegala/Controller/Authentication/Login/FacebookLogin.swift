//
//  FacebookLogin.swift
//  givbio
//
//  Created by om on 7/2/18.
//  Copyright © 2018 dignizant. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import SwiftyJSON

struct FacebookService {
    
    func login(_ from: UIViewController, completion: @escaping (Bool) -> ()) {
        let loginPermission = LoginManager()
        loginPermission.logIn(permissions: ["public_profile","email"], from: from, handler: { (result, error) in
            
            guard error == nil else { return }
            
            if(result?.isCancelled)! {
                print("Login Cancelled")
            } else {
                print("Loogin Success")
                print("result:=\(result)")
                if (result?.token) != nil {
                    completion(true)
                }
            }
        })
    }
    
    func getFBResult(completion: @escaping (JSON) -> ()) {
        
        guard AccessToken.current != nil else { return }
        
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender, link"]).start(completionHandler: { (connection, result, error) -> Void in
            
            guard error == nil else { return }
            
            if let resultData = result {
                let json = JSON(resultData)
                let profileUrl = json["picture"]["data"]["url"].string
                completion(json)

            }
        })
    }
}
