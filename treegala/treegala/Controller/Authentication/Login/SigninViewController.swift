//
//  SigninViewController.swift
//  treegala
//
//  Created by PC on 7/25/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import TwitterKit
import RxSwift

class SigninViewController: ParentViewController {
    
    @IBOutlet private weak var txtEmail: UITextField!
    @IBOutlet private weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnShowPassword: UIButton!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtPassword.rx.text
            .map {
                text -> Bool in
                (text!.trimmed().isEmpty)
            }.bind(to: btnShowPassword.rx.isHidden)
            .disposed(by: disposeBag)
    }
    
    func setupUI() {
        
    }
}


//MARK: - Action
extension SigninViewController {
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = !sender.isSelected
        let str = txtPassword.text
        txtPassword.text = ""
        txtPassword.text = str
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        if isLoginPresent {
            isLoginPresent = false
            self.dismiss(animated: true, completion: nil)
        } else {
            appDelegate.setupSideMenu()
        }
    }
    
    @IBAction func onBtnForgotPassword_Action(_ sender: Any) {
        
        let vc = SB.signin.forgotViewController()!
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onBtnSignin(_ sender: Any) {
        
        if txtEmail.text!.trimmed().isEmpty {
            showAlert(messages.enterUsername())
            return
        }
        
        if txtPassword.text!.trimmed().isEmpty {
            showAlert(messages.enterPassword())
            return
        }
        
        let kURL = webUrls.baseUrl() + webUrls.loginURL()
        
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        
        let param = ["username" : txtEmail.text!.trimmed(),
                     "password" : txtPassword.text!.trimmed(),
                     "device_token" : uuid,
                     "register_id" : Messaging.messaging().fcmToken ?? "",
                     "device_type" : "1",
                     "lang" : "0"]
        
        startLoader()
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            
            if json["flag"].stringValue == "1" {
                guard let rawData = try? json["data"].rawData() else { return }
                
                UserDefaults.standard.setValue(rawData, forKey: R.string.keys.userDetail())
                UserDefaults.standard.set(true, forKey: R.string.keys.isUserLogin())

                if isLoginPresent {
                    isLoginPresent = false
                    self.dismiss(animated: true, completion: nil)
                } else {
                    appDelegate.setupSideMenu()
                }
            } else {
                self.showAlert(json["msg"].stringValue)
            }
        }
    }
    
    @IBAction func onBtnFaceBookAction(_ sender: Any) {
        FacebookService().login(self, completion: { (status) in
            if status {
                self.startLoader()
                FacebookService().getFBResult(completion: { (json) in
                    
                    print(json)
                    self.sendFacebook(json)
                })
            }
        })
    }
    
    @IBAction func btnTwitterTapped(_ sender: UIButton) {
        loginwithTwitter()
    }
    
    
    func getTwitteData() {
        TWTRTwitter.sharedInstance().logIn(with: self) { (session, error) in
            print(session?.userName)
            guard session != nil else { return }

            let client = TWTRAPIClient(userID: session!.userName)
            client.loadUser(withID: session!.userID, completion: { (user, userError) in
                if let user = user {
                   // self.sendTwitter(user)
                }
            })
        }
    }
    
    @IBAction func onBtnSignUp_Action(_ sender: Any) {
        let signupViewcontroller = SignUpViewController.instantiateFromAppStoryboard(appStoryboard: .signin)
        self.navigationController?.pushViewController(signupViewcontroller, animated: true)
    }
    
}

//MARK:- Twitter Login

extension SigninViewController{
    
    func loginwithTwitter()
    {
        
        let client = TWTRAPIClient.withCurrentUser()
        // Swift
        
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            
            print("session \(session)")
            
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))")
                print("session \(session)")
                
                client.requestEmail { email, error in
                    if (email != nil) {
                        print("signed in as \(email ?? "")")
                        print("user id \(TWTRTwitter.sharedInstance().sessionStore.session()!.userID)")
                        
                        
                        self.consultarImagePerfil(strUserID: TWTRTwitter.sharedInstance().sessionStore.session()!.userID, strName: (session?.userName)!, completion:  { (result) in
                            if let tweets = result as? [String: AnyObject] {
                                print("tweets \(tweets)")
                                var dictData = JSON()
                                if let a:String = tweets["name"] as? String {
                                    print("Name: ",a)
                                    let arrFullName = a.components(separatedBy: " ")
                                    if arrFullName.count >= 2
                                    {
                                        dictData["first_name"] = JSON(arrFullName[0] as? String ?? "")
                                        dictData["last_name"] = JSON(arrFullName[1] as? String ?? "")
                                    }
                                    else{
                                        if arrFullName.count != 0
                                        {
                                            dictData["first_name"] = JSON(arrFullName[0] as? String ?? "")
                                        }
                                    }
                                }
                                if let a:String = tweets["screen_name"] as? String {
                                    print("ScreenName: ",a)
                                    dictData["username"] = JSON(a)
                                }
                                var ProfileImage = String()
                                if let a:String = tweets["profile_image_url"] as? String {
                                    print("Profile Image ",a)
                                    ProfileImage = a
                                    dictData["profile_image"] = JSON(ProfileImage)
                                }
                                let userId = "\(TWTRTwitter.sharedInstance().sessionStore.session()!.userID)"
                                let emailId = email ?? ""
                                dictData["email"] = JSON(emailId)
                                dictData["id"] = JSON(userId)
                            
                                self.sendTwitter(dictData)
                                //self.checkTwitterUserAvailable(userId:userId,email : emailId,userName: (session?.userName)!, userProfile: ProfileImage)
                            }
                        })
                        
                        
                        //self.gotTwiiterUserData(email : email!,username : session?.userName)
                        
                    } else {
                        print("error: \(String(describing: session?.userName))")
                    }
                }
                
            } else {
                print("error: \(String(describing: error?.localizedDescription))")
            }
        })
    }
    
    func consultarImagePerfil(strUserID: String,strName:String,completion: @escaping (_ result: [String : AnyObject])->()) {
        //let client = TWTRAPIClient()
        let client = TWTRAPIClient(userID: strUserID)
        let statusesShowEndpoint = "https://api.twitter.com/1.1/users/show.json"
        let params = ["screen_name" : strName]
        var clientError : NSError?
        let request = client.urlRequest(withMethod: "GET", urlString: statusesShowEndpoint, parameters: params, error: &clientError)
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            do {
                if data != nil
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    print("json: \(json)")
                    completion (json as! [String : AnyObject])
                }
            } catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
    }
}


//MARK: - Facebook Login
extension SigninViewController {
    func sendFacebook(_ data: JSON) {
        
        let kURL = webUrls.baseUrl() + webUrls.socialLoginURL()
        
        let param = ["oauth_id" : data["id"].stringValue,
                     "firstname" : data["first_name"].stringValue,
                     "lastname" : data["last_name"].stringValue,
                     "email" : data["email"].stringValue,
                     "provider" : "facebook",
                     "timezone" : TimeZone.current.identifier,
                     "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                     //"picture" : data["picture"]["data"]["url"].stringValue,
                     "register_id" : Messaging.messaging().fcmToken ?? "",
                     "device_type" : "1",
                     "lang" : "0"]
        
        print(param)
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            if json["flag"].stringValue == "1" {
                guard let rawData = try? json["data"].rawData() else { return }
                
                UserDefaults.standard.set(rawData, forKey: R.string.keys.userDetail())
                UserDefaults.standard.set(true, forKey: R.string.keys.isUserLogin())
                
                appDelegate.setupSideMenu()
            } else {
                self.showAlert(json["msg"].stringValue)
            }
            
        }
    }
    
   /* func sendTwitter(_ data: TWTRUser) {
        
        let kURL = webUrls.baseUrl() + webUrls.socialLoginURL()
        
        let param = ["oauth_id" : data.userID,
                     "firstname" : data.name,
                     "lastname" : data.screenName,
                     "email" : data.name,
                     "provider" : "twitter",
                     "timezone" : TimeZone.current.identifier,
                     "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                     //"picture" : data["picture"]["data"]["url"].stringValue,
            "register_id" : Messaging.messaging().fcmToken ?? "",
            "device_type" : "1",
            "lang" : "0"]
        
        print(param)
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            if json["flag"].stringValue == "1" {
                
                guard let rawData = try? json["data"].rawData() else { return }
                
                UserDefaults.standard.set(rawData, forKey: R.string.keys.userDetail())
                UserDefaults.standard.set(true, forKey: R.string.keys.isUserLogin())
                
                appDelegate.setupSideMenu()
            } else {
                self.showAlert(json["msg"].stringValue)
            }
            
        }
    }*/
    
    func sendTwitter(_ dict: JSON) {
        
        let kURL = webUrls.baseUrl() + webUrls.socialLoginURL()
        print("dict \(dict)")
        let param = ["oauth_id" : dict["id"].stringValue,
                     "firstname" :  dict["first_name"].stringValue,
                     "lastname" :  dict["last_name"].stringValue,
                     "email" :  dict["email"].stringValue,
                     "provider" : "twitter",
                     "timezone" : TimeZone.current.identifier,
                     "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                     //"picture" : data["picture"]["data"]["url"].stringValue,
            "register_id" : Messaging.messaging().fcmToken ?? "",
            "device_type" : "1",
            "lang" : "0"]
        
        print(param)
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            if json["flag"].stringValue == "1" {
                
                guard let rawData = try? json["data"].rawData() else { return }
                
                UserDefaults.standard.set(rawData, forKey: R.string.keys.userDetail())
                UserDefaults.standard.set(true, forKey: R.string.keys.isUserLogin())
                
                appDelegate.setupSideMenu()
            } else {
                self.showAlert(json["msg"].stringValue)
            }
            
        }
    }
}

class SigninNavigationController: UINavigationController {
    
}
