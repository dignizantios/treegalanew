//
//  SignUpViewController.swift
//  treegala
//
//  Created by PC on 7/25/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import Firebase
import RxSwift

class SignUpViewController: ParentViewController {
    
    @IBOutlet private weak var txtUserName: UITextField!
    @IBOutlet private weak var txtEmail: UITextField!
    @IBOutlet private weak var txtPassword: UITextField!
    @IBOutlet private weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var btnPasswordShow: UIButton!
    @IBOutlet weak var btnConPasswordShow: UIButton!
    
    var disposeBag = DisposeBag()
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPassword.rx.text
            .map {
                text -> Bool in
                (text!.trimmed().isEmpty)
            }.bind(to: btnPasswordShow.rx.isHidden)
            .disposed(by: disposeBag)
        
        txtConfirmPassword.rx.text
            .map {
                text -> Bool in
                (text!.trimmed().isEmpty)
            }.bind(to: btnConPasswordShow.rx.isHidden)
            .disposed(by: disposeBag)
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 0 {
            txtPassword.isSecureTextEntry = !sender.isSelected
            let str = txtPassword.text
            txtPassword.text = ""
            txtPassword.text = str
        } else {
            txtConfirmPassword.isSecureTextEntry = !sender.isSelected
            let str = txtConfirmPassword.text
            txtConfirmPassword.text = ""
            txtConfirmPassword.text = str
        }
    }
    
    func validateInput() -> Bool {
        
        if txtUserName.text!.trimmed().isEmpty {
            showAlert(messages.pleaseEnterUserName())
            return false
        } else if !txtUserName.text!.isAlphanumeric() {
            showAlert(messages.enterValidUsername())
            return false
        }
        
        if txtEmail.text!.trimmed().isEmpty {
            showAlert(messages.enterEmail())
            return false
        } else if !txtEmail.text!.isValidEmail() {
            showAlert(messages.enterValidEmail())
            return false
        }
        
        if txtPassword.text!.trimmed().isEmpty {
            showAlert(messages.enterPassword())
            return false
        }
        
        if txtConfirmPassword.text!.trimmed().isEmpty {
            showAlert(messages.enterConfirmPassword())
            return false
        }
        
        if txtPassword.text != txtConfirmPassword.text {
            showAlert(messages.passwordNotMatch())
            return false
        }
        
        return true
    }
    
    @IBAction func onBtnSignup_Action(_ sender: Any) {
        if validateInput() {
            let kURL = webUrls.baseUrl() + webUrls.registerURL()
            
            let param = ["username" : txtUserName.text!.trimmed(),
                         "email" : txtEmail.text!.trimmed(),
                         "password" : txtPassword.text!.trimmed(),
                         "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                         "timezone" : TimeZone.current.identifier,
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : "1",
                         "lang" : "0"]
            
            startLoader()
            GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                self.dismissLoader()
                if json["flag"].stringValue == "1" {
                    print(json)
                    guard let rawData = try? json["data"].rawData() else { return }
                    UserDefaults.standard.setValue(rawData, forKey: R.string.keys.userDetail())
                    UserDefaults.standard.set(true, forKey: R.string.keys.isUserLogin())
                    
                    if isLoginPresent {
                        isLoginPresent = false
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        appDelegate.setupSideMenu()
                    }
                } else {
                    self.showAlert(json["msg"].stringValue)
                }
            })
        }
    }
    
    @IBAction func onBtnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
