//
//  SuggestionViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class SuggestionViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtComment: KMPlaceholderTextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Suggestions?"
        
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(logoClick))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
    @objc func logoClick() {
        NotificationCenter.default.post(name: NSNotification.Name.init("loadHome"), object: nil)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if txtEmail.text!.trimmed().isEmpty {
            showAlert(messages.enterEmail())
            return
        } else if !txtEmail.text!.trimmed().isValidEmail() {
            showAlert(messages.enterValidEmail())
            return
        }
        
        if txtComment.text!.trimmed().isEmpty {
            showAlert("Please enter comment")
            return
        }
        
        let kURL = webUrls.baseUrl() + webUrls.appSuggestion()
        
        let param = ["email_id" : txtEmail.text!.trimmed(),
                     "comment" : txtComment.text!.trimmed(),
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            if json["flag"].stringValue == "1" {
                self.txtEmail.text = ""
                self.txtComment.text = ""
                self.showAlert(json["msg"].stringValue)
            }
        }
    }
}

class SuggestionNavigationController: UINavigationController {
    
}
