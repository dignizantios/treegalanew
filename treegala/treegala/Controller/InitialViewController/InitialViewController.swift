//
//  InitialViewController.swift
//  treegala
//
//  Created by PC on 7/30/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import Firebase

class InitialViewController: ParentViewController {

    var timerControl: Timer?
    var currentIndex = 0
    
    @IBOutlet private weak var pageControl: UIPageControl!
    
    func setupUI() {
        pageControl.numberOfPages = 4
        pageControl.currentPage = 0
    }
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isUserLogin() {
            generateToken()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTimer()
    }
    
    func updateUIPageControlSize(scaleX:CGFloat, scaleY:CGFloat) {
        pageControl.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
    }
    
    func updatePageControlIndex(index:Int) {
        pageControl.currentPage = index
    }
    
    func setupTimer() {
        if timerControl == nil {
            currentIndex = 1
            timerControl = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimer() {
        print("currentIndex:=\(currentIndex)")
        updatePageControl(index: currentIndex)
        currentIndex += 1
    }
    
    func generateToken() {
        
        if Rechability.isConnectedToNetwork() {
            let url = webUrls.baseUrl() + webUrls.generateAccessToken()
            
            let param = ["device_token" : UIDevice.current.identifierForVendor!.uuidString,
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : "1",
                         "lang" : "0",
                         "username" : "treegala",
                         "password" : "UzuyQh,Sb+g9754S"]
            
            print(param)
            GeneralRequest.makeRequest(url, param: param, context: self, isShowLoader: false) { (json) in
                print("-----------------------")
                print("Response")
                print(json)
                guard let rawData = try? json.rawData() else { return }
                UserDefaults.standard.setValue(rawData, forKey: R.string.keys.guestDetail())
                appDelegate.setupSideMenu()
            }
        } else {
            showAlert(keys.noInternet())
            timerControl?.invalidate()
            timerControl = nil
        }
    }

    func loadSigninViewController() {
        
        let snap = UIImageView(image: AppDelegate.shared.window?.takeSnap())
        snap.frame = UIScreen.main.bounds
        
        let signinViewController = SigninViewController.instantiateFromAppStoryboard(appStoryboard: .signin)
        signinViewController.view.addSubview(snap)
        
        let signinNavigationController = SigninNavigationController.instantiateFromAppStoryboard(appStoryboard: .signin)
        AppDelegate.shared.window?.rootViewController = signinNavigationController
        AppDelegate.shared.window?.makeKeyAndVisible()
        
        UIView.animate(withDuration: 0.5, animations: {
            snap.frame = CGRect(x: UIScreen.main.bounds.size.width, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) { (completed) in
            snap.removeFromSuperview()
        }
    }
    
    // MARK:- Other Method
    func updatePageControl(index:Int) {
        updatePageControlIndex(index: index)
        if index == 4 {
            timerControl?.invalidate()
            timerControl = nil
            if isUserLogin() {
                appDelegate.setupSideMenu()
            }
        }
    }

}
