//
//  EventDetailViewController.swift
//  treegala
//
//  Created by om on 8/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import CarbonKit
import RxSwift
import SwiftyJSON
import AVKit
import SDWebImage

class EventDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imgSlider: KIImagePager!
    @IBOutlet weak var lblEventName: UILabel!
    
    @IBOutlet weak var widthOfClaim: NSLayoutConstraint!
    @IBOutlet weak var btnClaim: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var lblEventLocation: UILabel!
    
    @IBOutlet weak var topOfView: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet var headerButtons: [UIButton]!
    @IBOutlet var lblHeader: [UILabel]!
    
    @IBOutlet weak var carbonView: UIView!
    @IBOutlet weak var heightOfCarbon: NSLayoutConstraint!
    
    weak var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
    var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
    
    var eventImages: [Any] = [] {
        didSet {
            [btnBack,btnForward].forEach { $0?.isHidden = eventImages.count < 2 }
        }
    }
    
    var sliderCount = 0 {
        didSet {
            btnBack.isHidden = sliderCount == 0
            btnForward.isHidden = sliderCount == (eventImages.count - 1)
        }
    }
    
    var eventType: EventType = .nightClub
    
    var favoriteObserver = PublishSubject<Bool>()
    var disposeBag = DisposeBag()
    
    static var listID = ""
    
    static var heightOfView: CGFloat = 0.0
    
    var eventDetail: JSON?
    
    var otherVideo: YKDirectVideo?
    var youtubeVideo: YKYouTubeVideo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        btnFavorite.rx.tap.subscribe(onNext: {
            
            if isUserLogin() {
                let kURL = webUrls.baseUrl() + webUrls.eventFavorite()
                
                let param = ["user_id" : getUserDetail("user_id"),
                             "access_token" : getUserDetail("access_token"),
                             "listing_id" : EventDetailViewController.listID,
                             "lang" : "0",
                             "timezone" : TimeZone.current.identifier ]
                
                GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                    print(json)
                    if json["flag"].stringValue == "1" {
                        self.btnFavorite.isSelected = json["data"]["is_favourite"].boolValue
                    }
                })
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)
        
        btnClaim.rx.tap.subscribe(onNext: {
            if isUserLogin() {
                if let status = self.eventDetail?["is_claimed_by"].stringValue, status == "0" {
                    let vc = ClaimSubmitPopup()
                    vc.claimStatusObserver.subscribe(onNext: { (status) in
                        if status {
                            self.btnClaim.setTitle(" Requested", for: .normal)
                            self.widthOfClaim.constant = 100
                        }
                    }).disposed(by: self.disposeBag)
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)
        
        btnForward.rx.tap.subscribe(onNext: {
            print(self.sliderCount)
            if self.sliderCount < self.eventImages.count - 1 {
                self.sliderCount += 1
                let index = UInt(self.sliderCount)
                print(index)
                self.imgSlider.setCurrentPage(index, animated: true)
            }
        }).disposed(by: disposeBag)
        
        btnBack.rx.tap.subscribe(onNext: {
            print(self.sliderCount)
            if self.sliderCount > 0 {
                self.sliderCount -= 1
                let index = UInt(self.sliderCount)
                self.imgSlider.setCurrentPage(index, animated: true)
            }
        }).disposed(by: disposeBag)
        
        getEventDetailData()
    }
    
    
    @objc func logoClick() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            self.favoriteObserver.onNext(btnFavorite.isSelected)
        }
    }
    
    override func viewWillLayoutSubviews() {
        print("Height of view")
        print(topView.bounds.height)

        EventDetailViewController.heightOfView = topView.bounds.height
    }
    
    @IBAction func btnDummyTapped(_ sender: UIButton) {
        
        let tag = sender.tag
        
        lblHeader.forEach { $0.isHidden = true }
        lblHeader[tag].isHidden = false
        carbonTabSwipeNavigation?.currentTabIndex = UInt(tag)
        carbonTabSwipeNavigation(carbonTabSwipeNavigation!, willMoveAt: UInt(tag))
    }
    
    func setupUI() {
        
        eventImages = []
        sliderCount = 0
        
        setBackButton()
        setGradientBar()
        scrollView.delegate = self
        
        imgSlider.slideshowTimeInterval = UInt(0.0)
        imgSlider.indicatorDisabled = false
        imgSlider.imageCounterDisabled = true
        imgSlider.hidePageControlForSinglePages = true
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        button.addTarget(self, action: #selector(self.logoClick), for: .touchUpInside)
        navigationItem.titleView = button
        
        setCategoryUI()
    }
    
    func setupCarbonView() {
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: headerTitles, delegate: self)
        carbonTabSwipeNavigation?.toolbarHeight.constant = 50.0
        
        carbonTabSwipeNavigation?.setIndicatorHeight(2.0)
        carbonTabSwipeNavigation?.setIndicatorColor(.white)
        
        carbonTabSwipeNavigation?.setNormalColor(.white, font: font.ubuntuCondensedRegular(size: 16.0)!)
        carbonTabSwipeNavigation?.setSelectedColor(.white, font: font.ubuntuCondensedRegular(size: 16.0)!)
        
        let width = UIScreen.main.bounds.width / CGFloat(headerTitles.count)
        
        let widths = headerTitles.map { ($0.width(withConstrainedHeight: 20, font: font.ubuntuCondensedRegular(size: 17)!) + 10) }
        print(widths)
        print(UIScreen.main.bounds.width )
        let temp = widths.reduce(0.0, { $0 + $1 })
        print(temp)
        
        for i in 0..<headerTitles.count {
            let str = headerTitles[i]
            if temp >= UIScreen.main.bounds.width {
                let width = str.width(withConstrainedHeight: 20, font: font.ubuntuCondensedRegular(size: 17)!) + 10
                carbonTabSwipeNavigation?.carbonSegmentedControl?.setWidth(width, forSegmentAt: i)
                carbonTabSwipeNavigation?.carbonTabSwipeScrollView.isScrollEnabled = true
            } else {
                carbonTabSwipeNavigation?.carbonSegmentedControl?.setWidth(width, forSegmentAt: i)
                carbonTabSwipeNavigation?.carbonTabSwipeScrollView.isScrollEnabled = false
            }
        }
        
        carbonTabSwipeNavigation?.toolbar.barTintColor = color.themePurple()
        carbonTabSwipeNavigation?.toolbar.isTranslucent = false
        carbonTabSwipeNavigation?.insert(intoRootViewController: self, andTargetView: self.carbonView)
        dummyView.backgroundColor = color.themePurple()
        carbonTabSwipeNavigation?.view.layoutIfNeeded()
        
        let btnDummy = UIButton()
        btnDummy.tag = 0
        btnDummyTapped(btnDummy)
    }
    
    func setCategoryUI() {
        switch eventType {
        case .nightClub:
            // self.title = "Nightclub"
            break
        case .clubCrawl:
            //self.title = "Club Crawls"
            headerTitles[1] = "DETAILS"
        case .music:
            // self.title = "Music Festival"
            headerTitles[1] = "LINEUP"
        case .street:
            //self.title = "Street Festival"
            break
        case .obstacle:
            //self.title = "Obstacles Event"
            headerTitles[1] = "RACE DETAILS"
        case .local:
            //self.title = "Local Event"
            headerTitles[1] = "DESCRIPTION"
        }
        
        headerButtons[0].setTitle(headerTitles[0], for: .normal)
        headerButtons[1].setTitle(headerTitles[1], for: .normal)
        headerButtons[2].setTitle(headerTitles[2], for: .normal)
        headerButtons[3].setTitle(headerTitles[3], for: .normal)
    }
    
    func setData(_ json: JSON) {
        lblEventName.text = json["title"].stringValue
        lblEventLocation.text = json["location"].stringValue
        
        self.widthOfClaim.constant = 75
        
        let status = json["is_claimed_by"].stringValue
        btnClaim.isHidden = true
        if status == "0" {
            btnClaim.isSelected = false
        } else if status == "-1" {
            btnClaim.isSelected = false
            btnClaim.setTitle(" Requested", for: .normal)
            self.widthOfClaim.constant = 100
            btnClaim.isUserInteractionEnabled = false
        } else {
            btnClaim.isSelected = true
            btnClaim.isUserInteractionEnabled = false
        }
        
        var imageArray: [Any] = []
        
        for i in json["images"].arrayValue {
            let imagePath = i["img_path"].stringValue
            if i["is_video"].stringValue == "2" {
                if imagePath.contains("youtube.com") {
                    let videoId = imagePath.components(separatedBy: "v=")[1]
                    let path = "https://img.youtube.com/vi/\(videoId)/0.jpg"
                    
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    SDWebImageManager.shared().imageDownloader?.downloadImage(with: path.toURL(), options: .highPriority, progress: nil, completed: { (image, data, error, status) in
                        imageArray[1] = self.createVideoView(object: image ?? #imageLiteral(resourceName: "ic_image_placeholder"))
                        self.eventImages = imageArray
                        self.imgSlider.reloadData()
                    })
                } else {
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    DispatchQueue.global().async {
                        let image = self.getThumbnailImage(forUrl: imagePath.toURL()!) ?? UIImage()
                        DispatchQueue.main.async {
                            imageArray[1] = self.createVideoView(object: image)
                            self.eventImages = imageArray
                            self.imgSlider.reloadData()
                        }
                    }
                }
            } else {
                imageArray.append(imagePath)
            }
        }
        
        print(imageArray)
        
        eventImages = imageArray
        imgSlider.reloadData()
        
        btnFavorite.isSelected = json["is_favourite"].boolValue
    }
    
    func getEventDetailData() {
        
        let kURL = webUrls.baseUrl() + webUrls.getEventData()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "listing_id" : EventDetailViewController.listID]
        
        print(param)
        
        startLoader()
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            print(json)
            if json["flag"].stringValue == "1" {
                self.eventDetail = json["data"]
                self.setData(json["data"])
                self.setupCarbonView()
            }
        }
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    func createVideoView(object: UIImage) -> UIImage {
        
        let bottomImage = object
        let topImage = #imageLiteral(resourceName: "ic_play_video")
        
        let size = CGRect(x: 0, y: 0, width: 300, height: 150)
        UIGraphicsBeginImageContext(size.size)
        
        let areaSize = CGRect(x: (size.width / 2 - 25), y: (size.height / 2 - 25), width: 50, height: 50)
        bottomImage.draw(in: size)
        
        topImage.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

extension EventDetailViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //print(scrollView.contentOffset.y)
       // print(EventDetailViewController.heightOfView)
        
        if EventDetailViewController.heightOfView <= scrollView.contentOffset.y {
            dummyView.isHidden = false
        } else {
            dummyView.isHidden = true
        }
    }
    
}

extension EventDetailViewController: CarbonTabSwipeNavigationDelegate, OffsetDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        if index == 0 {
            if eventType == .nightClub {
                let vc = SB.eventDetail.nightHeadlineViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.details = self.eventDetail
                return vc
            } else if eventType == .clubCrawl {
                let vc = SB.eventDetail.clubCrawlsHeadlineViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.details = self.eventDetail
                return vc
            } else if eventType == .music {
                let vc = SB.eventDetail.musicHeadlineViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.detail = self.eventDetail
                return vc
            } else if eventType == .street {
                let vc = SB.eventDetail.streetHeadlineViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.detail = self.eventDetail
                return vc
            } else if eventType == .obstacle {
                let vc = SB.eventDetail.obsctacleHeadlineViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.detail = self.eventDetail
                return vc
            }
        } else if index == 1 {
            if eventType == .nightClub {
                let vc = SB.eventDetail.nightEventViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                print(vc)
                print(eventDetail)
                vc.eventDetail = self.eventDetail
                return vc
            } else if eventType == .clubCrawl {
                let vc = SB.eventDetail.musicLineupViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.detail = self.eventDetail
                vc.isFromMusic = false
                vc.isImageData = false
                return vc
            } else if eventType == .music {
                let vc = SB.eventDetail.musicLineupViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.detail = self.eventDetail
                return vc
            } else if eventType == .street {
                let vc = SB.eventDetail.nightEventViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.eventDetail = self.eventDetail
                return vc
            } else if eventType == .obstacle {
                let vc = SB.eventDetail.obsctacleRaceDetailViewController()!
                vc.offsetDeledate = self
                vc.topConstraintConstant = -topOfView.constant
                vc.details = self.eventDetail
                return vc
            }
        } else if index == 2 {
            let vc = SB.eventDetail.descriptionSubViewController()!
            vc.offsetDeledate = self
            vc.topConstraintConstant = -topOfView.constant
            vc.descriptionDetail = self.eventDetail
            return vc
        } else {
            let vc = SB.eventDetail.ratingSubViewController()!
            vc.details = self.eventDetail
            vc.offsetDeledate = self
            vc.topConstraintConstant = -topOfView.constant
            return vc
        }
        
        let vc = SB.eventDetail.nightHeadlineViewController()!
        return vc
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {

        let tag = Int(index)
        
        lblHeader.forEach { $0.isHidden = true }
        lblHeader[tag].isHidden = false
        
        if index == 0 {
            
            if eventType == .nightClub {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  NightHeadlineViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .clubCrawl {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  ClubCrawlsHeadlineViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .music {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  MusicHeadlineViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .street {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  StreetHeadlineViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .obstacle {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  ObsctacleHeadlineViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            }
        } else if index == 1 {
            
            if eventType == .nightClub {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  NightEventViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .clubCrawl {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  MusicLineupViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .music {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  MusicLineupViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .street {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  NightEventViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            } else if eventType == .obstacle {
                if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  ObsctacleRaceDetailViewController {
                    vc.topConstraintConstant = -topOfView.constant
                }
            }
        } else if index == 2 {
            if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  DescriptionSubViewController {
                vc.topConstraintConstant = -topOfView.constant
            }
        } else if index == 3 {
            if let vc = carbonTabSwipeNavigation.viewControllers.object(forKey: index) as?  RatingSubViewController {
                vc.topConstraintConstant = -topOfView.constant
            }
        }
    }
    
    func offsetValueChanged(_ offset: CGFloat) {
        //print(offset)
       // self.topOfView.constant = -offset
          print("Offset Outside \(offset)")
        heightOfCarbon.constant = offset - 64
    }
}

extension EventDetailViewController: KIImagePagerDataSource, KIImagePagerDelegate {
    func array(withImages pager: KIImagePager!) -> [Any]! {
        
        return eventImages
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
        if image == 0 {
            return .scaleToFill
        }
        return .scaleAspectFill
    }
    
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
        
        sliderCount = Int(index)
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
        if index == 1 {
            if let detail = eventDetail {
                let array = detail["images"].arrayValue
                
                if array[1]["is_video"].stringValue == "2" {
                    
                    let imagePath = array[1]["img_path"].stringValue
                    print(imagePath)
                    if imagePath.contains("youtube.com") {
                        if let url = imagePath.toURL(), UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }
                        //                    youtubeVideo = YKYouTubeVideo.init(content: URL(string: imagePath)!)
                        //                    youtubeVideo?.play(.low)
                    } else {
                        
                        otherVideo = YKDirectVideo.init(content: URL(string: imagePath)!)
                        otherVideo?.play(YKQualityOptions.low)
                        
                        //                    let vc = SB.eventDetail.videoPlayerViewController()!
                        //                    vc.url = imagePath
                        //                    vc.eventType = eventType
                        //                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
}
