//
//  MusicLineupViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON

class MusicLineupViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgLineUp: UIImageView!
    @IBOutlet weak var btnDescription: UIButton!
    
    var isImageData = false
    var lineupDescription = ""
    
    var isFromMusic = true
    
    var detail: JSON?
    
    var isScrollDown = false
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromMusic {
            
            if detail!["lineup"]["desc"].stringValue.isEmpty && detail!["lineup"]["img"].stringValue.isEmpty {
                lblDescription.text = keys.notMention()
                lblDescription.textAlignment = .center
                imgLineUp.isHidden = true
                btnDescription.isHidden = true
                return
            }
            
            lineupDescription = detail!["lineup"]["desc"].stringValue
            
            let imageURL = detail!["lineup"]["img"].stringValue
            isImageData = !imageURL.isEmpty
            imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: nil)
   
            imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: { (image, error, SDImageCacheTypeNone, url) in
                guard let image = image else { return }
                print("Image arrived!")
                self.imgLineUp.image = self.resizeImage(image: image, newWidth: 300)
            })
            
            
        } else {
            
            if detail!["details"]["detail_tab"].stringValue.isEmpty {
                lblDescription.text = keys.notMention()
                lblDescription.textAlignment = .center
                imgLineUp.isHidden = true
                btnDescription.isHidden = true
                return
            }
            
            lineupDescription = detail!["details"]["detail_tab"].stringValue
        }

        lblDescription.text = lineupDescription
        
        imgLineUp.isHidden = !isImageData
        btnDescription.isHidden = !isImageData
        
        
    }

    override func viewWillLayoutSubviews() {
        if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
        } else {
            heightOfView.constant = scrollView.contentSize.height
        }
        offsetDeledate.offsetValueChanged(heightOfView.constant)
    }
    
    @IBAction func btnImagePreview(_ sender: UIButton) {
        let vc = ImagePreviewPopup()
        vc.imageURL = isImageData ? detail!["lineup"]["img"].stringValue : ""
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale

        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension MusicLineupViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            
            //            topConstraintConstant -= 5.0
            //            offsetDeledate.isScrollDown!(topConstraintConstant)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
}
