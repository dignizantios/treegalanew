//
//  VideoPlayerViewController.swift
//  treegala
//
//  Created by om on 8/24/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import AVKit

class VideoPlayerViewController: UIViewController {
    
    var url = ""
    
    var eventType: EventType = .nightClub
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(url)
        let player = AVPlayer(url: URL(string: url)!)
        
        let playerLayer = AVPlayerLayer(player: player)
        //playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        // playerLayer.backgroundColor = UIColor.green.cgColor
        playerLayer.frame = view.frame
        
        self.view.layer.addSublayer(playerLayer)
        
        player.play()
        
        setBackButton()
        setCategoryUI()
        // Do any additional setup after loading the view.
    }
    
    func setCategoryUI() {
        switch eventType {
        case .nightClub:
            self.title = "Nightclubs"
        case .clubCrawl:
            self.title = "Club Crawls"
        case .music:
            self.title = "Music Festival"
        case .street:
            self.title = "Street Festival"
        case .obstacle:
            self.title = "Obstacles Event"
        case .local:
            self.title = "Local Event"
        }
    }
    
}
