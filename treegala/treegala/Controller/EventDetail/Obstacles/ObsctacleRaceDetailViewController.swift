//
//  ObsctacleRaceDetailViewController.swift
//  treegala
//
//  Created by om on 8/13/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON

class ObsctacleRaceDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    @IBOutlet weak var viewDifficulty: UIView!
    @IBOutlet var btnDifficulties: [UIButton]!
    
    @IBOutlet weak var viewPrizes: UIView!
    @IBOutlet weak var lblPrize: UILabel!
    
    @IBOutlet weak var viewOtherDetail: UIView!
    @IBOutlet weak var lblOtherDetails: UILabel!
    
    var details: JSON?
    var isScrollDown = false
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDetail()
    }
    
    override func viewWillLayoutSubviews() {
        if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
        } else {
            heightOfView.constant = scrollView.contentSize.height
        }
        offsetDeledate.offsetValueChanged(heightOfView.constant)
    }
    
    func setDetail() {
        
        let item = details!["race_details"]
        
        let level = item["difficulty_level"].intValue
        
        viewDifficulty.isHidden = (level == 0)
        if level > 0 {
            btnDifficulties.forEach { $0.isHidden = true }
            btnDifficulties[level - 1].isHidden = false
        }
        
        viewPrizes.isHidden = item["prizes_text"].stringValue.isEmpty
        lblPrize.text = item["prizes_text"].stringValue
        
        viewOtherDetail.isHidden = item["other_details"].stringValue.isEmpty
        lblOtherDetails.text = item["other_details"].stringValue
    }
}

extension ObsctacleRaceDetailViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            
            //            topConstraintConstant -= 5.0
            //            offsetDeledate.isScrollDown!(topConstraintConstant)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
}
