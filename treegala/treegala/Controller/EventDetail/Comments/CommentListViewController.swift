//
//  CommentListViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RxSwift
import IQKeyboardManager

class CommentListViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblCommentCount: UILabel!
    
    @IBOutlet weak var bottomOfView: NSLayoutConstraint!
    
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var btnComment: UIButton!
    
    var listID: String?
    
    var spinner = UIActivityIndicatorView()
    
    var offset = 0
    
    var isEditMode = false
    var selectedIndex: Int?
    
    var commentList: [JSON]?
    var disposeBag = DisposeBag()
    
    var totalComment = 0 {
        didSet {
           self.lblCommentCount.text = "\(totalComment) Comment(s)"
        }
    }
    
    let refreshControl = UIRefreshControl()
    
    var errorMessage = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtComment.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0)
        txtComment.delegate = self
        
        setBackButton()
        self.title =  "Comments"
        
        tableView.register(nib.commentTableViewCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        
//        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        spinner.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        spinner.color = color.lightGray()
//        spinner.hidesWhenStopped = true
//        tableView.tableHeaderView = spinner
        
        //refreshControl.tintColor = .white
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        
        btnLogin.rx.tap.subscribe(onNext: {
            self.presentLogin()
        }).disposed(by: disposeBag)
        
        txtComment.rx.text
            .map {
                text -> Bool in
                !(text!.trimmed().isEmpty)
            }.bind(to: btnComment.rx.valid)
            .disposed(by: disposeBag)
        
        btnComment.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            if self.isEditMode {
                self.comment(1, index: self.selectedIndex)
            } else {
                self.comment(0, index: nil)
            }
        }).disposed(by: disposeBag)
        
        getCommentList()
        
    }
    
    @objc func refreshList() {
        offset = 0
        commentList = []
        getCommentList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewLogin.isHidden = isUserLogin()
        viewComment.isHidden = !isUserLogin()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("clear tapped")
        
        if isEditMode {
            isEditMode = false
            self.txtComment.clearButtonMode = .never
        }
        return true
    }
}

extension CommentListViewController {
    
    @objc func getCommentList() {
        
        if offset == -1 {
            refreshControl.endRefreshing()
            return
        }
        
        let param = ["listing_id" : listID ?? EventDetailViewController.listID,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset" : "\(offset)",
            "lang" : "0",
            "timezone" : TimeZone.current.identifier  ]
        
        print(param)
        
        let kURL =  GeneralRequest.queryString((webUrls.baseUrl() + webUrls.getCommentList()), params: param) ?? ""
        
        print(kURL)
        
        if Rechability.isConnectedToNetwork() {

            request(kURL).authenticate(user: webUrls.authUserName(), password: webUrls.authPassword()).responseSwiftyJSON { (response) in
                print(response.result.value)
                self.refreshControl.endRefreshing()
                if response.result.isSuccess, let json = response.result.value {
                    self.errorMessage = json["msg"].stringValue
                    if json["flag"].stringValue == "1" {
                        let firstTime = self.commentList == nil
                        self.offset = json["next_offset"].intValue
                        var tempArray = self.commentList ?? []
                        tempArray.insert(contentsOf: json["data"].arrayValue, at: 0)
                        //tempArray.append(contentsOf: json["data"].arrayValue)
                        self.commentList = tempArray
                        self.totalComment = json["total_comment"].intValue
                        
                        self.tableView.reloadData()
                        if firstTime {
                            self.tableView.scrollToRow(at: IndexPath(row: self.commentList!.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                        }
                    } else {
                        self.commentList = []
                        self.tableView.reloadData()
                    }
                    
                } else {
                    self.commentList = []
                    self.tableView.reloadData()
                    self.showAlert(R.string.keys.somethingWentWrong())
                }
            }
        } else {
            showAlert(R.string.keys.noInternet())
        }
    }
    
    func comment(_ flag: Int, index: Int?) {
        
        let kURL = webUrls.baseUrl() + webUrls.manageComment()
        
        var commentId = "0"
        
        if let index = index {
            commentId = commentList![index]["comment_id"].stringValue
        }
        
        var param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : listID ?? EventDetailViewController.listID,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "comment" : txtComment.text!.trimmed(),
                     "comment_id" : commentId]
        
        if flag == 1 { //Edit
            param["is_del"] = "0"
        } else if flag == 2 {
            param["is_del"] = "1"
        }
 
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.txtComment.text = ""
                if flag == 0 {
                    self.commentList?.append(json["data"])
                    if self.offset != -1 {
                        self.offset += 1
                    }
                    self.totalComment += 1
                } else if flag == 1 {
                    self.isEditMode = false
                    self.commentList![index!] = json["data"]
                } else if flag == 2 {
                    self.commentList?.remove(at: index!)
                    if self.offset != -1 {
                        self.offset -= 1
                    }
                    self.totalComment -= 1
                }
                self.tableView.reloadData()
                if flag == 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: self.commentList!.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                }
            }
        }
        
    }
    
    func likeDislikeComment(index: Int, flag: String) {
            
            let kURL = webUrls.baseUrl() + webUrls.liekDislikeComments()
            
            let commentId = commentList![index]["comment_id"].stringValue
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "listing_id" : listID ?? EventDetailViewController.listID,
                         "lang" : "0",
                         "timezone" : TimeZone.current.identifier,
                         "comment_id" : commentId,
                         "is_like" : flag]
            
            GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
                print(json)
                if json["flag"].stringValue == "1" {
                    self.commentList![index] = json["data"]
                    self.tableView.reloadData()
                }
            }
        
    }
}

//MARK: - Pagiantion
extension CommentListViewController {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
//                if offset == -1 {
//                    tableView.tableFooterView = nil
//                    return
//                } else {
//                    getCommentList()
//                }
            }
        }
    }
}

//MARK: - TableView methods
extension CommentListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if commentList == nil {
            return 10
        } else {
            if commentList!.isEmpty {
                setTableView(errorMessage, color: color.themePurple(), tableView: tableView)
                return 0
            } else {
                tableView.backgroundView = nil
                return commentList!.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! 
        CommentListTableViewCell
        
        cell.selectionStyle = .none
        
        if commentList != nil {
            
            cell.hideAnimation()
            
            let item = commentList![indexPath.row]
            cell.setDetail(item)
            
            cell.btnLike.rx.tap.subscribe(onNext: {
                if isUserLogin() {
                    self.likeDislikeComment(index: indexPath.row, flag: cell.btnLike.isSelected ? "0" : "1")
                    cell.btnLike.isSelected = !cell.btnLike.isSelected
                } else {
                    self.presentLogin()
                }
            }).disposed(by: cell.disposeBag)
            
            cell.btnDisLike.rx.tap.subscribe(onNext: {
                if isUserLogin() {
                    self.likeDislikeComment(index: indexPath.row, flag: cell.btnDisLike.isSelected ? "0" : "2")
                    cell.btnDisLike.isSelected = !cell.btnDisLike.isSelected
                } else {
                    self.presentLogin()
                }
            }).disposed(by: cell.disposeBag)
            
            cell.btnEdit.rx.tap.subscribe(onNext: {
                self.txtComment.text = item["comment"].stringValue
                self.txtComment.clearButtonMode = .whileEditing
                self.selectedIndex = indexPath.row
                self.isEditMode = true
                self.txtComment.becomeFirstResponder()
            }).disposed(by: cell.disposeBag)
            
            cell.btnDelete.rx.tap.subscribe(onNext: {
                
                self.popupAlert(title: keys.appTitle(), message: "Are you sure you want to delete comment?", actionTitles: ["OK","Cancel"], actions: [ { action1 in
                      self.comment(2, index: indexPath.row)
                    }, { cancelAction in
                        
                    }])
                
                
            }).disposed(by: cell.disposeBag)
            
            cell.btnReply.rx.tap.subscribe(onNext: {
                
                let vc = SB.eventDetail.replyViewController()!
                vc.likeObserver.subscribe(onNext: { (status) in
                    self.likeDislikeComment(index: indexPath.row, flag: "\(status)")
                }).disposed(by: self.disposeBag)
                vc.replyCountObserver.subscribe(onNext: {
                    print($0)
                    self.commentList![indexPath.row]["total_replies"].intValue = $0
                    self.tableView.reloadData()
                }).disposed(by: self.disposeBag)
                vc.commentDetail = item
//                vc.listID = self.
                self.navigationController?.pushViewController(vc, animated: true)
                
//                if isUserLogin() {
//
//                } else {
//                    self.presentLogin()
//                }
            }).disposed(by: cell.disposeBag)
            
        }
        
        return cell
    }
    
}


