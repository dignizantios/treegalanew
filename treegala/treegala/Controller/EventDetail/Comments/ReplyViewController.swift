//
//  ReplyViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManager
import RxSwift

class ReplyViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblReplyCount: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRated: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblComment : UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDisLike: UIButton!
    
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var btnComment: UIButton!
    
    var commentDetail: JSON!
    var listID: String?
    
    var spinner = UIActivityIndicatorView()
    var offset = 0
    var isEditMode = false
    var selectedIndex: Int?
    
    var totalReply = 0 {
        didSet {
            self.lblReplyCount.text = "\(totalReply) REPLY"
        }
    }
    
    let refreshControl = UIRefreshControl()
    var replyList: [JSON]?
    
    var likeObserver = PublishSubject<Int>()
    var replyCountObserver = PublishSubject<Int>()
    var disposeBag = DisposeBag()
    
    var errorMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackButton()
        self.title =  "Replies"
        
        tableView.register(nib.commentTableViewCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none

        txtComment.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0)
        txtComment.delegate = self
        
//        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        spinner.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        spinner.color = color.lightGray()
//        spinner.hidesWhenStopped = true
//        tableView.tableFooterView = spinner
        
       
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        
        setDetail()
        
        btnLogin.rx.tap.subscribe(onNext: {
            self.presentLogin()
        }).disposed(by: disposeBag)
        
        txtComment.rx.text
            .map {
                text -> Bool in
                !(text!.trimmed().isEmpty)
            }.bind(to: btnComment.rx.valid)
            .disposed(by: disposeBag)
        
        btnComment.rx.tap.subscribe(onNext: {
            self.view.endEditing(true)
            if self.isEditMode {
                self.manageReply(1, index: self.selectedIndex)
            } else {
                self.manageReply(0, index: nil)
            }
        }).disposed(by: disposeBag)
        
        btnLike.rx.tap.subscribe(onNext: {
            self.likeObserver.onNext(self.btnLike.isSelected ? 0 : 1)
            
            if self.btnDisLike.isSelected {
                self.btnDisLike.isSelected = false
            }
            
            self.btnLike.isSelected = !self.btnLike.isSelected
            
        }).disposed(by: disposeBag)
        
        btnDisLike.rx.tap.subscribe(onNext: {
            self.likeObserver.onNext(self.btnDisLike.isSelected ? 0 : 2)
            if self.btnLike.isSelected {
                self.btnLike.isSelected = false
            }
            self.btnDisLike.isSelected = !self.btnDisLike.isSelected
        }).disposed(by: disposeBag)
        
        getReplyList()
    }
    
    @objc func refreshList() {
        offset = 0
        replyList = []
        getReplyList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewLogin.isHidden = isUserLogin()
        viewComment.isHidden = !isUserLogin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         if self.isMovingFromParentViewController {
           replyCountObserver.onNext(self.replyList?.count ?? 0)
        }
    }
    
    func setDetail() {
        lblUserName.text = commentDetail["user_name"].stringValue
        lblComment.text = commentDetail["comment"].stringValue
        
        btnLike.setTitle(commentDetail["like_count"].stringValue, for: .normal)
        btnDisLike.setTitle(commentDetail["dislike_count"].stringValue, for: .normal)
        
        btnLike.isSelected = commentDetail["like_flag"].intValue == 1
        btnDisLike.isSelected = commentDetail["like_flag"].intValue == 2
        
        lblRated.isHidden = commentDetail["is_rate"].intValue == 0
        imgCategory.isHidden = commentDetail["is_rate"].intValue == 0
        
        let rateType = commentDetail["ratting_type"].stringValue
        
        if rateType == "1" {
            imgCategory.image = #imageLiteral(resourceName: "ic_fun_small")
        } else  if rateType == "2" {
            imgCategory.image = #imageLiteral(resourceName: "ic_satisfactory_big")
        } else  if rateType == "3" {
            imgCategory.image = #imageLiteral(resourceName: "ic_boaring_small")
        } else  if rateType == "4" {
            imgCategory.image = #imageLiteral(resourceName: "ic_dislike_small")
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("Clear Tapped")
        if isEditMode {
            isEditMode = false
            self.txtComment.clearButtonMode = .never
        }
        return true
    }
}

extension ReplyViewController {
    
    @objc func getReplyList() {
        
        if offset == -1 {
            refreshControl.endRefreshing()
            return
        }
        
        let param = [//"listing_id" : listID ?? EventDetailViewController.listID,
                        "listing_id" : commentDetail["listing_id"].stringValue,
                     "comment_id" : commentDetail["comment_id"].stringValue,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset" : "\(offset)",
            "lang" : "0",
            "timezone" : TimeZone.current.identifier  ]
        let kURL =  GeneralRequest.queryString((webUrls.baseUrl() + webUrls.getReplyList()), params: param) ?? ""
        
        print(kURL)
        
        if Rechability.isConnectedToNetwork() {
            
            request(kURL).authenticate(user: webUrls.authUserName(), password: webUrls.authPassword()).responseSwiftyJSON { (response) in
                self.refreshControl.endRefreshing()
                print(response.result.value)
                if response.result.isSuccess, let json = response.result.value {
                    self.errorMessage = json["msg"].stringValue
                    if json["flag"].stringValue == "1" {
                        
                        let firstTime = self.replyList == nil
                        self.offset = json["next_offset"].intValue
                        var tempArray = self.replyList ?? []

                        tempArray.insert(contentsOf: json["data"].arrayValue, at: 0)
                        //tempArray.append(contentsOf: json["data"].arrayValue)
                        self.replyList = tempArray
                        self.totalReply = json["total_comment"].intValue
                        
                        self.tableView.reloadData()
                        if firstTime {
                            self.tableView.scrollToRow(at: IndexPath(row: self.replyList!.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                        }
                        
//                        self.offset = json["next_offset"].intValue
//                        self.replyList = json["data"].arrayValue
//
//                        self.tableView.reloadData()
                    } else {
                        self.replyList = []
                        self.tableView.reloadData()
                    }
                } else {
                    self.replyList = []
                    self.tableView.reloadData()
                    self.showAlert(R.string.keys.somethingWentWrong())
                }
            }
        } else {
            showAlert(R.string.keys.noInternet())
        }
    }
    
    func manageReply(_ flag: Int, index: Int?) {
        
        let kURL = webUrls.baseUrl() + webUrls.manageReply()
        
        let commentId = commentDetail["comment_id"].stringValue
        var replyID = "0"
        
        if let index = index {
            replyID = replyList![index]["reply_id"].stringValue
        }
        
        var param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : listID ?? EventDetailViewController.listID,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "comment" : txtComment.text!.trimmed(),
                     "comment_id" : commentId,
                     "reply_id" : replyID]
        
        if flag == 1 { //Edit
            param["is_del"] = "0"
        } else if flag == 2 {
            param["is_del"] = "1"
        }
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                
                if flag == 0 {
                    self.replyList?.append(json["data"])
                    if self.offset != -1 {
                        self.offset += 1
                    }
                    self.totalReply += 1
                    self.txtComment.text = ""
                } else if flag == 1 {
                    self.isEditMode = false
                    self.replyList![index!] = json["data"]
                } else if flag == 2 {
                    self.replyList?.remove(at: index!)
                    if self.offset != -1 {
                        self.offset -= 1
                    }
                    self.totalReply -= 1
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func likeDislikeReply(index: Int, flag: String) {
        
        let kURL = webUrls.baseUrl() + webUrls.likeDislikereply()
        
        let commentId = replyList![index]["comment_id"].stringValue
        let replyID = replyList![index]["reply_id"].stringValue
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : listID ?? EventDetailViewController.listID,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "comment_id" : commentId,
                     "reply_id" : replyID,
                     "is_like" : flag]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.replyList![index] = json["data"]
                self.tableView.reloadData()
            }
        }
    }
}

//MARK: - Pagiantion
extension ReplyViewController {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
//                if offset == -1 {
//                    tableView.tableFooterView = nil
//                    return
//                } else {
//                    getReplyList()
//                }
            }
        }
    }
}

extension ReplyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func setBGTableView(_ message: String, color: UIColor = .white, tableView: UITableView) {
        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: tableView.frame.width - 20 , height: 50))
        noDataLabel.textAlignment = .center
        noDataLabel.text = message
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = color
        noDataLabel.font = font.ubuntuCondensedRegular(size: 20.0)
        noDataLabel.tag = 1111
        tableView.backgroundView = noDataLabel
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return replyList == nil ? 10 : (replyList!.count)
        
        if replyList == nil {
            return 10
        } else {
            if replyList!.isEmpty {
                setBGTableView(errorMessage, color: color.themePurple(), tableView: tableView)
                return 0
            } else {
                tableView.backgroundView = nil
                return replyList!.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as!
        CommentListTableViewCell
        
        cell.selectionStyle = .none

        cell.btnReply.isHidden = true
        
        if replyList != nil {
            cell.hideAnimation()
            
            let index = indexPath.row
            
            let item = replyList![index]
            cell.setDetail(item)
            
            cell.isReplyComment = true
            
            cell.btnLike.rx.tap.subscribe(onNext: {
                self.likeDislikeReply(index: index, flag: cell.btnLike.isSelected ? "0" : "1")
                cell.btnLike.isSelected = !cell.btnLike.isSelected
            }).disposed(by: cell.disposeBag)
            
            cell.btnDisLike.rx.tap.subscribe(onNext: {
                self.likeDislikeReply(index: index, flag: cell.btnDisLike.isSelected ? "0" : "2")
                cell.btnDisLike.isSelected = !cell.btnDisLike.isSelected
            }).disposed(by: cell.disposeBag)
            
            cell.btnEdit.rx.tap.subscribe(onNext: {
                self.txtComment.text = item["comment"].stringValue
                self.txtComment.clearButtonMode = .whileEditing
                self.selectedIndex = indexPath.row
                self.isEditMode = true
                self.txtComment.becomeFirstResponder()
            }).disposed(by: cell.disposeBag)
            
            cell.btnDelete.rx.tap.subscribe(onNext: {
                
                self.popupAlert(title: keys.appTitle(), message: "Are you sure you want to delete comment?", actionTitles: ["OK","Cancel"], actions: [ { action1 in
                        self.manageReply(2, index: indexPath.row)
                    }, { cancelAction in
                        
                    }])
            }).disposed(by: cell.disposeBag)
        }
        return cell
    }
    
}
