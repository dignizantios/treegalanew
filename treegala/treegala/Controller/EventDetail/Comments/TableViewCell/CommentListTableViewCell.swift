//
//  CommentListTableViewCell.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import SkeletonView
import SwiftyJSON

class CommentListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leadingOfView: NSLayoutConstraint!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    @IBOutlet weak var lblRated: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDisLike: UIButton!
    
    @IBOutlet weak var btnReply: UIButton!
    
    var disposeBag = DisposeBag()

    var isReplyComment = false {
        didSet {
            leadingOfView.constant = isReplyComment ? 24.0 : 0.0
        }
    }
    
    var isMyComment = false {
        didSet {
            [btnEdit, btnDelete].forEach { $0?.isHidden = !isMyComment }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isMyComment = false
        
        [lblUserName,lblComment].forEach { $0?.showAnimatedSkeleton() }
        [btnLike, btnDisLike, btnReply].forEach { $0?.showAnimatedSkeleton()}
    }
    
    func hideAnimation() {
        [lblUserName,lblComment].forEach { $0?.hideSkeleton() }
        [btnLike, btnDisLike, btnReply].forEach { $0?.hideSkeleton()}
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDetail(_ json: JSON) {
        lblUserName.text = json["user_name"].stringValue
        lblComment.text = json["comment"].stringValue
        
        btnLike.setTitle(json["like_count"].stringValue, for: .normal)
        btnDisLike.setTitle(json["dislike_count"].stringValue, for: .normal)
        
        btnLike.isSelected = json["like_flag"].intValue == 1
        btnDisLike.isSelected = json["like_flag"].intValue == 2
        
        let replyText = json["total_replies"].intValue == 0 ? " Reply" : ((json["total_replies"].intValue == 1) ? "\(json["total_replies"].intValue) Reply" : "\(json["total_replies"].intValue) Replies")
        btnReply.setTitle(replyText, for: .normal)
        
        imgCategory.isHidden = json["is_rate"].intValue == 0
        lblRated.isHidden = json["is_rate"].intValue == 0
        
        let rateType = json["ratting_type"].stringValue
        
        if rateType == "1" {
            imgCategory.image = #imageLiteral(resourceName: "ic_fun_small")
        } else  if rateType == "2" {
            imgCategory.image = #imageLiteral(resourceName: "ic_satisfactory_big")
        } else  if rateType == "3" {
            imgCategory.image = #imageLiteral(resourceName: "ic_boaring_small")
        } else  if rateType == "4" {
            imgCategory.image = #imageLiteral(resourceName: "ic_dislike_small")
        }
        
        isMyComment = json["user_id"].stringValue == getUserDetail("user_id")
    }

}
