//
//  NewEventDetailVC.swift
//  treegala
//
//  Created by Jaydeep on 20/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import RxSwift
import AVKit
import DropDown

class NewEventDetailVC: UIViewController {
    
    //MARK:- Variblde Declaration
    
    var selectedIndex = Int()
    let vvHeader = Bundle.main.loadNibNamed("EventDetailHeaderView", owner: self, options: nil)?[0] as? EventDetailHeaderView
    var dictData = JSON()
    var dictEventDetailData = JSON()
    var eventImages: [Any] = []
    var sliderCount = 0
    var otherVideo: YKDirectVideo?
    var youtubeVideo: YKYouTubeVideo?
    var disposeBag = DisposeBag()
    var eventType: EventType = .nightClub
    var openTimeDD = DropDown()
    var chargeDD = DropDown()
    var openTimes = Variable<[String]>([])
    var navigationPBController = PBSwipeController()
    var swipeLeft = UISwipeGestureRecognizer()
    var swipeRight = UISwipeGestureRecognizer()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblEventDetail: UITableView!
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        selectedIndex = 0
        tblEventDetail.register(UINib(nibName: "EventDesciptionCell", bundle: nil), forCellReuseIdentifier: "EventDesciptionCell")
        tblEventDetail.register(UINib(nibName: "EventRatingCell", bundle: nil), forCellReuseIdentifier: "EventRatingCell")
        tblEventDetail.register(UINib(nibName: "EventHeadlineCell", bundle: nil), forCellReuseIdentifier: "EventHeadlineCell")
        tblEventDetail.register(UINib(nibName: "NightEventCell", bundle: nil), forCellReuseIdentifier: "NightEventCell")
        tblEventDetail.register(UINib(nibName: "EventObstacleRaceCell", bundle: nil), forCellReuseIdentifier: "EventObstacleRaceCell")
        tblEventDetail.register(UINib(nibName: "EventMusicLineUpCell", bundle: nil), forCellReuseIdentifier: "EventMusicLineUpCell")
        tblEventDetail.register(UINib(nibName: "EventClubCrawelCell", bundle: nil), forCellReuseIdentifier: "EventClubCrawelCell")
        tblEventDetail.register(UINib(nibName: "EventObstacleCell", bundle: nil), forCellReuseIdentifier: "EventObstacleCell")
        tblEventDetail.register(UINib(nibName: "EventStreetHeaderCell", bundle: nil), forCellReuseIdentifier: "EventStreetHeaderCell")
        tblEventDetail.register(UINib(nibName: "EventMusicHeaderCell", bundle: nil), forCellReuseIdentifier: "EventMusicHeaderCell")
        tblEventDetail.register(UINib(nibName: "EventNightCell", bundle: nil), forCellReuseIdentifier: "EventNightCell")
        [vvHeader?.lblBottomEvent,vvHeader?.lblBottomHeadlines,vvHeader?.lblBottomRatting,vvHeader?.lblBottomDescription].forEach { (lbl) in
            lbl?.isHidden = true
        }
        
        vvHeader?.lblBottomHeadlines.isHidden = false
        swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.labelSwipedLeft(sender:)))
        swipeLeft.direction = .left
        
        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.labelSwipedLeft(sender:)))
        swipeRight.direction = .right
       // addInitialObjects()
        
        getEventDetailData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    
    
    //MARK:- Overide Method
    
    
    
    func setupUI(cell:EventFirstCell) {
        
        eventImages = []
        sliderCount = 0
        
        setBackButton()
        setGradientBar()
        
        cell.imgSlider.slideshowTimeInterval = UInt(0.0)
        cell.imgSlider.indicatorDisabled = false
        cell.imgSlider.imageCounterDisabled = true
        cell.imgSlider.hidePageControlForSinglePages = true
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        button.addTarget(self, action: #selector(self.logoClick), for: .touchUpInside)
        navigationItem.titleView = button
        
        setCategoryUI()
    }
    
    @objc func logoClick() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setCategoryUI() {
        switch eventType {
        case .nightClub:
            // self.title = "Nightclub"
            vvHeader?.btnEventOutlet.setTitle("EVENTS", for: .normal)
            break
        case .clubCrawl:
            //self.title = "Club Crawls"
            vvHeader?.btnEventOutlet.setTitle("DETAILS", for: .normal)
            break
           // headerTitles[1] = "DETAILS"
        case .music:
            vvHeader?.btnEventOutlet.setTitle("LINEUP", for: .normal)
            break
            // self.title = "Music Festival"
           // headerTitles[1] = "LINEUP"
        case .street:
            //self.title = "Street Festival"
            vvHeader?.btnEventOutlet.setTitle("Street Festival", for: .normal)
            break
        case .local:
             vvHeader?.btnEventOutlet.setTitle("RACE DETAILS", for: .normal)
            break
        case .obstacle:
            vvHeader?.btnEventOutlet.setTitle("DESCRIPTION", for: .normal)
            break
            //self.title = "Obstacles Event"
         //   headerTitles[1] = "RACE DETAILS"
        }

      /*  headerButtons[0].setTitle(headerTitles[0], for: .normal)
        headerButtons[1].setTitle(headerTitles[1], for: .normal)
        headerButtons[2].setTitle(headerTitles[2], for: .normal)
        headerButtons[3].setTitle(headerTitles[3], for: .normal)*/
    }

}

//MARK:- PBSwipe Controller

extension NewEventDetailVC:PBSwipeControllerDelegate
{
    func addInitialObjects()
    {
         let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
        
        let child1 = eventStoryboard.instantiateViewController(withIdentifier: "NightHeadlineViewController") as! NightHeadlineViewController
        child1.details = dictEventDetailData
        
       /* let child3 = eventStoryboard.instantiateViewController(withIdentifier: "DescriptionSubViewController") as! DescriptionSubViewController
        child3.descriptionDetail = dictEventDetailData*/
        
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
      //  pageController.setViewControllers([child1], direction: .forward, animated: true, completion: nil)
        
        navigationPBController = PBSwipeController(rootViewController: pageController)
        navigationPBController.swipeDelegate = self
        self.addChildViewController(navigationPBController)
        navigationPBController.didMove(toParentViewController: self)
    }
    
    func swipe(at index: Int32) {
        print("Index :- \(index)")
      
    }
    
    
}

//MARK:- Action Zone

extension NewEventDetailVC
{
    @objc func btnHeadlinesAction(_ sender:UIButton)  {
        print("tag \(sender.tag)")
        [vvHeader?.lblBottomEvent,vvHeader?.lblBottomHeadlines,vvHeader?.lblBottomRatting,vvHeader?.lblBottomDescription].forEach { (lbl) in
            lbl?.isHidden = true
        }
        if sender.tag == 0
        {
            vvHeader?.lblBottomHeadlines.isHidden = false
        }
        else if sender.tag == 1
        {
            vvHeader?.lblBottomEvent.isHidden = false
        }
        else if sender.tag == 2
        {
            vvHeader?.lblBottomDescription.isHidden = false
        }
        else if sender.tag == 3
        {
            vvHeader?.lblBottomRatting.isHidden = false
        }
        selectedIndex = sender.tag
        self.tblEventDetail.reloadData {
            if sender.tag == 1 {
                self.tblEventDetail.layoutIfNeeded()
                self.tblEventDetail.reloadData()
            }
        }
        
    }
}

//MARK:- TableView Life Cycle

extension NewEventDetailVC:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 1000
        {
            // Event Calender
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView.tag == 1000
        {
            // Event Calender
            return 0
        }
        if section == 0
        {
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView.tag == 1000
        {
            // Event Calender
            let vw = UIView()
            return vw
        }
        if section == 0
        {
            let vw = UIView()
            return vw
        }        
      
       /* let viewHeader = UIView()
        viewHeader.addSubview(navigationPBController.addInitialObjects())
        return viewHeader*/
        vvHeader?.btnHeadlinesOutlet.addTarget(self, action: #selector(btnHeadlinesAction(_:)), for: .touchUpInside)
        vvHeader?.btnEventOutlet.addTarget(self, action: #selector(btnHeadlinesAction(_:)), for: .touchUpInside)
        vvHeader?.btnDesciptionOutlet.addTarget(self, action: #selector(btnHeadlinesAction(_:)), for: .touchUpInside)
        vvHeader?.btnRatingOutlet.addTarget(self, action: #selector(btnHeadlinesAction(_:)), for: .touchUpInside)
        return vvHeader
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 1000
        {
            // Event Calender
            //return dictEventDetailData["events"].arrayValue.count
            
            if dictEventDetailData["events"].arrayValue.count ==  0
            {
                if dictEventDetailData["events_page_link"].stringValue.isEmpty {
                    setTableViewBG("There are no events/festivals.", tableView: tableView)
                   // viewCalendar.isHidden = true
                   // btnEventCalendar.isHidden = true
                }
                return 0
            } else {
                tableView.backgroundView = nil
                return dictEventDetailData["events"].arrayValue.count
            }
        }
        if section == 0
        {
            return 1
        }
        /*if selectedIndex == 1
        {
            switch eventType {
            case .nightClub:
                return dictEventDetailData["events"].arrayValue.count
            case .clubCrawl:
                return 1
            case .music:
                return 1
            case .street:
                return dictEventDetailData["events"].arrayValue.count
            case .obstacle:
               return 1
            }
        }*/
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 1000
        {
            // Event Calender
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NightEventTableViewCell
            cell.selectionStyle = .none
            let item = dictEventDetailData["events"][indexPath.row]
            cell.lblDayName.text = item["day_name"].stringValue.capitalized
            cell.lblAmenties.text = item["day_description"].stringValue
            return cell
        }
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventFirstCell") as! EventFirstCell
            setupUI(cell: cell)
            setData(dictEventDetailData, cell: cell)
            btnFavAction(cell: cell)
            return cell
        }
       
        if selectedIndex == 0
        {
            switch eventType {
            case .nightClub:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventHeadlineCell") as! EventHeadlineCell
                setData(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .clubCrawl:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventClubCrawelCell") as! EventClubCrawelCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .music:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicHeaderCell") as! EventMusicHeaderCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .street:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventStreetHeaderCell") as! EventStreetHeaderCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .obstacle:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleCell") as! EventObstacleCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .local:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleCell") as! EventObstacleCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            }
        }
        else if selectedIndex == 1
        {
            switch eventType {
            case .nightClub:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventNightCell") as! EventNightCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .clubCrawl:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicLineUpCell") as! EventMusicLineUpCell
                setDetail(cell: cell, isFromMusic: false, isImageData: false)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .music:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicLineUpCell") as! EventMusicLineUpCell
                setDetail(cell: cell, isFromMusic: true, isImageData: false)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .street:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventNightCell") as! EventNightCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .obstacle:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleRaceCell") as! EventObstacleRaceCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
            case .local:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleRaceCell") as! EventObstacleRaceCell
                setDetail(cell: cell)
                cell.addGestureRecognizer(swipeRight)
                cell.addGestureRecognizer(swipeLeft)
                return cell
                
            }
        }
        else if selectedIndex == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventDesciptionCell") as! EventDesciptionCell
            setDescription(cell: cell)
            cell.addGestureRecognizer(swipeRight)
            cell.addGestureRecognizer(swipeLeft)
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventRatingCell") as! EventRatingCell
        setRattingData(cell: cell, json: dictEventDetailData)
        cell.addGestureRecognizer(swipeRight)
        cell.addGestureRecognizer(swipeLeft)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func setTableViewBG(_ message: String, tableView: UITableView) {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 0, y: 10, width: tableView.frame.width, height: 50)
        noDataLabel.textAlignment = .center
        noDataLabel.text = message
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = .white
        noDataLabel.font = font.ubuntuCondensedRegular(size: 20.0)
        noDataLabel.tag = 1111
        tableView.backgroundView = noDataLabel
        
    }
    
}

//MARK:- Swipe
extension NewEventDetailVC
{
    @objc func labelSwipedLeft(sender: UIGestureRecognizer) {
        if let swipeGesture = sender as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                if selectedIndex != 0
                {
                    selectedIndex -= 1
                    if selectedIndex == 0
                    {
                        self.btnHeadlinesAction(vvHeader!.btnHeadlinesOutlet)
                    }
                    else if selectedIndex == 1
                    {
                        self.btnHeadlinesAction(vvHeader!.btnEventOutlet)
                    }
                    else if selectedIndex == 2
                    {
                        self.btnHeadlinesAction(vvHeader!.btnDesciptionOutlet)
                    }
                }
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                if selectedIndex != 3
                {
                    selectedIndex += 1
                    if selectedIndex == 1
                    {
                        self.btnHeadlinesAction(vvHeader!.btnEventOutlet)
                    }
                    else if selectedIndex == 2
                    {
                        self.btnHeadlinesAction(vvHeader!.btnDesciptionOutlet)
                    }
                    else if selectedIndex == 3
                    {
                        self.btnHeadlinesAction(vvHeader!.btnRatingOutlet)
                    }
                   
                }
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
}

//MARK:- Set Ratting Data
extension NewEventDetailVC
{
    func setRattingData(cell:EventRatingCell,json:JSON)  {
        
        cell.btnViewComments.rx.tap.subscribe(onNext: {
            let vc = SB.eventDetail.commentListViewController()!
            self.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposeBag)
        
        cell.btnResetRating.rx.tap.subscribe({_ in
            self.manageRating(flag: 0, cell: cell)
        }).disposed(by: disposeBag)
        
        let rating = json["ratings"].exists() ? json["ratings"] : json
        
        if rating["ratting_type"].intValue == 0 {
            cell.btnResetRating.isHidden = true
            cell.btnRates.forEach { $0.isUserInteractionEnabled = true }
        } else {
            cell.btnResetRating.isHidden = false
            cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
            let flag = rating["ratting_type"].intValue - 1
            cell.btnRates[flag].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
            cell.btnRates.forEach { $0.isUserInteractionEnabled = false }
        }
        
        cell.lblTotalRate.text = "Based on \(rating["total_vote"].stringValue) Votes"
        
        cell.lblFunVote.text = "\(rating["fun_rate"].stringValue) Voted Fun"
        cell.lblSatisFactoryVote.text = "\(rating["satisfactory_rate"].stringValue) Voted Satisfactory"
        cell.lblBoringVote.text = "\(rating["boring_rate"].stringValue) Voted Boring"
        cell.lblDislikedVote.text = "\(rating["dislike_rate"].stringValue) Voted Disliked"
    }
    
    func manageRating(flag: Int,cell:EventRatingCell) {
        let kURL = webUrls.baseUrl() + webUrls.manageRating()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : dictData["listing_id"].stringValue,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "ratting_type" : flag == 0 ? "" : "\(flag)"]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                let details = json["data"]
                self.setRattingData(cell: cell, json: details)
                if flag == 0 {
                    cell.btnResetRating.isHidden = true
                    cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    cell.btnRates.forEach { $0.isUserInteractionEnabled = true }
                } else {
                    cell.btnResetRating.isHidden = false
                    cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    cell.btnRates[flag - 1].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
                    cell.btnRates.forEach { $0.isUserInteractionEnabled = false }
                }
            }
        }
        
    }
}


//MARK:- Set Decription Data
extension NewEventDetailVC
{
    func setDescription(cell:EventDesciptionCell) {
        cell.btnPhone.rx.tap.subscribe(onNext: {
            if let url = URL(string: "tel://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }).disposed(by: disposeBag)
        
        if dictEventDetailData["description"]["desc"].stringValue.isEmpty {
            cell.lblDescription.textAlignment = .center
            cell.lblDescription.text = keys.notMention()
        } else {
            cell.lblDescription.text = dictEventDetailData["description"]["desc"].stringValue
        }
    }
}

//MARK:- Set Obstacle Data
extension NewEventDetailVC
{
    func setDetail(cell:EventObstacleRaceCell)
    {
        let item = dictEventDetailData["race_details"]
        
        let level = item["difficulty_level"].intValue
        
        cell.viewDifficulty.isHidden = (level == 0)
        if level > 0 {
            cell.btnDifficulties.forEach { $0.isHidden = true }
            cell.btnDifficulties[level - 1].isHidden = false
        }
        
        cell.viewPrizes.isHidden = item["prizes_text"].stringValue.isEmpty
        cell.lblPrize.text = item["prizes_text"].stringValue
        
        cell.viewOtherDetail.isHidden = item["other_details"].stringValue.isEmpty
        cell.lblOtherDetails.text = item["other_details"].stringValue
    }
}

//MARK:- Tableview Extension


extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

//MARK:- Set MusicLineUp Cell
extension NewEventDetailVC
{
    func setDetail(cell:EventMusicLineUpCell,isFromMusic:Bool,isImageData:Bool)
    {
        var lineupDescription = ""
        var isData = isImageData
        cell.btnDescription.addTarget(self, action: #selector(btnImagePreview(_:)), for: .touchUpInside)
        if isFromMusic {
            
            if dictEventDetailData["lineup"]["desc"].stringValue.isEmpty && dictEventDetailData["lineup"]["img"].stringValue.isEmpty {
                cell.lblDescription.text = keys.notMention()
                cell.lblDescription.textAlignment = .center
                cell.imgLineUp.isHidden = true
                cell.btnDescription.isHidden = true
                return
            }
            
            lineupDescription = dictEventDetailData["lineup"]["desc"].stringValue
            
            let imageURL = dictEventDetailData["lineup"]["img"].stringValue
            isData = imageURL.isEmpty
            cell.imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: nil)
            
            cell.imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: { (image, error, SDImageCacheTypeNone, url) in
                guard let image = image else { return }
                print("Image arrived!")
                cell.imgLineUp.image = self.resizeImage(image: image, newWidth: 300)
            })
            
            
        } else {
            
            if dictEventDetailData["details"]["detail_tab"].stringValue.isEmpty {
                cell.lblDescription.text = keys.notMention()
                cell.lblDescription.textAlignment = .center
                cell.imgLineUp.isHidden = true
                cell.btnDescription.isHidden = true
                return
            }
            
            lineupDescription = dictEventDetailData["details"]["detail_tab"].stringValue
        }
        
        cell.lblDescription.text = lineupDescription
        
        cell.imgLineUp.isHidden = !isData
        cell.btnDescription.isHidden = !isData
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func btnImagePreview(_ sender: UIButton) {
        let vc = ImagePreviewPopup()
        vc.imageURL = dictEventDetailData["lineup"]["img"].stringValue
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK:- Set EventNight Data

extension NewEventDetailVC
{
    func setDetail(cell:EventNightCell) {
        
        
        let events = dictEventDetailData["events"].arrayValue
        
        if !events.isEmpty && !dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = true
            cell.btnEventCalendar.isHidden = false
        } else if events.isEmpty && !dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = false
            cell.btnEventCalendar.isHidden = true
        } else if !events.isEmpty && dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = true
            cell.btnEventCalendar.isHidden = true
        }
        
        cell.btnEventCalendarUppar.addTarget(self, action: #selector(btnEventCalendar(_:)), for: .touchUpInside)
        cell.btnEventCalendarBottom.addTarget(self, action: #selector(btnEventCalendar(_:)), for: .touchUpInside)
        
        cell.calendarHeight = cell.btnEventCalendar.isHidden ? 0.0 : 88.0
        
        cell.tableView.register(nib.nightEventCell(), forCellReuseIdentifier: "cell")
        //
        cell.tableView.separatorStyle = .none
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.reloadData()
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
}




//MARK:- Set Obstacle Cell Data

extension NewEventDetailVC
{
    func setDetail(cell:EventObstacleCell) {
        let item = dictEventDetailData["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        cell.lblRegister.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.viewCourseDetail.isHidden = item["course_details"].stringValue.isEmpty
        cell.lblCourseDetail.text = item["course_details"].stringValue
        
        cell.viewFreeSwag.isHidden = item["free_swag"].stringValue.isEmpty
        cell.lblFreeSwag.text = item["free_swag"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        
        if let time = item["hedline_dates"].arrayValue.first {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            if let date = dateFmt.date(from: time["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM"
                cell.lblDate.text = dateFmt.string(from: date)
            }
        }
        
        cell.lblRegister.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
}

//MARK:- Set Street Data

extension NewEventDetailVC
{
    func setDetail(cell:EventStreetHeaderCell) {
        let item = dictEventDetailData["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.viewFacts.isHidden = item["other_facts"].stringValue.isEmpty
        cell.lblFacts.text = item["other_facts"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        let times = item["hedline_dates"].arrayValue
        
        if times.count == 1 {
            let item = times[0]
            
            if item["end_date"].stringValue == "0000-00-00" {
                cell.lblDates.text = setDates(from: times)
            } else {
                
                let date = item["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM") + " - " + item["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM")
                 cell.lblDates.text = date
               // openTimes.value = [date]
            }
        } else {
            cell.lblDates.text = setDates(from: times)
        }
        
        cell.lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
}

//MARK:- Set MusicHeader Data

extension NewEventDetailVC
{
    func setDetail(cell: EventMusicHeaderCell)
    {
        let item = dictEventDetailData["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.viewGenres.isHidden = item["genres"].stringValue.isEmpty
        cell.lblGenres.text = item["genres"].stringValue
        
        cell.viewHeadline.isHidden = item["hadeliner_text"].stringValue.isEmpty
        cell.lblHeadline.text = item["hadeliner_text"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        let times = item["hedline_dates"].arrayValue
        
        if times.count == 1 {
            let item = times[0]
            
            if item["end_date"].stringValue == "0000-00-00" {
               cell.lblDates.text = setDates(from: times)
            } else {
                
                let date = item["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM") + " - " + item["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM")
                cell.lblDates.text = date
                //openTimes.value = [date]
            }
        } else {
           cell.lblDates.text = setDates(from: times)
        }
        
        cell.lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func setDates(from times: [JSON]) -> String {
        
        var dates: [String] = []
        
        for i in times {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            var string = ""
            
            if let date = dateFmt.date(from: i["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM"
                string += dateFmt.string(from: date)
            }
            
            dateFmt.dateFormat = "hh:mm:ss"
            if let startTime = dateFmt.date(from: i["start_time"].stringValue), let endTime = dateFmt.date(from: i["end_time"].stringValue) {
                dateFmt.dateFormat = "h:mm a"
                let temp = " - \(dateFmt.string(from: startTime)) to \(dateFmt.string(from: endTime))"
                
                string += temp
            }
            dates.append(string)
        }
        
       // openTimes.value = dates
        
        return dates.joined(separator: "\n")
    }
}


//MARK:- Set Club Crawl Data
extension NewEventDetailVC
{
    func setDetail(cell:EventClubCrawelCell) {
        
        let item = dictEventDetailData["headlines"]
        
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.viewBusStop.isHidden = item["bus_stop_text"].stringValue.isEmpty
        cell.lblBusStop.text = item["bus_stop_text"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        if let time = item["hedline_dates"].arrayValue.first {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            if let date = dateFmt.date(from: time["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM"
                cell.lblDate.text = dateFmt.string(from: date)
            }
        }
        
        cell.lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
}


//MARK:-  Set Event

extension NewEventDetailVC
{
    @IBAction func btnEventCalendar(_ sender: UIButton) {
        if let url = dictEventDetailData["events_page_link"].stringValue.toURL() {
            UIApplication.shared.openURL(url)
        }
    }
}

//MARK:- Headline Cell

extension NewEventDetailVC
{
    func setData(cell:EventHeadlineCell)  {
        configDropdown(dropdown: openTimeDD, sender: cell.txtTime,cell:cell)
        configDropdown(dropdown: chargeDD, sender: cell.lblCharge,cell:cell)
        cell.txtTime.delegate = self
        
        let item = dictEventDetailData["headlines"]
        
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.viewGenres.isHidden = item["genres"].stringValue.isEmpty
        cell.lblGenres.text = item["genres"].stringValue
        
        cell.viewDressCode.isHidden = item["dress_code"].stringValue.isEmpty
     //   cell.txtvwDressCode.text = item["dress_code"].stringValue
        cell.lblDressCode.text = item["dress_code"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
       // cell.txtOtherResearch.text = item["tags"].stringValue
        
        cell.btnTimeDropDown.isHidden = false
        
        displayTimes(cell: cell)
        
        let charge = item["cover_charge"].arrayValue
        
        if charge.isEmpty {
            cell.lblCharge.text = keys.notMention()
            cell.btnDropDown.isHidden = true
        } else if charge.count == 1 {
            let firstObj = charge[0]
            if firstObj["is_flag"].stringValue == "1" {
                cell.lblCharge.text = (firstObj["cover_charge"].stringValue)
                cell.btnDropDown.isHidden = true
            } else {
                displayCharges(charge, cell: cell)
            }
        } else {
            displayCharges(charge, cell: cell)
        }
        
        cell.lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.lblCharge.onClick = {
            self.chargeDD.show()
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap, cell: cell)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
            
        }).disposed(by: disposeBag)
        
       // viewDidLayoutSubviews()
    }
    
    func displayCharges(_ array: [JSON],cell:EventHeadlineCell) {
       // cell.btnDropDown.isHidden = false
        let items = array.map { $0["day_name"].stringValue + " " + $0["cover_charge"].stringValue }
        print(items)
        
        print(Date().getDayOfWeek())
        
        let temp = (array.map { $0["is_day"].intValue }).filter { $0 >= Date().getDayOfWeek() }.sorted(by: { $0 < $1 })
        print(temp)
        
        if let i = temp.first {
            if let index = array.index(where: { $0["is_day"].stringValue == "\(i)" }) {
                let obj = array[index]
                cell.lblCharge.text = String(obj["day_name"].stringValue.prefix(3)) + " " + obj["cover_charge"].stringValue
            }
        } else {
            cell.lblCharge.text = items.first
        }
        
        chargeDD.dataSource = items
    }
    
    func displayTimes(cell:EventHeadlineCell) {
        
        let times = dictEventDetailData["headlines"]["hedline_dates"].arrayValue
        
        var items: [String] = []
        
        for i in times {
            if let startDate = i["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = i["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "hh:mm a"
                let temp = i["day_name"].stringValue + " " + dateFmt.string(from: startDate) + " - "
                    + dateFmt.string(from: endDate)
                items.append(temp)
            }
        }
        
        openTimeDD.dataSource = items
        
        if let index = times.index(where: { $0["is_day"].intValue == Date().getDayOfWeek() }) {
            let object = times[index]
            
            if let startDate = object["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = object["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                var date = Date()
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "HH:mm:ss"
                
                let tempStr = dateFmt.string(from: date)
                
                date = dateFmt.date(from: tempStr)!
                
                print(startDate)
                print(endDate)
                
                
                dateFmt.dateFormat = "h a"
                if date < startDate {
                    //Open at time
                    cell.txtTime.attributedText = createAttributedTime(.green, string1: "OPEN ", string2: "at \(dateFmt.string(from: startDate))")
                    
                } else if date > endDate {
                    // Closed at end time
                    cell.txtTime.attributedText = createAttributedTime(.red, string1: "CLOSED ", string2: "at \(dateFmt.string(from: endDate))")
                } else if date > startDate && date < endDate {
                    //Open until endtime
                    
                    cell.txtTime.attributedText = createAttributedTime(.green, string1: "OPEN ", string2: "until \(dateFmt.string(from: endDate))")
                }
            }
            
        } else if times.isEmpty {
            cell.txtTime.text = keys.notMention()
            cell.btnTimeDropDown.isHidden = true
        } else {
            //txtTime.textColor = .red
            
            let attribute1 = [NSAttributedStringKey.foregroundColor : UIColor.white]
            
            let attrString1 = NSMutableAttributedString(string: "TODAY ", attributes: attribute1)
            
            let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.red]
            
            let attrString2 = NSAttributedString(string: "CLOSED", attributes: attribute2)
            
            attrString1.append(attrString2)
            
            cell.txtTime.attributedText = attrString1
        }
    }
    
    func createAttributedTime(_ color: UIColor, string1: String, string2: String) -> NSAttributedString {
        
        let attribute1 = [NSAttributedStringKey.foregroundColor : color]
        
        let attrString1 = NSMutableAttributedString(string: string1, attributes: attribute1)
        
        let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let attrString2 = NSAttributedString(string: string2, attributes: attribute2)
        
        attrString1.append(attrString2)
        
        return attrString1
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView,cell:EventHeadlineCell) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = cell.viewTime.frame.width
        
        if dropdown == openTimeDD {
            dropdown.width = cell.viewTime.frame.width + 30
        }
        
        dropdown.selectionBackgroundColor = .white
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
    
}

extension NewEventDetailVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let cell = tblEventDetail.cellForRow(at: IndexPath(row: 0, section: 1)) as? EventHeadlineCell
        {
            if textField == cell.txtTime {
                openTimeDD.show()
                return false
            }
        }
        
        
        return true
    }
}

//MARK:- Set Data First cell

extension NewEventDetailVC
{
    func btnFavAction(cell:EventFirstCell)  {
        cell.btnFavorite.rx.tap.subscribe(onNext: {
            
            if isUserLogin() {
                let kURL = webUrls.baseUrl() + webUrls.eventFavorite()
                
                let param = ["user_id" : getUserDetail("user_id"),
                             "access_token" : getUserDetail("access_token"),
                             "listing_id" : self.dictData["listing_id"].stringValue,
                             "lang" : "0",
                             "timezone" : TimeZone.current.identifier ]
                
                GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                    print(json)
                    if json["flag"].stringValue == "1" {
                        cell.btnFavorite.isSelected = json["data"]["is_favourite"].boolValue
                    }
                })
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)
    }
    
    func btnClaimAction(cell:EventFirstCell)  {
        cell.btnClaim.rx.tap.subscribe(onNext: {
            if isUserLogin() {
                let status = self.dictEventDetailData["is_claimed_by"].stringValue
                    let vc = ClaimSubmitPopup()
                    vc.claimStatusObserver.subscribe(onNext: { (status) in
                        if status {
                            cell.btnClaim.setTitle(" Requested", for: .normal)
                            cell.widthOfClaim.constant = 100
                        }
                    }).disposed(by: self.disposeBag)
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)

    }
    
    
    
    @IBAction func btnPreviosAction(_ sender:UIButton)
    {
        if self.sliderCount > 0 {
            self.sliderCount -= 1
            let index = UInt(self.sliderCount)
            if let cell = tblEventDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventFirstCell
            {
                cell.imgSlider.setCurrentPage(index, animated: true)
                if sliderCount == 0
                {
                    cell.btnBack.isHidden = true
                }
                cell.btnForward.isHidden = false
            }
            
        }
    }
    
    @IBAction func btnNextAction(_ sender:UIButton)
    {
        if self.sliderCount < self.eventImages.count - 1 {
            self.sliderCount += 1
            let index = UInt(self.sliderCount)
            print(index)
            if let cell = tblEventDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventFirstCell
            {
                cell.imgSlider.setCurrentPage(index, animated: true)
                if sliderCount == self.eventImages.count - 1
                {
                    cell.btnForward.isHidden = true
                }
                cell.btnBack.isHidden = false
            }
        }
    }
    
    func setData(_ json: JSON,cell:EventFirstCell) {
        
        if eventImages.count == 1
        {
            cell.btnBack.isHidden = true
            cell.btnForward.isHidden = true
        }
        cell.btnBack.isHidden = true
        
        cell.btnBack.addTarget(self, action: #selector(btnPreviosAction), for: .touchUpInside)
        cell.btnForward.addTarget(self, action: #selector(btnNextAction), for: .touchUpInside)
        
        cell.lblEventName.text = json["title"].stringValue
        cell.lblEventLocation.text = json["location"].stringValue
        
        cell.imgSlider.delegate = self
        cell.imgSlider.dataSource = self
        
        cell.widthOfClaim.constant = 75
        
        let status = json["is_claimed_by"].stringValue
        
        if status == "0" {
            cell.btnClaim.isSelected = false
        } else if status == "-1" {
            cell.btnClaim.isSelected = false
            cell.btnClaim.setTitle(" Requested", for: .normal)
            cell.widthOfClaim.constant = 100
        } else {
            cell.btnClaim.isSelected = true
        }
        
        var imageArray: [Any] = []
        
        for i in json["images"].arrayValue {
            let imagePath = i["img_path"].stringValue
            if i["is_video"].stringValue == "2" {
                if imagePath.contains("youtube.com") {
                    let videoId = imagePath.components(separatedBy: "v=")[1]
                    let path = "https://img.youtube.com/vi/\(videoId)/0.jpg"
                    
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    SDWebImageManager.shared().imageDownloader?.downloadImage(with: path.toURL(), options: .highPriority, progress: nil, completed: { (image, data, error, status) in
                        imageArray[1] = self.createVideoView(object: image ?? #imageLiteral(resourceName: "ic_image_placeholder"))
                        self.eventImages = imageArray
                        cell.imgSlider.reloadData()
                    })
                } else {
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    DispatchQueue.global().async {
                        let image = self.getThumbnailImage(forUrl: imagePath.toURL()!) ?? UIImage()
                        DispatchQueue.main.async {
                            imageArray[1] = self.createVideoView(object: image)
                            self.eventImages = imageArray
                            cell.imgSlider.reloadData()
                        }
                    }
                }
            } else {
                imageArray.append(imagePath)
            }
        }
        
        print(imageArray)
        
        eventImages = imageArray
        cell.imgSlider.reloadData()
        
        cell.btnFavorite.isSelected = json["is_favourite"].boolValue
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    func createVideoView(object: UIImage) -> UIImage {
        
        let bottomImage = object
        let topImage = #imageLiteral(resourceName: "ic_play_video")
        
        let size = CGRect(x: 0, y: 0, width: 300, height: 150)
        UIGraphicsBeginImageContext(size.size)
        
        let areaSize = CGRect(x: (size.width / 2 - 25), y: (size.height / 2 - 25), width: 50, height: 50)
        bottomImage.draw(in: size)
        
        topImage.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

//MARK:- API

extension NewEventDetailVC
{
    func getEventDetailData() {
        
        let kURL = webUrls.baseUrl() + webUrls.getEventData()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "listing_id" : dictData["listing_id"].stringValue]
        
        print(param)
        
        startLoader()
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            print(json)
            if json["flag"].stringValue == "1" {
               
                
                self.dictEventDetailData = json["data"]
                self.tblEventDetail.delegate = self
                self.tblEventDetail.dataSource = self
               // self.addInitialObjects()
                self.tblEventDetail.reloadData()
            }
        }
    }
}


extension NewEventDetailVC: KIImagePagerDataSource, KIImagePagerDelegate {
    func array(withImages pager: KIImagePager!) -> [Any]! {
        
        return eventImages
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
        if image == 0 {
            return .scaleToFill
        }
        return .scaleAspectFill
    }
    
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
        
        sliderCount = Int(index)
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
        if index == 1 {
          //  if let detail = dictEventDetailData {
                let array = dictEventDetailData["images"].arrayValue
                
                if array[1]["is_video"].stringValue == "2" {
                    
                    let imagePath = array[1]["img_path"].stringValue
                    print(imagePath)
                    if imagePath.contains("youtube.com") {
                        if let url = imagePath.toURL(), UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }
                        //                    youtubeVideo = YKYouTubeVideo.init(content: URL(string: imagePath)!)
                        //                    youtubeVideo?.play(.low)
                    } else {
                        
                        otherVideo = YKDirectVideo.init(content: URL(string: imagePath)!)
                        otherVideo?.play(YKQualityOptions.low)
                        
                        //                    let vc = SB.eventDetail.videoPlayerViewController()!
                        //                    vc.url = imagePath
                        //                    vc.eventType = eventType
                        //                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
           // }
        }
    }
}
