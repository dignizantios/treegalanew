//
//  BottomEventDetailVC.swift
//  treegala
//
//  Created by Jaydeep on 27/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import RxSwift
import AVKit
import DropDown
import MXScroll


class BottomEventDetailVC: UIViewController {
    
    //MARK:- Varible declaration
    
    var openTimeDD = DropDown()
    var chargeDD = DropDown()
    var dictEventDetailData = JSON()
    var disposeBag = DisposeBag()
    var eventType: EventType = .nightClub
    var selectedIndex = Int()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblEventInnerDetail: UITableView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblEventInnerDetail.alwaysBounceVertical = false
        self.tblEventInnerDetail.alwaysBounceHorizontal = false
        
        
        tblEventInnerDetail.register(UINib(nibName: "EventHeadlineCell", bundle: nil), forCellReuseIdentifier: "EventHeadlineCell")
        tblEventInnerDetail.register(UINib(nibName: "EventDesciptionCell", bundle: nil), forCellReuseIdentifier: "EventDesciptionCell")
        tblEventInnerDetail.register(UINib(nibName: "EventRatingCell", bundle: nil), forCellReuseIdentifier: "EventRatingCell")
        tblEventInnerDetail.register(UINib(nibName: "NightEventCell", bundle: nil), forCellReuseIdentifier: "NightEventCell")
        tblEventInnerDetail.register(UINib(nibName: "EventObstacleRaceCell", bundle: nil), forCellReuseIdentifier: "EventObstacleRaceCell")
        tblEventInnerDetail.register(UINib(nibName: "EventMusicLineUpCell", bundle: nil), forCellReuseIdentifier: "EventMusicLineUpCell")
        tblEventInnerDetail.register(UINib(nibName: "EventClubCrawelCell", bundle: nil), forCellReuseIdentifier: "EventClubCrawelCell")
        tblEventInnerDetail.register(UINib(nibName: "EventObstacleCell", bundle: nil), forCellReuseIdentifier: "EventObstacleCell")
        tblEventInnerDetail.register(UINib(nibName: "EventStreetHeaderCell", bundle: nil), forCellReuseIdentifier: "EventStreetHeaderCell")
        tblEventInnerDetail.register(UINib(nibName: "EventMusicHeaderCell", bundle: nil), forCellReuseIdentifier: "EventMusicHeaderCell")
        tblEventInnerDetail.register(UINib(nibName: "EventNightCell", bundle: nil), forCellReuseIdentifier: "EventNightCell")
        
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
      /*  setBackButton()
      //  setGradientBar()
        navigationItem.hidesBackButton = true
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        button.addTarget(self, action: #selector(self.logoClick), for: .touchUpInside)
        navigationItem.titleView = button*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tblEventInnerDetail.reloadData()
    }
   
    @objc func logoClick() {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- Tableview Delegate & Datasource

extension BottomEventDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1000
        {
            if dictEventDetailData["events"].arrayValue.count ==  0
            {
                if dictEventDetailData["events_page_link"].stringValue.isEmpty {
                    setTableViewBG("There are no events/festivals.", tableView: tableView)
                    /*if let cell = tblEventInnerDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventNightCell
                    {
                        cell.viewCalendar.isHidden = true
                        cell.btnEventCalendar.isHidden = true
                    }*/
                        
                    
                }
                return 0
            } else {
                tableView.backgroundView = nil
                return dictEventDetailData["events"].arrayValue.count
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1000
        {
            // Event Calender
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NightEventTableViewCell
            cell.selectionStyle = .none
            let item = dictEventDetailData["events"][indexPath.row]
            cell.lblDayName.text = item["day_name"].stringValue.capitalized
            cell.lblAmenties.text = item["day_description"].stringValue
            cell.lblUpparSep.isHidden = indexPath.row == 0 ? false : true            
            return cell
        }
        if selectedIndex == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventHeadlineCell") as! EventHeadlineCell
            setData(cell: cell)
            return cell
        }
        else if selectedIndex == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventNightCell") as! EventNightCell
            setDetail(cell: cell)
            print("Tableview Height \(tableView.frame.height)")
            return cell
        }
        else if selectedIndex == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventClubCrawelCell") as! EventClubCrawelCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicLineUpCell") as! EventMusicLineUpCell
            setDetail(cell: cell, isFromMusic: false, isImageData: false)
            return cell
        }
        else if selectedIndex == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicHeaderCell") as! EventMusicHeaderCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventMusicLineUpCell") as! EventMusicLineUpCell
            setDetail(cell: cell, isFromMusic: true, isImageData: false)
            return cell
        }
        else if selectedIndex == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventStreetHeaderCell") as! EventStreetHeaderCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 7
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventNightCell") as! EventNightCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleCell") as! EventObstacleCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 9
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventObstacleRaceCell") as! EventObstacleRaceCell
            setDetail(cell: cell)
            return cell
        }
        else if selectedIndex == 10
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventDesciptionCell") as! EventDesciptionCell
            setDescription(cell: cell)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventRatingCell") as! EventRatingCell
        setRattingData(cell: cell, json: dictEventDetailData, flg: true)
        return cell
        
    }
    
    func setTableViewBG(_ message: String, tableView: UITableView) {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 0, y: 10, width: tableView.frame.width, height: 50)
        noDataLabel.textAlignment = .center
        noDataLabel.text = message
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = .white
        noDataLabel.font = font.ubuntuCondensedRegular(size: 20.0)
        noDataLabel.tag = 1111
        tableView.backgroundView = noDataLabel
        
    }
    
}

//MARK:- Set Ratting Data
extension BottomEventDetailVC
{
    func setRattingData(cell:EventRatingCell,json:JSON,flg:Bool)  {
        
     //   print("json:\(json)")
        
        for button in cell.btnRates
        {
            button.addTarget(self, action: #selector(btnRatesTapped(_:)), for: .touchUpInside)
        }
        
        if flg == true{
            cell.btnViewComments.addTarget(self, action: #selector(btnCommentsTapped(_:)), for: .touchUpInside)
            /*cell.btnViewComments.rx.tap.asDriver()
                .drive(onNext: { [weak self] in
                    let vc = SB.eventDetail.commentListViewController()!
                    vc.listID = self?.dictEventDetailData["listing_id"].stringValue
                    self?.navigationController?.pushViewController(vc, animated: true)
                }).disposed(by: cell.disposeBag)
            cell.btnViewComments.rx.tap.subscribe(onNext: {
                let vc = SB.eventDetail.commentListViewController()!
                vc.listID = self.dictEventDetailData["listing_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }).disposed(by: cell.disposeBag)*/
            
            cell.btnResetRating.rx.tap.subscribe({_ in
                self.manageRating(flag: 0, cell: cell)
            }).disposed(by: disposeBag)
            
        }
        
        let rating = json["ratings"].exists() ? json["ratings"] : json
        
        if rating["ratting_type"].intValue == 0 {
            cell.btnResetRating.isHidden = true
            cell.btnRates.forEach { $0.isUserInteractionEnabled = true }
        } else {
            cell.btnResetRating.isHidden = false
            cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
            let flag = rating["ratting_type"].intValue - 1
            cell.btnRates[flag].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
            cell.btnRates.forEach { $0.isUserInteractionEnabled = false }
        }
        
        cell.lblTotalRate.text = "Based on \(rating["total_vote"].stringValue) Votes"
        
        cell.lblFunVote.text = "\(rating["fun_rate"].stringValue) Voted Fun"
        cell.lblSatisFactoryVote.text = "\(rating["satisfactory_rate"].stringValue) Voted Satisfactory"
        cell.lblBoringVote.text = "\(rating["boring_rate"].stringValue) Voted Boring"
        cell.lblDislikedVote.text = "\(rating["dislike_rate"].stringValue) Voted Disliked"
    }
    
    func manageRating(flag: Int,cell:EventRatingCell) {
        let kURL = webUrls.baseUrl() + webUrls.manageRating()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : dictEventDetailData["listing_id"].stringValue,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "ratting_type" : flag == 0 ? "" : "\(flag)"]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                let details = json["data"]
                self.setRattingData(cell: cell, json: details, flg: false)
                if flag == 0 {
                    cell.btnResetRating.isHidden = true
                    cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    cell.btnRates.forEach { $0.isUserInteractionEnabled = true }
                } else {
                    cell.btnResetRating.isHidden = false
                    cell.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    cell.btnRates[flag - 1].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
                    cell.btnRates.forEach { $0.isUserInteractionEnabled = false }
                }
            }
        }
        
    }
    
    @IBAction func btnCommentsTapped(_ sender:UIButton){
        let vc = SB.eventDetail.commentListViewController()!
        vc.listID = self.dictEventDetailData["listing_id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRatesTapped(_ sender: UIButton) {
        if isUserLogin() {
            let cell = tblEventInnerDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as! EventRatingCell
            manageRating(flag: sender.tag + 1, cell: cell)
        } else {
            self.presentLogin()
        }
    }
}


//MARK:- Set Decription Data
extension BottomEventDetailVC
{
    func setDescription(cell:EventDesciptionCell) {
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        
        if dictEventDetailData["description"]["desc"].stringValue.isEmpty {
            cell.lblDescription.textAlignment = .center
            cell.lblDescription.text = keys.notMention()
        } else {
            cell.lblDescription.text = dictEventDetailData["description"]["desc"].stringValue
        }
    }
}

//MARK:- Set Obstacle Data
extension BottomEventDetailVC
{
    func setDetail(cell:EventObstacleRaceCell)
    {
        let item = dictEventDetailData["race_details"]
        
        let level = item["difficulty_level"].intValue
        
        cell.viewDifficulty.isHidden = (level == 0)
        if level > 0 {
            cell.btnDifficulties.forEach { $0.isHidden = true }
            cell.btnDifficulties[level - 1].isHidden = false
        }
        
        cell.viewPrizes.isHidden = item["prizes_text"].stringValue.isEmpty
        cell.lblPrize.text = item["prizes_text"].stringValue
        
        cell.viewOtherDetail.isHidden = item["other_details"].stringValue.isEmpty
        cell.lblOtherDetails.text = item["other_details"].stringValue
        
        if cell.viewDifficulty.isHidden == true && cell.viewPrizes.isHidden == true && cell.viewOtherDetail.isHidden == true
        {
            cell.lblDisclosed.isHidden = false
        }
        else
        {
            cell.lblDisclosed.isHidden = true
        }
    }
}

//MARK:- Tableview Extension


/*extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}*/

//MARK:- Set MusicLineUp Cell
extension BottomEventDetailVC
{
    func setDetail(cell:EventMusicLineUpCell,isFromMusic:Bool,isImageData:Bool)
    {
        var lineupDescription = ""
        var isData = isImageData
        cell.btnDescription.addTarget(self, action: #selector(btnImagePreview(_:)), for: .touchUpInside)
        if isFromMusic {
            
            if dictEventDetailData["lineup"]["desc"].stringValue.isEmpty && dictEventDetailData["lineup"]["img"].stringValue.isEmpty {
                cell.lblDescription.text = keys.notMention()
                cell.lblDescription.textAlignment = .center
                cell.imgLineUp.isHidden = true
                cell.btnDescription.isHidden = true
                return
            }
            
            lineupDescription = dictEventDetailData["lineup"]["desc"].stringValue
            
            let imageURL = dictEventDetailData["lineup"]["img"].stringValue
            isData = !imageURL.isEmpty
            cell.imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: nil)
            
            cell.imgLineUp.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: { (image, error, SDImageCacheTypeNone, url) in
                guard let image = image else { return }
                print("Image arrived!")
                cell.imgLineUp.image = self.resizeImage(image: image, newWidth: 300)
            })
            
            
        } else {
            
            if dictEventDetailData["details"]["detail_tab"].stringValue.isEmpty {
                cell.lblDescription.text = keys.notMention()
                cell.lblDescription.textAlignment = .center
                cell.imgLineUp.isHidden = true
                cell.btnDescription.isHidden = true
                return
            }
            
            lineupDescription = dictEventDetailData["details"]["detail_tab"].stringValue
        }
        
        cell.lblDescription.text = lineupDescription
        
        cell.imgLineUp.isHidden = !isData
        cell.btnDescription.isHidden = !isData
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func btnImagePreview(_ sender: UIButton) {
        let vc = ImagePreviewPopup()
        vc.imageURL = dictEventDetailData["lineup"]["img"].stringValue
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK:- Set EventNight Data

extension BottomEventDetailVC
{
    func setDetail(cell:EventNightCell) {
        
        let events = dictEventDetailData["events"].arrayValue
        
        if !events.isEmpty && !dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = true
            cell.btnEventCalendar.isHidden = false
        } else if events.isEmpty && !dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = false
            cell.btnEventCalendar.isHidden = true
        } else if !events.isEmpty && dictEventDetailData["events_page_link"].stringValue.isEmpty {
            cell.viewCalendar.isHidden = true
            cell.btnEventCalendar.isHidden = true
        }
        else{
            cell.viewCalendar.isHidden = true
            cell.btnEventCalendar.isHidden = true
        }
        
        cell.btnEventCalendarUppar.addTarget(self, action: #selector(btnEventCalendar(_:)), for: .touchUpInside)
        cell.btnEventCalendarBottom.addTarget(self, action: #selector(btnEventCalendar(_:)), for: .touchUpInside)
        
        cell.calendarHeight = cell.btnEventCalendar.isHidden ? 0.0 : 88.0

        cell.tableView.register(nib.nightEventCell(), forCellReuseIdentifier: "cell")
        //
        cell.tableView.separatorStyle = .none
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.reloadData()
//        cell.heightOfTableview.constant = cell.tableView.contentSize.height
//        print("Tableview Height \(cell.heightOfTableview.constant)")
        cell.handlerForTableHeight = {[weak self] (height) in
            print("inner tableview height \(height)")
            cell.heightOfTableview.constant = height
//            cell.calendarHeight = cell.btnEventCalendar.isHidden ? 0.0 : 88.0
        }
        cell.contentView.layoutSubviews()
        cell.contentView.layoutIfNeeded()
    }
    
    @objc func setupBottomView(cell:EventNightCell){
        cell.calendarHeight = cell.btnEventCalendar.isHidden ? 0.0 : 88.0
        cell.contentView.layoutSubviews()
        cell.contentView.layoutIfNeeded()
    }
}






//MARK:- Set Obstacle Cell Data

extension BottomEventDetailVC
{
    func setDetail(cell:EventObstacleCell) {
        let item = dictEventDetailData["headlines"]
        
        //let charge = item["ticket_range"].stringValue
        
       // cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
       // cell.lblRegister.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        cell.lblRegister.text = item["website"].stringValue.isEmpty ? keys.notMention() : "Click Here for Website"
        
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        
        cell.viewCourseDetail.isHidden = item["course_details"].stringValue.isEmpty
        cell.lblCourseDetail.text = item["course_details"].stringValue
        
        cell.viewFreeSwag.isHidden = item["free_swag"].stringValue.isEmpty
        cell.lblFreeSwag.text = item["free_swag"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        cell.viewFacts.isHidden = item["local_other_facts"].stringValue.isEmpty
        cell.lblOtherFact.text = item["local_other_facts"].stringValue
        
        if let time = item["hedline_dates"].arrayValue.first {
            if time["is_date"].stringValue == "1"
            {
                cell.lblDate.text = time["date_text"].stringValue
            }
            else
            {
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "yyyy-MM-dd"
                
                if let date = dateFmt.date(from: time["start_date"].stringValue) {
                    dateFmt.dateFormat = "d MMM yyyy"
                    cell.lblDate.text = dateFmt.string(from: date)
                    
                    var endDateStr = ""
                    dateFmt.dateFormat = "yyyy-MM-dd"
                    if let endDate = dateFmt.date(from: time["end_date"].stringValue) {
                        dateFmt.dateFormat = "d MMM yyyy"
                        endDateStr = dateFmt.string(from: endDate)
                    }
                    
                    if endDateStr != "" {
                        cell.lblDate.text = dateFmt.string(from: date) + " - \(endDateStr)"
                    }
                }
            }
        }
        else
        {
            cell.lblDate.text = "Undisclosed"
        }
        
        cell.lblRegister.onClick = {
            if item["website"].stringValue == ""
            {
                return
            }
            if let url = (item["website"].stringValue).toURL() {
//            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
}

//MARK:- Set Street Data

extension BottomEventDetailVC
{
    func setDetail(cell:EventStreetHeaderCell) {
        let item = dictEventDetailData["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        //cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : "Click Here for Website"
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        
        cell.viewFacts.isHidden = item["other_facts"].stringValue.isEmpty
        cell.lblFacts.text = item["other_facts"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        let times = item["hedline_dates"].arrayValue
        
        if times.count == 1 {
            let item = times[0]
            
            if item["is_date"].stringValue == "1"
            {
                cell.lblDates.text = item["date_text"].stringValue
            }
            else
            {
                if item["end_date"].stringValue == "0000-00-00" {
                    cell.lblDates.text = setDates(from: times)
                } else {
                    
                    let date = item["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy") + " - " + item["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy")
                    cell.lblDates.text = date
                    // openTimes.value = [date]
                }
            }
            
        } else {
            cell.lblDates.text = setDates(from: times)
        }
        
        cell.lblWebSite.onClick = {
            if item["website"].stringValue == ""
            {
                return
            }
//            if let url = ("http://" + item["website"].stringValue).toURL() {
            if let url = (item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
}

//MARK:- Set MusicHeader Data

extension BottomEventDetailVC
{
    func setDetail(cell: EventMusicHeaderCell)
    {
        let item = dictEventDetailData["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        //cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : "Click Here for Website"
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        
        cell.viewGenres.isHidden = item["genres"].stringValue.isEmpty
        cell.lblGenres.text = item["genres"].stringValue
        
        cell.viewHeadline.isHidden = item["hadeliner_text"].stringValue.isEmpty
        cell.lblHeadline.text = item["hadeliner_text"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        let times = item["hedline_dates"].arrayValue
        
        if times.count == 1 {
            let item = times[0]
            
            if item["is_date"].stringValue == "1"
            {
                cell.lblDates.text = item["date_text"].stringValue
            }
            else
            {
                if item["end_date"].stringValue == "0000-00-00" {
                    cell.lblDates.text = setDates(from: times)
                } else {
                    
                    let date = item["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy") + " - " + item["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM yyyy")
                    cell.lblDates.text = date
                    //openTimes.value = [date]
                }
            }
            
        } else {
            cell.lblDates.text = setDates(from: times)
        }
        
        cell.lblWebSite.onClick = {
            if item["website"].stringValue == ""
            {
                return
            }
//            if let url = ("http://" + item["website"].stringValue).toURL() {
             if let url = (item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func setDates(from times: [JSON]) -> String {
        
        var dates: [String] = []
        
        for i in times {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            var string = ""
            
            if let date = dateFmt.date(from: i["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM yyyy"
                string = dateFmt.string(from: date)
            }
            print("Current Dict \(i)")
            dateFmt.dateFormat = "yyyy-MM-dd"
            if let date2 = dateFmt.date(from: i["end_date"].stringValue) {
                dateFmt.dateFormat = "d MMM yyy"
                let temp = " - \(dateFmt.string(from: date2))"
                string += temp
            }            
            
           /* dateFmt.dateFormat = "hh:mm:ss"
            if let startTime = dateFmt.date(from: i["start_time"].stringValue), let endTime = dateFmt.date(from: i["end_time"].stringValue) {
                dateFmt.dateFormat = "h:mm a"
                let temp = " - \(dateFmt.string(from: startTime)) to \(dateFmt.string(from: endTime))"
                
                string += temp
            }*/
            dates.append(string)
        }
        
        // openTimes.value = dates
        
        return dates.joined(separator: "\n")
    }
}


//MARK:- Set Club Crawl Data
extension BottomEventDetailVC
{
    func setDetail(cell:EventClubCrawelCell) {
        
        let item = dictEventDetailData["headlines"]
        
       // cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : "Click Here for Website"
        
        if dictEventDetailData["category_id"].stringValue != "1"
        {
            cell.vwTicketRange.isHidden = true
            //Hidden
        }
        else
        {
            cell.vwTicketRange.isHidden = false
            // Show
        }
        
        
        
        let charge = item["ticket_range"].stringValue
        
        cell.lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        
        cell.viewBusStop.isHidden = item["bus_stop_text"].stringValue.isEmpty
        cell.lblBusStop.text = item["bus_stop_text"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        
        if let time = item["hedline_dates"].arrayValue.first {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            if let date = dateFmt.date(from: time["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM yyyy"
                cell.lblDate.text = dateFmt.string(from: date)
            }
        }
        else
        {
            cell.lblDate.text = "Undisclosed"
        }
        
        cell.lblWebSite.onClick = {
            if item["website"].stringValue == ""
            {
                return
            }
//            if let url = ("http://" + item["website"].stringValue).toURL() {
             if let url = (item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
}


//MARK:-  Set Event

extension BottomEventDetailVC
{
    @IBAction func btnEventCalendar(_ sender: UIButton) {
        if let url = dictEventDetailData["events_page_link"].stringValue.toURL() {
            UIApplication.shared.openURL(url)
        }
    }
}

//MARK:- Headline Cell

extension BottomEventDetailVC
{
    func setData(cell:EventHeadlineCell)  {
        configDropdown(dropdown: openTimeDD, sender: cell.txtTime,cell:cell)
        configDropdown(dropdown: chargeDD, sender: cell.lblCharge,cell:cell)
        cell.txtTime.delegate = self
        
        print("DictEventData:\(self.dictEventDetailData)")
        
       
        
        let item = dictEventDetailData["headlines"]
        
        //cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        cell.lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : "Click Here for Website"
        
        
        cell.viewPhone.isHidden = dictEventDetailData["description"]["phone_number"].stringValue.isEmpty
        cell.btnPhone.setTitle(dictEventDetailData["description"]["phone_number"].stringValue, for: .normal)
        
        cell.btnPhone.rx.tap.subscribe(onNext: {
            var phoneStr: String = "telprompt://\(self.dictEventDetailData["description"]["phone_number"].stringValue)"
            print("PhoneStr: \(phoneStr)")
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "[-()N.A ]", with: "",options: .regularExpression)
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(urlPhone!)
                }
            }
        }).disposed(by: disposeBag)
        cell.viewGenres.isHidden = item["genres"].stringValue.isEmpty
        cell.lblGenres.text = item["genres"].stringValue
        
        cell.viewDressCode.isHidden = item["dress_code"].stringValue.isEmpty
        //   cell.txtvwDressCode.text = item["dress_code"].stringValue
        cell.lblDressCode.text = item["dress_code"].stringValue
        
        cell.viewFeature.isHidden = item["tags"].stringValue.isEmpty
        cell.lblFeature.text = item["tags"].stringValue
        // cell.txtOtherResearch.text = item["tags"].stringValue
        
        cell.btnTimeDropDown.isHidden = false
        
        displayTimes(cell: cell)
        
        let charge = item["cover_charge"].arrayValue
        
        if charge.isEmpty {
            
            if item["is_free"].stringValue == "0"
            {
                cell.lblCharge.text = "Undisclosed"
            }
            else if item["is_free"].stringValue == "1"
            {
//                cell.lblCharge.text = "Free"
                cell.lblCharge.text = "Undisclosed"
            }
            
           // cell.lblCharge.text = keys.notMention()
            cell.btnDropDown.isHidden = true
        } else if charge.count == 1 {
            let firstObj = charge[0]
            if firstObj["is_flag"].stringValue == "1" {
                cell.lblCharge.text = (firstObj["cover_charge"].stringValue)
                cell.btnDropDown.isHidden = true
            } else {
                displayCharges(charge, cell: cell)
            }
        } else {
            displayCharges(charge, cell: cell)
        }
        
        cell.lblWebSite.onClick = {
            if item["website"].stringValue == ""
            {
                return
            }
            
//            if let url = ("http://" + item["website"].stringValue).toURL() {
            if let url = (item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        cell.lblCharge.onClick = {
            self.chargeDD.show()
        }
        
        cell.btnViewMap.rx.tap.subscribe(onNext: {
            
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: cell.btnViewMap, cell: cell)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.dictEventDetailData
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
            
        }).disposed(by: disposeBag)
        
         viewDidLayoutSubviews()
    }
    
    func displayCharges(_ array: [JSON],cell:EventHeadlineCell) {
        // cell.btnDropDown.isHidden = false
        let items = array.map { $0["day_name"].stringValue + " " + $0["cover_charge"].stringValue }
        print(items)
        
        print(Date().getDayOfWeek())
        
        let temp = (array.map { $0["is_day"].intValue }).filter { $0 >= Date().getDayOfWeek() }.sorted(by: { $0 < $1 })
        print(temp)
        
        if let i = temp.first {
            if let index = array.index(where: { $0["is_day"].stringValue == "\(i)" }) {
                let obj = array[index]
                cell.lblCharge.text = String(obj["day_name"].stringValue.prefix(3)) + " " + obj["cover_charge"].stringValue
            }
        } else {
            cell.lblCharge.text = items.first
        }
        
        chargeDD.dataSource = items
    }
    
    func displayTimes(cell:EventHeadlineCell) {
        
        let times = dictEventDetailData["headlines"]["hedline_dates"].arrayValue
        
        var items: [String] = []
        
        for i in times {
            if let startDate = i["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = i["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "hh:mm a"
                let temp = i["day_name"].stringValue + " " + dateFmt.string(from: startDate) + " - "
                    + dateFmt.string(from: endDate)
                items.append(temp)
            }
        }
        
        openTimeDD.dataSource = items
        
        if let index = times.index(where: { $0["is_day"].intValue == Date().getDayOfWeek() }) {
            let object = times[index]
            
            if let startDate = object["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = object["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                var date = Date()
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "HH:mm:ss"
                
                let tempStr = dateFmt.string(from: date)
                
                date = dateFmt.date(from: tempStr)!
                
                print(startDate)
                print(endDate)
                
                
                dateFmt.dateFormat = "hh:mm a"
                if date < startDate {
                    //Open at time
                    cell.txtTime.attributedText = createAttributedTime(.orange, string1: "OPENS ", string2: "at \(dateFmt.string(from: startDate))")
                    
                } else if date > endDate {
                    // Closed at end time
                    cell.txtTime.attributedText = createAttributedTime(.red, string1: "CLOSED ", string2: "at \(dateFmt.string(from: endDate))")
                } else if date > startDate && date < endDate {
                    //Open until endtime
                    
                    cell.txtTime.attributedText = createAttributedTime(.green, string1: "OPENED ", string2: "until \(dateFmt.string(from: endDate))")
                }
            }
            
        } else if times.isEmpty {
            cell.txtTime.text = keys.notMention()
            cell.btnTimeDropDown.isHidden = true
        } else {
            //txtTime.textColor = .red
            
            let attribute1 = [NSAttributedStringKey.foregroundColor : UIColor.white]
            
            let attrString1 = NSMutableAttributedString(string: "TODAY ", attributes: attribute1)
            
            let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.red]
            
            let attrString2 = NSAttributedString(string: "CLOSED", attributes: attribute2)
            
            attrString1.append(attrString2)
            
            cell.txtTime.attributedText = attrString1
        }
    }
    
    func createAttributedTime(_ color: UIColor, string1: String, string2: String) -> NSAttributedString {
        
        let attribute1 = [NSAttributedStringKey.foregroundColor : color]
        
        let attrString1 = NSMutableAttributedString(string: string1, attributes: attribute1)
        
        let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let attrString2 = NSAttributedString(string: string2, attributes: attribute2)
        
        attrString1.append(attrString2)
        
        return attrString1
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView,cell:EventHeadlineCell) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = cell.viewTime.frame.width
        
        if dropdown == openTimeDD {
            dropdown.width = cell.viewTime.frame.width + 50
        }
        
        dropdown.selectionBackgroundColor = .white
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }    
    
}


extension BottomEventDetailVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let cell = tblEventInnerDetail.cellForRow(at: IndexPath(row: 0, section: 0)) as? EventHeadlineCell
        {
            if textField == cell.txtTime {
                openTimeDD.show()
                return false
            }
        }
        return true
    }
}

extension BottomEventDetailVC:MXViewControllerViewSource{
    func viewForMixToObserveContentOffsetChange() -> UIView {
        return self.tblEventInnerDetail
    }
    
   /* func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tblEventInnerDetail {
            self.tblEventInnerDetail.isScrollEnabled = (self.tblEventInnerDetail.contentOffset.y > 0)
        }
    }*/

}




