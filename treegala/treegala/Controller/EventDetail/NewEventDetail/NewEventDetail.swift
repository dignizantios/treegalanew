//
//  NewEventDetail.swift
//  treegala
//
//  Created by Jaydeep on 26/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import MXScroll
import RxSwift
import SwiftyJSON
import AVKit
import SDWebImage
import ZoomableImageSlider
//import YouTubePlayer
import XCDYouTubeKit
import AVFoundation

class NewEventDetail: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictData = JSON()
    var favoriteObserver = PublishSubject<Bool>()
    var eventImages: [Any] = [] {
        didSet {
            if sliderCount == 0
            {
                self.btnBack.isHidden = true
            }else{
                [btnBack,btnForward].forEach { $0?.isHidden = eventImages.count < 2 }
            }
            
        }
    }
    
    var sliderCount = 0 {
        didSet {
            btnBack.isHidden = sliderCount == 0
            btnForward.isHidden = sliderCount == (eventImages.count - 1)
        }
    }
    
    var otherVideo: YKDirectVideo?
    var youtubeVideo: YKYouTubeVideo?
    
    var disposeBag = DisposeBag()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var scrollHeader: UIScrollView!
    @IBOutlet weak var imgSlider: KIImagePager!
    @IBOutlet weak var lblEventName: UILabel!
    
    @IBOutlet weak var widthOfClaim: NSLayoutConstraint!
    @IBOutlet weak var btnClaim: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var lblEventLocation: UILabel!
    
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    //MARK:- Viewlife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        btnFavorite.rx.tap.subscribe(onNext: {
            
            if isUserLogin() {
                let kURL = webUrls.baseUrl() + webUrls.eventFavorite()
                
                let param = ["user_id" : getUserDetail("user_id"),
                             "access_token" : getUserDetail("access_token"),
                             "listing_id" : self.dictData["listing_id"].stringValue,
                             "lang" : "0",
                             "timezone" : TimeZone.current.identifier ]
                
                GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                    print(json)
                    if json["flag"].stringValue == "1" {
                        self.btnFavorite.isSelected = json["data"]["is_favourite"].boolValue
                    }
                })
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)
        
        btnClaim.rx.tap.subscribe(onNext: {
            if isUserLogin() {
                 let status = self.dictData["is_claimed_by"].stringValue
                    let vc = ClaimSubmitPopup()
                    vc.dictData = self.dictData
                    vc.claimStatusObserver.subscribe(onNext: { (status) in
                        if status {
                            self.btnClaim.setTitle(" Requested", for: .normal)
                            self.widthOfClaim.constant = 100
                        }
                    }).disposed(by: self.disposeBag)
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                
            } else {
                self.presentLogin()
            }
        }).disposed(by: disposeBag)
        
        btnForward.rx.tap.subscribe(onNext: {
            print(self.sliderCount)
            if self.sliderCount < self.eventImages.count - 1 {
                self.sliderCount += 1
                let index = UInt(self.sliderCount)
                print(index)
                self.imgSlider.setCurrentPage(index, animated: true)
            }
        }).disposed(by: disposeBag)
        
        btnBack.rx.tap.subscribe(onNext: {
            print(self.sliderCount)
            if self.sliderCount > 0 {
                self.sliderCount -= 1
                let index = UInt(self.sliderCount)
                self.imgSlider.setCurrentPage(index, animated: true)
            }
        }).disposed(by: disposeBag)
        
        btnBack.isHidden = true
        setupUI()
    }
    
    func setupUI() {
        
        eventImages = []
        sliderCount = 0
        
        //setBackButton()
       // setGradientBar()
        
        imgSlider.slideshowTimeInterval = UInt(0.0)
        imgSlider.indicatorDisabled = false
        imgSlider.imageCounterDisabled = true
        imgSlider.hidePageControlForSinglePages = true
        imgSlider.delegate = self
        
      /*  let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        button.addTarget(self, action: #selector(self.logoClick), for: .touchUpInside)
        navigationItem.titleView = button*/
        
      //  setCategoryUI()
        
        setData(dictData)
    }
    
    @objc func logoClick() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

}


//MARK:- Setup UI

extension NewEventDetail
{
    func setData(_ json: JSON) {
        lblEventName.text = json["title"].stringValue
        lblEventLocation.text = json["location"].stringValue
        
        self.widthOfClaim.constant = 75
        
        let status = json["is_claimed_by"].stringValue
        
        if status == "0" {
            btnClaim.isSelected = false
        } else if status == "-1" {
            btnClaim.isSelected = false
            btnClaim.setTitle(" Requested", for: .normal)
            self.widthOfClaim.constant = 100
            btnClaim.isUserInteractionEnabled = false
        } else {
            btnClaim.isSelected = true
            btnClaim.isUserInteractionEnabled = false
        }
        
        var imageArray: [Any] = []
        
        for i in json["images"].arrayValue {
            let imagePath = i["img_path"].stringValue
            if i["is_video"].stringValue == "2" {
                if imagePath.contains("youtube.com") {
                    let videoId = imagePath.components(separatedBy: "v=")[1]
                    let path = "https://img.youtube.com/vi/\(videoId)/0.jpg"
                    
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    SDWebImageManager.shared().imageDownloader?.downloadImage(with: path.toURL(), options: .highPriority, progress: nil, completed: { (image, data, error, status) in
                        imageArray[1] = self.createVideoView(object: image ?? #imageLiteral(resourceName: "ic_image_placeholder"))
                        self.eventImages = imageArray
                        self.imgSlider.reloadData()
                    })
                } else {
                    imageArray.append(self.createVideoView(object: #imageLiteral(resourceName: "ic_image_placeholder")))
                    
                    DispatchQueue.global().async {
                        let image = self.getThumbnailImage(forUrl: imagePath.toURL()!) ?? UIImage()
                        DispatchQueue.main.async {
                            imageArray[1] = self.createVideoView(object: image)
                            self.eventImages = imageArray
                            self.imgSlider.reloadData()
                        }
                    }
                }
            } else {
                imageArray.append(imagePath)
            }
        }
        
        print(imageArray)
        
        eventImages = imageArray
        imgSlider.reloadData()
       
       
        
        btnFavorite.isSelected = json["is_favourite"].boolValue
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    func createVideoView(object: UIImage) -> UIImage {
        
        let bottomImage = object
        let topImage = #imageLiteral(resourceName: "ic_play_video")
        
        let size = CGRect(x: 0, y: 0, width: 300, height: 150)
        UIGraphicsBeginImageContext(size.size)
        
        let areaSize = CGRect(x: (size.width / 2 - 25), y: (size.height / 2 - 25), width: 50, height: 50)
        bottomImage.draw(in: size)
        
        topImage.draw(in: areaSize, blendMode: .normal, alpha: 0.8)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

//MARK:- Scrollview

extension NewEventDetail: MXViewControllerViewSource {
    func headerViewForMixObserveContentOffsetChange() -> UIView? {
        // if scrollHeader != nil {
            return scrollHeader
      //  }
      //  return nil
    }
}


extension NewEventDetail: KIImagePagerDataSource, KIImagePagerDelegate {
    func array(withImages pager: KIImagePager!) -> [Any]! {
        
        return eventImages
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
        if image == 0 {
            return .scaleToFill
        }
        return .scaleAspectFill
    }
    
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
        
        sliderCount = Int(index)
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
        if index == 1 {
         //  if let detail = eventDetail {
                let array = dictData["images"].arrayValue
                
                if array[1]["is_video"].stringValue == "2" {
                    
                    let imagePath = array[1]["img_path"].stringValue
                    print(imagePath)
                    if imagePath.contains("youtube.com") {
                        
                        playVideoInView(strVideoURL: imagePath)
                                /*let obj = YoutubeVideoVC()
                                obj.strVideoURL = imagePath
                                obj.modalPresentationStyle = .overCurrentContext
                                obj.modalTransitionStyle = .crossDissolve
                                self.present(obj, animated: true, completion: nil)*/
                        
                        /*if let url = imagePath.toURL(), UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }*/
                        //                    youtubeVideo = YKYouTubeVideo.init(content: URL(string: imagePath)!)
                        //                    youtubeVideo?.play(.low)
                    } else {
                        
                        otherVideo = YKDirectVideo.init(content: URL(string: imagePath)!)
                        otherVideo?.play(YKQualityOptions.low)
                        
                        //                    let vc = SB.eventDetail.videoPlayerViewController()!
                        //                    vc.url = imagePath
                        //                    vc.eventType = eventType
                        //                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
           // }
        }
        else
        {
            let imgURL = dictData["images"].arrayValue
            var urls = [String]()
            for i in 0..<imgURL.count
            {
                if imgURL[i]["is_video"].stringValue == "1" || imgURL[i]["is_video"].stringValue == "3"
                {
                    urls.append(imgURL[i]["img_path"].stringValue)
                }
            }
            let vc = ZoomableImageSlider(images: urls, currentIndex: Int(index), placeHolderImage:UIImage.init(named: "ic_image_placeholder"))
            present(vc, animated: true, completion: nil)
        }
    }
    
    func playVideoInView(strVideoURL:String) {
        print("Youtube ID \(strVideoURL.youtubeID ?? "")")
        
        /*
        let videoPlayerViewController = XCDYouTubeVideoPlayerViewController(videoIdentifier: strVideoURL.youtubeID)
        
        presentMoviePlayerViewControllerAnimated(videoPlayerViewController)
*/
        
        
        
        let playerViewController = AVPlayerViewController()
        
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:self.view.frame.height)
        
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
        //        weakPlayerViewController?.delegate = self
        
       
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL.youtubeID ?? "") { [weak self] (video, error) in
            /*if let obj = self {
             obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
             }*/
            
            
            if let streamURLs = video?.streamURLs, let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs[XCDYouTubeVideoQuality.HD720.rawValue] ?? streamURLs[XCDYouTubeVideoQuality.medium360.rawValue] ?? streamURLs[XCDYouTubeVideoQuality.small240.rawValue]) {
                weakPlayerViewController?.player = AVPlayer(url: streamURL)
                weakPlayerViewController?.player?.play()
            } else {
            }
            
            /*
            if video != nil {
                var streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                    weakPlayerViewController?.player?.play()
                }
                
                print("Errr:\(error?.localizedDescription)")
                
                //
            } else {
            }*/
        }
        
        self.present(weakPlayerViewController!, animated: true, completion: nil)

    }
}





