//
//  NightEventViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift

class NightEventViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var btnEventCalendar: UIView!
    
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    
    var eventDetail: JSON?
    
    var events: [JSON] = []
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!
    
    var heightOfView: CGFloat = 0.0
    var tableHeight: CGFloat = 0.0
    var calendarHeight: CGFloat = 0.0
    
    var isScrollDown = false
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        events = eventDetail!["events"].arrayValue
        
        if !events.isEmpty && !eventDetail!["events_page_link"].stringValue.isEmpty {
            viewCalendar.isHidden = true
            btnEventCalendar.isHidden = false
        } else if events.isEmpty && !eventDetail!["events_page_link"].stringValue.isEmpty {
            viewCalendar.isHidden = false
            btnEventCalendar.isHidden = true
        } else if !events.isEmpty && eventDetail!["events_page_link"].stringValue.isEmpty {
            viewCalendar.isHidden = true
            btnEventCalendar.isHidden = true
        }
        
        calendarHeight = btnEventCalendar.isHidden ? 0.0 : 88.0
        
        tableView.register(nib.nightEventCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        
        tableView.rx.observe(CGSize.self, "contentSize").subscribe(onNext: { (size) in
            self.tableHeight = size?.height ?? 0
        }).disposed(by: disposeBag)
    }
    
    @IBAction func btnEventCalendar(_ sender: UIButton) {
        if let url = eventDetail!["events_page_link"].stringValue.toURL() {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewWillLayoutSubviews() {
      /*  if (tableHeight + calendarHeight) < UIScreen.main.bounds.height {
            heightOfView = UIScreen.main.bounds.height
        } else {
            heightOfView = tableHeight + calendarHeight
        }
        offsetDeledate.offsetValueChanged(heightOfView)*/
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
    
    func setTableViewBG(_ message: String, tableView: UITableView) {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 0, y: 10, width: tableView.frame.width, height: 50)
        noDataLabel.textAlignment = .center
        noDataLabel.text = message
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = .white
        noDataLabel.font = font.ubuntuCondensedRegular(size: 20.0)
        noDataLabel.tag = 1111
        tableView.backgroundView = noDataLabel

    }
}

extension NightEventViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if events.isEmpty {
            if eventDetail!["events_page_link"].stringValue.isEmpty {
              setTableViewBG("There are no events/festivals.", tableView: tableView)
              viewCalendar.isHidden = true
              btnEventCalendar.isHidden = true
            }
            return 0
        } else {
            tableView.backgroundView = nil
            return events.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NightEventTableViewCell
        cell.selectionStyle = .none
        let item = events[indexPath.row]
        cell.lblDayName.text = item["day_name"].stringValue.capitalized
        cell.lblAmenties.text = item["day_description"].stringValue
        return cell
    }
    
}
