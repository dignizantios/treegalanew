//
//  NightHeadlineViewController.swift
//  treegala
//
//  Created by om on 8/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import RxSwift

class NightHeadlineViewController: UIViewController {
    
    var openTimeDD = DropDown()
    var chargeDD = DropDown()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var viewTimeCharge: UIView!
    
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var btnTimeDropDown: UIButton!
    
    @IBOutlet weak var viewCharge: UIView!
    @IBOutlet weak var lblCharge: LabelButton!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewGenres: UIView!
    @IBOutlet weak var lblGenres: UILabel!
    
    @IBOutlet weak var viewDressCode: UIView!
    @IBOutlet weak var lblDressCode : UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!

    var details: JSON?
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!
    
    var isScrollDown = false
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configDropdown(dropdown: openTimeDD, sender: txtTime)
        configDropdown(dropdown: chargeDD, sender: lblCharge)
        
        scrollView.delegate = self
        txtTime.delegate = self
        
        setData()

//        scrollView.rx
//            .observe(CGSize.self, "contentSize")
//            .subscribe(onNext: { (size) in
//            print(size)
//            if self.scrollView.contentSize.height < UIScreen.main.bounds.height {
//                self.heightOfView.constant = UIScreen.main.bounds.height
//                print("less")
//            } else {
//                self.heightOfView.constant = self.scrollView.contentSize.height
//                print("greater")
//            }
//            print("Offset Inside \(self.heightOfView.constant)")
//            self.offsetDeledate.offsetValueChanged(self.heightOfView.constant + 200.0)
//        }).disposed(by: disposeBag)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
      
        let height = scrollView.updateContentView()
        
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: height)
       /* if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
            print("less")
        } else {
            heightOfView.constant = scrollView.contentSize.height
            print("greater")
        }*/
      //  scrollView.resizeScrollViewContentSize()
     //   print("Offset Inside \(heightOfView.constant)")
     //   self.offsetDeledate.offsetValueChanged(heightOfView.constant)
        self.view.layoutIfNeeded()
    }
    
  

    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = viewTime.frame.width
        
        if dropdown == openTimeDD {
             dropdown.width = viewTime.frame.width + 30
        }
        
        dropdown.selectionBackgroundColor = .white
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
    func setData() {
        let item = details!["headlines"]
        
        lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        viewPhone.isHidden = details!["description"]["phone_number"].stringValue.isEmpty
        btnPhone.setTitle(details!["description"]["phone_number"].stringValue, for: .normal)
        
        viewGenres.isHidden = item["genres"].stringValue.isEmpty
        lblGenres.text = item["genres"].stringValue

        viewDressCode.isHidden = item["dress_code"].stringValue.isEmpty
        lblDressCode.text = item["dress_code"].stringValue

        viewFeature.isHidden = item["tags"].stringValue.isEmpty
        lblFeature.text = item["tags"].stringValue
        
        displayTimes()
        
        let charge = item["cover_charge"].arrayValue
        
        if charge.isEmpty {
            lblCharge.text = keys.notMention()
            self.btnDropDown.isHidden = true
        } else if charge.count == 1 {
            let firstObj = charge[0]
            if firstObj["is_flag"].stringValue == "1" {
                lblCharge.text = (firstObj["cover_charge"].stringValue)
                self.btnDropDown.isHidden = true
            } else {
                displayCharges(charge)
            }
        } else {
            displayCharges(charge)
        }

        lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        lblCharge.onClick = {
            self.chargeDD.show()
        }
        
        btnViewMap.rx.tap.subscribe(onNext: {

            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: self.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.details!
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }

        }).disposed(by: disposeBag)
        
        viewDidLayoutSubviews()
    }
    
    func displayCharges(_ array: [JSON]) {
        self.btnDropDown.isHidden = false
        let items = array.map { $0["day_name"].stringValue + " " + $0["cover_charge"].stringValue }
        print(items)
        
        print(Date().getDayOfWeek())
        
        let temp = (array.map { $0["is_day"].intValue }).filter { $0 >= Date().getDayOfWeek() }.sorted(by: { $0 < $1 })
        print(temp)
        
        if let i = temp.first {
            if let index = array.index(where: { $0["is_day"].stringValue == "\(i)" }) {
                let obj = array[index]
                self.lblCharge.text = String(obj["day_name"].stringValue.prefix(3)) + " " + obj["cover_charge"].stringValue
            }
        } else {
            lblCharge.text = items.first
        }

        chargeDD.dataSource = items
    }
    
    func displayTimes() {

        let times = details!["headlines"]["hedline_dates"].arrayValue

        var items: [String] = []
        
        for i in times {
            if let startDate = i["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = i["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "hh:mm a"
                let temp = i["day_name"].stringValue + " " + dateFmt.string(from: startDate) + " - "
                    + dateFmt.string(from: endDate)
                items.append(temp)
            }
        }
        
        openTimeDD.dataSource = items
        
        if let index = times.index(where: { $0["is_day"].intValue == Date().getDayOfWeek() }) {
            let object = times[index]
            
            if let startDate = object["start_time"].stringValue.toDate(format: "HH:mm:ss"), let endDate = object["end_time"].stringValue.toDate(format: "HH:mm:ss") {
                var date = Date()
                
                let dateFmt = DateFormatter()
                dateFmt.dateFormat = "HH:mm:ss"
                
                let tempStr = dateFmt.string(from: date)
            
                date = dateFmt.date(from: tempStr)!
                
                print(startDate)
                print(endDate)
                
                dateFmt.dateFormat = "h a"
                if date < startDate {
                    //Open at time
                 txtTime.attributedText = createAttributedTime(.green, string1: "OPEN ", string2: "at \(dateFmt.string(from: startDate))")
                   
                } else if date > endDate {
                    // Closed at end time
                    txtTime.attributedText = createAttributedTime(.red, string1: "CLOSED ", string2: "at \(dateFmt.string(from: endDate))")
                } else if date > startDate && date < endDate {
                    //Open until endtime
                    
                  txtTime.attributedText = createAttributedTime(.green, string1: "OPEN ", string2: "until \(dateFmt.string(from: endDate))")
                }
            }
            
        } else if times.isEmpty {
            txtTime.text = keys.notMention()
            btnTimeDropDown.isHidden = true
        } else {
            //txtTime.textColor = .red
            
            let attribute1 = [NSAttributedStringKey.foregroundColor : UIColor.white]
            
            let attrString1 = NSMutableAttributedString(string: "TODAY ", attributes: attribute1)
            
            let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.red]
            
            let attrString2 = NSAttributedString(string: "CLOSED", attributes: attribute2)
            
            attrString1.append(attrString2)
            
            txtTime.attributedText = attrString1
        }
    }
    
    func createAttributedTime(_ color: UIColor, string1: String, string2: String) -> NSAttributedString {

        let attribute1 = [NSAttributedStringKey.foregroundColor : color]
        
        let attrString1 = NSMutableAttributedString(string: string1, attributes: attribute1)
        
        let attribute2 = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let attrString2 = NSAttributedString(string: string2, attributes: attribute2)
        
        attrString1.append(attrString2)
        
        return attrString1
    }
}

extension UIScrollView {
    func updateContentView() -> CGFloat {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
        print("Height \(contentSize.height)")
        return contentSize.height
       // heightOfView.constant = contentSize.height
    }
}

extension NightHeadlineViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtTime {
            openTimeDD.show()
            return false
        }
        
        return true
    }
}

/*extension NightHeadlineViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
//        print(scrollView.contentOffset.y)
//        print(delta)
//
//        print("Top Constraints => \(topConstraintConstant)" )
//        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        //print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            //print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            //print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
           // print("velocity:=",velocity)
           // print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
           // print("diffrence:==>",diffrence)
        
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
}*/
