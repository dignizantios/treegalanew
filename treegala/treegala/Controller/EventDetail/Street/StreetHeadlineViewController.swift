//
//  StreetHeadlineViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import DropDown

class StreetHeadlineViewController: UIViewController {
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewFacts: UIView!
    @IBOutlet weak var lblFacts : UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    var openTimes = Variable<[String]>([])
    
    var disposeBag = DisposeBag()
    
    var detail: JSON?
    var isScrollDown = false
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(nib.streetOpenTimeCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none

        tableView.rx.setDelegate(self).disposed(by: disposeBag)

        openTimes.asObservable().bind(to: tableView.rx.items(cellIdentifier: "cell", cellType: StreetOpenTimeTableViewCell.self)) { row, data, cell in
              cell.lblTime.text = data
            }.disposed(by: disposeBag)
        
        setDetail()
    }
    
    override func viewWillLayoutSubviews() {
      /*  if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
        } else {
            heightOfView.constant = scrollView.contentSize.height
        }
        offsetDeledate.offsetValueChanged(heightOfView.constant)*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            heightOfTable.constant = tableView.contentSize.height
        }
    }
    
    func setDetail() {
        let item = detail!["headlines"]
        
        let charge = item["ticket_range"].stringValue
        
        lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        viewPhone.isHidden = detail!["description"]["phone_number"].stringValue.isEmpty
        btnPhone.setTitle(detail!["description"]["phone_number"].stringValue, for: .normal)
        
        viewFacts.isHidden = item["other_facts"].stringValue.isEmpty
        lblFacts.text = item["other_facts"].stringValue
        
        viewFeature.isHidden = item["tags"].stringValue.isEmpty
        lblFeature.text = item["tags"].stringValue
        
        let times = item["hedline_dates"].arrayValue
        
        if times.count == 1 {
            let item = times[0]
            
            if item["end_date"].stringValue == "0000-00-00" {
                setDates(from: times)
            } else {
                
                let date = item["start_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM") + " - " + item["end_date"].stringValue.convertDate(format: "yyyy-MM-dd", toFormat: "d MMM")
                
                openTimes.value = [date]
            }
        } else {
            setDates(from: times)
        }
        
        lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        btnViewMap.rx.tap.subscribe(onNext: {
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: self.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                    vc.eventDetail = self.detail!
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
    
    func setDates(from times: [JSON]) {
        
        var dates: [String] = []
        
        for i in times {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            var string = ""
            
            if let date = dateFmt.date(from: i["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM"
                string += dateFmt.string(from: date)
            }
            
            dateFmt.dateFormat = "HH:mm:ss"
            if let startTime = dateFmt.date(from: i["start_time"].stringValue), let endTime = dateFmt.date(from: i["end_time"].stringValue) {
                dateFmt.dateFormat = "h:mm a"
                let temp = " - \(dateFmt.string(from: startTime)) to \(dateFmt.string(from: endTime))"
                
                string += temp
            }
            dates.append(string)
        }
        
        openTimes.value = dates
    }
}

/*extension StreetHeadlineViewController {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            
            //            topConstraintConstant -= 5.0
            //            offsetDeledate.isScrollDown!(topConstraintConstant)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
}*/
