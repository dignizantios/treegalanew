//
//  MXExampleViewController.swift
//  MXScroll_Example
//
//  Created by cc x on 2018/7/20.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import MXScroll
import SwiftyJSON
//import FBAudienceNetwork

class MXHomeViewController: UIViewController {
    
    var mx:MXViewController<MSSegmentControl>!
    
    var json:JSON = JSON()
    var eventType: EventType = .nightClub
//    var interstitialAd: FBInterstitialAd!
    
    @IBOutlet weak var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(btnBackClicked))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
        self.navigationItem.titleView = setMXLogoTitle()        

        loadVC()
        
        /*interstitialAd = FBInterstitialAd(placementID: "YOUR_PLACEMENT_ID")
        interstitialAd.delegate = self
        interstitialAd.load()*/
    }
    
    @objc func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadVC(){
        
        let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
        let headerScroll = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetail") as! NewEventDetail
        
        var hh:UIViewController!
        hh = headerScroll
        headerScroll.dictData = json
        
        // var child1 : UIViewController!
        // var child2 :  UIViewController!
        
        let child1 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child1.dictEventDetailData = json
        
        let child2 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child2.dictEventDetailData = json
        
        let child3 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child3.dictEventDetailData = json
        child3.selectedIndex = 10
        
        let child4 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child4.dictEventDetailData = json
        child4.selectedIndex = 11
        
        var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
        switch eventType {
        case .nightClub:
            child1.selectedIndex = 0 // Headline
            
            child2.selectedIndex = 1 //event
            
            break
        case .clubCrawl:
            headerTitles = ["HEADLINES", "DETAILS", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 2 //clubcrawl
            
            child2.selectedIndex = 3 //musiclineup
            break
        case .music:
            headerTitles = ["HEADLINES", "LINEUP", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 4 //musicheadline
            
            child2.selectedIndex = 5 //musiclineup
            break
        case .street:
            child1.selectedIndex = 6 //streethealine
            
            child2.selectedIndex = 7 //nightevent
            break
        case .obstacle:
            headerTitles = ["HEADLINES", "RACE DETAILS", "DESCRIPTION", "RATINGS"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        case .local:
//            headerTitles = ["HEADLINES", "DESCRIPTION", "RATINGS"]
            headerTitles = ["HEADLINES", "DESCRIPTION"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
        }
        
        let segment = MSSegmentControl(sectionTitles: headerTitles)
        setupSegment(segmentView: segment)
        
        var mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child2,child3,child4], segmentView: segment)
        
        if eventType == .local {
            mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child3,child4], segmentView: segment)
        }
        
        mx.headerViewOffsetHeight = 0
       
        mx.view.backgroundColor = color.themePurple()
        mx.segmentViewHeight = 0
        //        mx.title = "Treegala"
        
        let Segment_statu_titleNormal =  UIFont.systemFont(ofSize: 20)//UIFont(name: "UbuntuCondensed-Regular", size: 13)!
        
        mx.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.blue, NSAttributedStringKey.font: Segment_statu_titleNormal]
        
        mx.shouldScrollToBottomAtFirstTime = false
        //        mx.extendedLayoutIncludesOpaqueBars = true
        //            mx.automaticallyAdjustsScrollViewInsets = true
//        mx.view.layoutSubviews()
        mx.view.layoutIfNeeded()
        mx.addLeftBarButtonWithImage(UIImage(named: "ic_arrow_back_white") ?? UIImage())
        mx.setBackButton()
        
        
        
        addChildViewController(mx)
        containerView.addSubview(mx.view)
        mx.view.frame = containerView.bounds
        mx.didMove(toParentViewController: self)
    }
    
    
   /* func setMXLogoTitle() -> UIImageView {
        let image =  UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 68, height: 28)
        image.image = #imageLiteral(resourceName: "ic_logo_header")
        return image
    }*/
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor = color.themePurple()
        segmentView.selectionIndicatorColor = .white
        segmentView.selectionIndicatorHeight = 3
        let Segment_statu_titleNormal = UIFont(name: "UbuntuCondensed-Regular", size: 13)!
        segmentView.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Segment_statu_titleNormal]
        segmentView.easy.reload()
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 2
        segmentView.selectionStyle = .fullWidth
    }
    
}

//MARK:- Facebook Ads Delegate

/*extension MXHomeViewController : FBInterstitialAdDelegate {
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        print("Load")
        interstitialAd.show(fromRootViewController: self)
    }
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        print("error \(error)")
    }
}*/
