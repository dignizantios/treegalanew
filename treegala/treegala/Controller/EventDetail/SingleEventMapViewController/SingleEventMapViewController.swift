//
//  SingleEventMapViewController.swift
//  treegala
//
//  Created by om on 9/24/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import MXScroll

class SingleEventMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    
     var eventType: EventType = .nightClub
    var location: CLLocation!
    
    var eventDetail: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(eventDetail)
        
        setBackButton()
        
        location = CLLocation.init(latitude: eventDetail["headlines"]["latitude"].doubleValue, longitude: eventDetail["headlines"]["longtitude"].doubleValue)
        print(location)
        
        mapView.delegate = self
        
        let marker = GMSMarker()
        
        marker.position = location.coordinate
        marker.infoWindowAnchor = CGPoint(x: 0.5, y: -0.1)
        marker.tracksInfoWindowChanges = true
        marker.icon = #imageLiteral(resourceName: "ic_pin_map_screen")
        marker.map = mapView
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        button.addTarget(self, action: #selector(self.btnBackTapped), for: .touchUpInside)
        navigationItem.titleView = button
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 16)
        mapView.camera = camera
        mapView.animate(to: camera)
    }
    
}

// MARK: - MapView Delegate method
extension SingleEventMapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let infoWindow = EventMapInfoView.loadFromNib() as! EventMapInfoView
        infoWindow.imgEvent.sd_setShowActivityIndicatorView(true)
        
        if let image = eventDetail["images"].arrayValue.first {
            infoWindow.imgEvent.sd_setImage(with: image["img_path"].stringValue.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_image_placeholder"), options: .highPriority, completed: nil)
        }
        infoWindow.lblName.text = eventDetail["title"].stringValue
        infoWindow.lblAddress.text = eventDetail["location"].stringValue
        return infoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
       /* print("Window Tapped")
        print(marker.userData)
        
        if let tag = marker.userData as? Int {*/
         //   let item = eventList[tag]
            /* let vc = SB.eventDetail.eventDetailViewController()!
             vc.eventType = EventType.type(index: item["category_id"].intValue - 1)
             EventDetailViewController.listID = item["listing_id"].stringValue
             self.navigationController?.pushViewController(vc, animated: true)*/
            eventType = EventType.type(index: eventDetail["category_id"].intValue - 1)
            getEventDetailData(eventId: eventDetail["listing_id"].stringValue ?? "0",                               tag: 0)
       // }
    }
    
}

extension SingleEventMapViewController
{
    func jumpToMixVC(json:JSON) {
        
        let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
        let headerScroll = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetail") as! NewEventDetail
        
        var hh:UIViewController!
        hh = headerScroll
        headerScroll.dictData = json
        
        // var child1 : UIViewController!
        // var child2 :  UIViewController!
        
        let child1 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child1.dictEventDetailData = json
        
        let child2 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child2.dictEventDetailData = json
        
        let child3 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child3.dictEventDetailData = json
        child3.selectedIndex = 10
        
        let child4 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child4.dictEventDetailData = json
        child4.selectedIndex = 11
        
        var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
        switch eventType {
        case .nightClub:
            child1.selectedIndex = 0 // Headline
            
            child2.selectedIndex = 1 //event
            
            break
        case .clubCrawl:
            headerTitles = ["HEADLINES", "DETAILS", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 2 //clubcrawl
            
            child2.selectedIndex = 3 //musiclineup
            break
        case .music:
            headerTitles = ["HEADLINES", "LINEUP", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 4 //musicheadline
            
            child2.selectedIndex = 5 //musiclineup
            break
        case .street:
            child1.selectedIndex = 6 //streethealine
            
            child2.selectedIndex = 7 //nightevent
            break
        case .obstacle:
            headerTitles = ["HEADLINES", "RACE DETAILS", "DESCRIPTION", "RATINGS"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        case .local:
//            headerTitles = ["HEADLINES", "DESCRIPTION", "RATINGS"]
            headerTitles = ["HEADLINES", "DESCRIPTION"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        }
        let segment = MSSegmentControl(sectionTitles: headerTitles)
        setupSegment(segmentView: segment)
        
        let mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child2,child3,child4], segmentView: segment)
        mx.headerViewOffsetHeight = 0
        mx.view.backgroundColor = color.themePurple()
        mx.navigationItem.titleView = setMXLogoTitle()
        mx.extendedLayoutIncludesOpaqueBars = true
        mx.shouldScrollToBottomAtFirstTime = false
        mx.setBackButton()
        navigationController?.pushViewController(mx, animated: true)
        
    }
    
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor = color.themePurple()
        segmentView.selectionIndicatorColor = .white
        segmentView.selectionIndicatorHeight = 3
        let Segment_statu_titleNormal = UIFont(name: "UbuntuCondensed-Regular", size: 13)!
        segmentView.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Segment_statu_titleNormal]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 2
        segmentView.selectionStyle = .fullWidth
    }
}

//MARK:- API

extension SingleEventMapViewController
{
    func getEventDetailData(eventId:String,tag:Int) {
        
        let kURL = webUrls.baseUrl() + webUrls.getEventData()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "listing_id" : eventId]
        
        print(param)
        
        startLoader()
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            print(json)
            if json["flag"].stringValue == "1" {
                
                let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
                let headerScroll: MXHomeViewController = eventStoryboard.instantiateViewController(withIdentifier: "MXHomeViewController") as! MXHomeViewController
                headerScroll.json = json["data"]
                headerScroll.eventType = self.eventType
                self.navigationController?.pushViewController(headerScroll, animated: true)
               // self.jumpToMixVC(json: json["data"])
                //self.dictEventDetailData = json["data"]
                // self.tblEventDetail.reloadData()
            }
        }
    }
}
