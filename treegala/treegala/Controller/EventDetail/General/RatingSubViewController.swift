//
//  RatingSubViewController.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

class RatingSubViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    @IBOutlet weak var btnResetRating: UIButton!
    
    @IBOutlet weak var lblFunVote: UILabel!
    @IBOutlet weak var lblSatisFactoryVote: UILabel!
    @IBOutlet weak var lblBoringVote: UILabel!
    @IBOutlet weak var lblDislikedVote: UILabel!
    
    @IBOutlet weak var lblTotalRate: UILabel!
    
    @IBOutlet var btnRates: [UIButton]!
    @IBOutlet weak var btnViewComments: UIButton!
    
    var isScrollDown = false
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!
    
    var disposeBag = DisposeBag()
    var details: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnViewComments.rx.tap.subscribe(onNext: {
            let vc = SB.eventDetail.commentListViewController()!
            self.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposeBag)
        
        btnResetRating.rx.tap.subscribe(onNext: {
            self.manageRating(flag: 0)
        }).disposed(by: disposeBag)
        
        setDetail()
 
    }
    
    override func viewWillLayoutSubviews() {
     /*   if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
        } else {
            heightOfView.constant = scrollView.contentSize.height
        }
        offsetDeledate.offsetValueChanged(heightOfView.constant)*/
    }
    
    func setDetail() {
        let rating = details!["ratings"].exists() ? details!["ratings"] : details!
        
        if rating["ratting_type"].intValue == 0 {
            btnResetRating.isHidden = true
            btnRates.forEach { $0.isUserInteractionEnabled = true }
        } else {
            btnResetRating.isHidden = false
            self.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
            let flag = rating["ratting_type"].intValue - 1
            self.btnRates[flag].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
            btnRates.forEach { $0.isUserInteractionEnabled = false }
        }

        lblTotalRate.text = "Based on \(rating["total_vote"].stringValue) Votes"
        
        lblFunVote.text = "\(rating["fun_rate"].stringValue) Voted Fun"
        lblSatisFactoryVote.text = "\(rating["satisfactory_rate"].stringValue) Voted Satisfactory"
        lblBoringVote.text = "\(rating["boring_rate"].stringValue) Voted Boring"
        lblDislikedVote.text = "\(rating["dislike_rate"].stringValue) Voted Disliked"
    }

    @IBAction func btnRatesTapped(_ sender: UIButton) {
        if isUserLogin() {
          manageRating(flag: sender.tag + 1)
        } else {
            self.presentLogin()
        }
    }
    
    func manageRating(flag: Int) {
        let kURL = webUrls.baseUrl() + webUrls.manageRating()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_id" : EventDetailViewController.listID,
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "ratting_type" : flag == 0 ? "" : "\(flag)"]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.details = json["data"]
                self.setDetail()
                if flag == 0 {
                    self.btnResetRating.isHidden = true
                    self.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    self.btnRates.forEach { $0.isUserInteractionEnabled = true }
                } else {
                    self.btnResetRating.isHidden = false
                    self.btnRates.forEach { $0.setBackgroundImage(nil, for: .normal) }
                    self.btnRates[flag - 1].setBackgroundImage(#imageLiteral(resourceName: "bg_rating_selected"), for: .normal)
                    self.btnRates.forEach { $0.isUserInteractionEnabled = false }
                }
            }
        }
        
    }
}

/*extension RatingSubViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            
            //            topConstraintConstant -= 5.0
            //            offsetDeledate.isScrollDown!(topConstraintConstant)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
    
}*/
