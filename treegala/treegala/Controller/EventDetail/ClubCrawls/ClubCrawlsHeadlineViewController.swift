//
//  ClubCrawlsHeadlineViewController.swift
//  treegala
//
//  Created by om on 8/13/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import DropDown

class ClubCrawlsHeadlineViewController: UIViewController {
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewBusStop: UIView!
    @IBOutlet weak var lblBusStop: UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    var oldContentOffset = CGPoint.zero
    var topConstraintConstant = CGFloat()
    var offsetDeledate: OffsetDelegate!
    
    var details: JSON?
    var isScrollDown = false
    
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setDetail()
        scrollView.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        if scrollView.contentSize.height < UIScreen.main.bounds.height {
            heightOfView.constant = UIScreen.main.bounds.height
        } else {
            heightOfView.constant = scrollView.contentSize.height
        }
        offsetDeledate.offsetValueChanged(heightOfView.constant)
    }
    
    func setDetail() {
        
        let item = details!["headlines"]
        
        lblWebSite.text = item["website"].stringValue.isEmpty ? keys.notMention() : item["website"].stringValue
        
        let charge = item["ticket_range"].stringValue
        
        lblCharge.text = charge.isEmpty ? keys.notMention() : charge
        
        viewPhone.isHidden = details!["description"]["phone_number"].stringValue.isEmpty
        btnPhone.setTitle(details!["description"]["phone_number"].stringValue, for: .normal)
        
        viewBusStop.isHidden = item["bus_stop_text"].stringValue.isEmpty
        lblBusStop.text = item["bus_stop_text"].stringValue
        
        viewFeature.isHidden = item["tags"].stringValue.isEmpty
        lblFeature.text = item["tags"].stringValue
        
        if let time = item["hedline_dates"].arrayValue.first {
            let dateFmt = DateFormatter()
            dateFmt.dateFormat = "yyyy-MM-dd"
            
            if let date = dateFmt.date(from: time["start_date"].stringValue) {
                dateFmt.dateFormat = "d MMM"
                lblDate.text = dateFmt.string(from: date)
            }
        }
        
        lblWebSite.onClick = {
            if let url = ("http://" + item["website"].stringValue).toURL() {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }

        btnViewMap.rx.tap.subscribe(onNext: {
            
            let mapDD = DropDown()
            mapDD.dataSource = ["Treegala map", "Google map"]
            
            self.configDropdown(dropdown: mapDD, sender: self.btnViewMap)
            
            mapDD.show()
            mapDD.selectionAction = { (index, text) in
                if index == 0 {
                    let vc = R.storyboard.eventDetail.singleEventMapViewController()!
                     vc.eventDetail = self.details!
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    var url = ""
                    
                    if let temp = "comgooglemaps://".toURL(), UIApplication.shared.canOpenURL(temp) {
                        url = "comgooglemaps://?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)&directionsmode=driving"
                    } else {
                        url = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(item["latitude"].doubleValue),\(item["longtitude"].doubleValue)"
                    }
                    
                    if let url = url.toURL() {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }

    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
}

extension ClubCrawlsHeadlineViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        print(scrollView.contentOffset.y)
        print(delta)
        
        print("Top Constraints => \(topConstraintConstant)" )
        print("Height Of View => \(EventDetailViewController.heightOfView)")
        
        let topConstraintRange = (0..<EventDetailViewController.heightOfView)
        
        print(topConstraintRange)
        // we compress top view
        if delta > 0 && topConstraintConstant >= topConstraintRange.lowerBound && scrollView.contentOffset.y > 0 {
            isScrollDown = false
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.upperBound ? topConstraintRange.upperBound : topConstraintConstant + delta
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            scrollView.contentOffset.y -= delta
        }
        
        // we expand top view
        if delta < 0 && topConstraintConstant <= topConstraintRange.upperBound && scrollView.contentOffset.y < 0 && topConstraintRange.lowerBound < topConstraintConstant {
            topConstraintConstant = topConstraintConstant + delta > topConstraintRange.lowerBound ? topConstraintConstant + delta : topConstraintRange.lowerBound
            isScrollDown = true
            print("Now Top Constraints => \(topConstraintConstant)" )
            
            offsetDeledate.offsetValueChanged(topConstraintConstant)
            
            
            scrollView.contentOffset.y -= delta
            print("Now ContentOffset:=",scrollView.contentOffset.y)
        }
        oldContentOffset.y = scrollView.contentOffset.y
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if isScrollDown {
            let topConstraintRange = (0..<EventDetailViewController.heightOfView)
            print("velocity:=",velocity)
            print("upperBound:==>",topConstraintRange.upperBound)
            let diffrence = topConstraintRange.upperBound - topConstraintConstant
            print("diffrence:==>",diffrence)
            
            //            topConstraintConstant -= 5.0
            //            offsetDeledate.isScrollDown!(topConstraintConstant)
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y:-10.0), animated: true)
            
        }
    }
}
