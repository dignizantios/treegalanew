//
//  TermsConditionVC.swift
//  treegala
//
//  Created by Jaydeep on 10/06/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit

class TermsConditionVC: UIViewController {
    
    //MARK:- Variablde Declaration
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblAboutUs: UILabel!
    
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Terms & Conditions"
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(logoClick))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
        
        let kURL = webUrls.baseUrl() + webUrls.termsConditionURL()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0"]
        
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.lblAboutUs.attributedText = json["data"]["terms_condition_content"].stringValue.html2AttributedString
            }
        }
    }
    
    @objc func logoClick() {
        NotificationCenter.default.post(name: NSNotification.Name.init("loadHome"), object: nil)
    }

}

class TermsConditionNavigationController:UINavigationController {
    
}
