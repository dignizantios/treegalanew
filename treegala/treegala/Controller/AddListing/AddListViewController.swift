//
//  AddListViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import DropDown
import RxSwift

class AddListViewController: UIViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtLink: UITextField!
    
    var categoryDD = DropDown()
    
    var selectedPlace: GMSPlace?
    var selectedCategory: Int?
    
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Listing"
        setBackButton()
        
        [txtLink, txtName, txtLocation, txtType].forEach {
            $0.layer.sublayerTransform = CATransform3DMakeTranslation(16, 0, 0)
        }
        
        [txtType, txtLocation].forEach { $0?.delegate = self }
        
        configDropdown(dropdown: categoryDD, sender: txtType)
        categoryDD.dataSource = ["Nightclubs","Club Crawls","Music Festival","Street Festival","Obstacle Events"]
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }

    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if txtName.text!.trimmed().isEmpty {
            showAlert(messages.pleaseEnterName())
            return
        }
        
        if selectedCategory == nil {
            showAlert(messages.selectListingType())
            return
        }
        
        if selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        }
        
        if !txtLink.text!.trimmed().isEmpty {
            let text = txtLink.text!.trimmed()
            if !text.isValidURL() {
                showAlert(messages.pleaseEnterValidWebsite())
                return
            }
        }
        
        let kURL = webUrls.baseUrl() + webUrls.addListingURL()

        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "listing_name" : txtName.text!.trimmed(),
                     "category_id" : "\(selectedCategory! + 1)",
                    "location" : "\(selectedPlace!.formattedAddress!)",
            "latitude" : "\(selectedPlace!.coordinate.latitude)",
            "longitude" : "\(selectedPlace!.coordinate.longitude)",
            "website_link" : txtLink.text!.trimmed(),
            "lang" : "0",
            "timezone" : TimeZone.current.identifier ]

        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            if json["flag"].stringValue == "1" {
                print(json)
                
                let vc = InfoPopupViewController()
                vc.dismissObserver.subscribe(onNext: {
                    print($0)
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name.init("redirectHome"), object: nil)
                }).disposed(by: self.disposeBag)
                vc.strTitle = messages.thankYou()
                vc.message = messages.thankYouSubmit()
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension AddListViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLocation {
            let vc = GMSAutocompleteViewController()
            UINavigationBar.appearance().tintColor = color.themePurple()
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            return false
        } else if textField == txtType {
            categoryDD.show()
            
            categoryDD.selectionAction = { (index, item) in
                self.txtType.text = item
                self.selectedCategory = index
            }
        }

        return false
    }
}

extension AddListViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white
        print("google map")
        print(place.placeID)
        print(place.coordinate)
        selectedPlace = place
        txtLocation.text = place.formattedAddress
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}

