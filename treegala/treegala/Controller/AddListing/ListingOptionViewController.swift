//
//  ListingOptionViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class ListingOptionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = setLogoTitleView()
        button.addTarget(self, action: #selector(logoClick), for: .touchUpInside)
        
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(logoClick))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
    @objc func logoClick() {
        NotificationCenter.default.post(name: NSNotification.Name.init("loadHome"), object: nil)
    }
    
    @IBAction func btnYesTapped(_ sender: UIButton) {
        //TODO: - Open web link
        
        guard let url = URL(string: "https://www.treegala.com/#/addlisting") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    @IBAction func btnNoTapped(_ sender: UIButton) {
        let vc = SB.addListing.addListViewController()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class OptionNavigationViewController: UINavigationController {
    
}
