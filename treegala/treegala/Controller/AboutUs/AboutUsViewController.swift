//
//  AboutUsViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

enum selectedController {
    case aboutsUs
    case terms
}

class AboutUsViewController: UIViewController {
    
    //MARK:- Variablde Declaration
    
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblAboutUs: UILabel!
    
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "About"
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(logoClick))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton

        let kURL = webUrls.baseUrl() + webUrls.aboutUsURL()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0"]
        
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.lblAboutUs.attributedText = json["data"]["about_us_content"].stringValue.html2AttributedString
            }
        }  
    }
    
    @objc func logoClick() {
        NotificationCenter.default.post(name: NSNotification.Name.init("loadHome"), object: nil)
    }
}

class AboutUsNavigationController: UINavigationController {
    
}
