//
//  ProfileViewController.swift
//  treegala
//
//  Created by om on 8/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import DropDown
import SwiftyJSON

class ProfileViewController: UIViewController {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var txtLocationPref: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var btnPasswordShow: UIButton!
    @IBOutlet weak var btnConPasswordShow: UIButton!
    
    @IBOutlet weak  var viewLocation: UIView!
    
    var disposeBag = DisposeBag()
    var locationDD = DropDown()
    
    var isFromSideMenu = false
    
    let item = ["Use Default Location", "Use Geolocated Location", "Use Picked Location"]
    
    var selectedLocationPref = 2 {
        didSet {
            self.viewLocation.isHidden = (selectedLocationPref != 1)
            if selectedLocationPref != 0
            {
                self.txtLocationPref.text = item[selectedLocationPref - 1]
            }            
        }
    }
    var selectedPlace: GMSPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedLocationPref = 2
        
        self.title = "Profile"
        setBackButton()
 
        setDetail()

        locationDD.dataSource = item
        
        configDropdown(dropdown: locationDD, sender: txtLocationPref)
        txtLocationPref.delegate = self
        
        txtLocation.delegate = self
        
        txtPassword.rx.text
            .map {
                text -> Bool in
                (text!.trimmed().isEmpty)
            }.bind(to: btnPasswordShow.rx.isHidden)
            .disposed(by: disposeBag)
        
        txtConfirmPassword.rx.text
            .map {
                text -> Bool in
                (text!.trimmed().isEmpty)
            }.bind(to: btnConPasswordShow.rx.isHidden)
            .disposed(by: disposeBag)
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 35.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.customCellConfiguration = { (index, item, cell) in
            cell.optionLabel.font = font.ubuntuCondensedRegular(size: 15)
        }
    }
    
    override func btnBackTapped() {
        if isFromSideMenu {
            appDelegate.setupSideMenu()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setDetail() {
        
        guard let data = UserDefaults.standard.value(forKey: isUserLogin() ? R.string.keys.userDetail() : R.string.keys.guestDetail()) as? Data else { return }
        
        let json = JSON(data)
        print(json)
        
        txtUserName.text = getUserDetail("username")
        txtEmail.text = getUserDetail("email")
        selectedLocationPref = json["is_location_flag"].intValue
        
        if let name = UserDefaults.standard.value(forKey: "locationName") as? String {
            txtLocation.text = name
        }
        
        if selectedLocationPref == 1 {
            startLoader()
            GMSPlacesClient.shared().lookUpPlaceID(json["place_id"].stringValue, callback: { (place, error) in
                self.dismissLoader()
                if let place = place {
                    self.selectedPlace = place
                    UserDefaults.standard.set(place.name, forKey: "locationName")
                    userPickedLocation = place
                    self.txtLocation.text =  place.name
                }
            })
        }
       
    }
    
    func validate() -> Bool {
        
        if txtUserName.text!.trimmed().isEmpty {
            showAlert(messages.enterUsername())
            return false
        } else if !txtUserName.text!.isAlphanumeric() {
            showAlert(messages.enterValidUsername())
            return false
        }
        
        if txtEmail.text!.trimmed().isEmpty {
            showAlert(messages.enterEmail())
            return false
        } else if !txtEmail.text!.trimmed().isValidEmail() {
            showAlert(messages.enterValidEmail())
            return false
        }
        
        if !txtPassword.text!.trimmed().isEmpty {
            
            if txtConfirmPassword.text!.trimmed().isEmpty {
                showAlert(messages.enterConfirmPassword())
                return false
            }
            
            if txtPassword.text != txtConfirmPassword.text {
                showAlert(messages.passwordNotMatch())
                return false
            }
        }
        
        if selectedLocationPref == 1 && selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return false
        }

        return true
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 0 {
            txtPassword.isSecureTextEntry = !sender.isSelected
            let str = txtPassword.text
            txtPassword.text = ""
            txtPassword.text = str
        } else {
            txtConfirmPassword.isSecureTextEntry = !sender.isSelected
            let str = txtConfirmPassword.text
            txtConfirmPassword.text = ""
            txtConfirmPassword.text = str
        }
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        if validate() {
            
            let kURL = webUrls.baseUrl() + webUrls.saveProfile()
            
            let latitude = selectedPlace?.coordinate.latitude == nil ? "" : "\(selectedPlace!.coordinate.latitude)"
            
            let longitude = selectedPlace?.coordinate.longitude == nil ? "" : "\(selectedPlace!.coordinate.longitude)"
            
            var param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "email" : txtEmail.text!.trimmed(),
                         "username" : txtUserName.text!.trimmed(),
                         "password" : txtPassword.text!.trimmed(),
                         "location_flag" : "\(selectedLocationPref)",
                         "latitude" : "",
                         "longtitude" : "",
                         "place_id" : "",
                         "timezone" : TimeZone.current.identifier,
                         "lang" : "0"]
            
            if selectedLocationPref == 1 {
                param["latitude"] = latitude
                param["longtitude"] = longitude
                param["place_id"] = "\(selectedPlace?.placeID ?? "")"
                UserDefaults.standard.set(selectedPlace?.name, forKey: "locationName")
            }

            startLoader()
            GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                self.dismissLoader()
                print(json)
                if json["flag"].stringValue == "1" {
                    
                    if self.selectedLocationPref != 1 {
                        UserDefaults.standard.removeObject(forKey: "locationName")
                    }
                    
                    guard let rawData = try? json["data"].rawData() else { return }
                
                    UserDefaults.standard.setValue(rawData, forKey: R.string.keys.userDetail())
                    
                    self.popupAlert(title: keys.appTitle(), message: json["msg"].stringValue, actionTitles: ["OK"], actions: [{ action in
                          if self.isFromSideMenu {
                            appDelegate.setupSideMenu()
                          } else {
                            self.navigationController?.popViewController(animated: true)
                          }
                        }])
                } else {
                    self.showAlert(json["msg"].stringValue)
                }
            })
            
        }
    }
}

extension ProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLocationPref {
            locationDD.show()
            locationDD.selectionAction = { (index, item) in
                self.txtLocationPref.text = item
                self.selectedLocationPref = index + 1
            }
            return false
        } else if textField == txtLocation {
            let vc = GMSAutocompleteViewController()
            UINavigationBar.appearance().tintColor = color.themePurple()
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            return false
        }
        
        return true
    }

}

extension ProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white
        self.selectedPlace = place
        txtLocation.text =  place.name
        userPickedLocation = place
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}

