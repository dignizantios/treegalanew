//
//  MyDashboardSubViewController.swift
//  treegala
//
//  Created by om on 8/28/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import MXScroll

class MyDashboardSubViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var index = 0
    var items: [JSON] = []
    var offset = 0
    var eventType: EventType = .nightClub
    var disposeBag = DisposeBag()
    
    var errorMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets.init(top: -30, left: 0, bottom: 0, right: 0)
        getItems()
        
        tableView.register(nib.myDashboardItemCell(), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
    }
    
    func getItems() {
        if index == 0 {
            getMyTopPicks()
        } else if index == 1 {
            getMyComments()
        } else if index == 2 {
            getMyListing()
        }
    }
    
    func getMyTopPicks() {
        
        let kURL = webUrls.baseUrl() + webUrls.myTopicList()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "offset" : "\(offset)"]
        
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            print(json)
            
            if json["flag"].stringValue == "1" {
                self.offset = json["next_offset"].intValue
                self.items = json["data"].arrayValue
            } else {
                self.errorMessage = json["msg"].stringValue
            }
            self.tableView.reloadData()
        }
    }
    
    func getMyListing() {
        let kURL = webUrls.baseUrl() + webUrls.myListing()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "offset" : "\(offset)"]
        
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.offset = json["next_offset"].intValue
                self.items = json["data"].arrayValue
            } else {
                self.errorMessage = json["msg"].stringValue
            }
            self.tableView.reloadData()
        }
    }
    
    func getMyComments() {
        let kURL = webUrls.baseUrl() + webUrls.myComments()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "offset" : "\(offset)"]
        
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            print(json)
            if json["flag"].stringValue == "1" {
                self.offset = json["next_offset"].intValue
                self.items = json["data"].arrayValue
            } else {
                self.errorMessage = json["msg"].stringValue
            }
            self.tableView.reloadData()
        }
    }
}

//MARK: - Pagiantion

extension MyDashboardSubViewController {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                if offset == -1 {
                    tableView.tableFooterView = nil
                    return
                } else {
                    getItems()
                }
            }
        }
    }
}

extension MyDashboardSubViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if items.isEmpty {
            setTableView(errorMessage, tableView: tableView)
            return 0
        } else {
            tableView.backgroundView = nil
            return items.count
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyDashboardItemTableViewCell
        
        cell.selectionStyle = .none
        cell.setDetail(items[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        if index == 0 {
            /*let vc = SB.eventDetail.eventDetailViewController()!
            vc.eventType = EventType.type(index: item["category_id"].intValue - 1)
            vc.favoriteObserver.subscribe(onNext: { (status) in
                if !status { //Event is unfavorite by user so need to remove from list
                    self.items.remove(at: indexPath.row)
                    if self.offset != -1 {
                        self.offset -= 1
                    }
                    
                    if self.items.isEmpty {
                        self.errorMessage = "You have not added any listing in your top picks yet."
                    }
                    
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
            EventDetailViewController.listID = item["listing_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)*/
            
            eventType = EventType.type(index: item["category_id"].intValue - 1)
            getEventDetailData(eventId: item["listing_id"].stringValue,tag: indexPath.row)
            
        } else if index == 1 { //My Comments
            if item["is_flag"].stringValue == "1" { //comment
                let vc = SB.eventDetail.commentListViewController()!
                vc.listID = item["listing_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            } else if item["is_flag"].stringValue == "2" { //reply
                let vc = SB.eventDetail.replyViewController()!
                vc.listID = item["listing_id"].stringValue
                vc.commentDetail = item
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if item["is_redirect"].stringValue == "1" {
               /* let vc = SB.eventDetail.eventDetailViewController()!
                vc.eventType = EventType.type(index: item["category_id"].intValue - 1)
                EventDetailViewController.listID = item["listing_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)*/
                eventType = EventType.type(index: item["category_id"].intValue - 1)
                getEventDetailData(eventId: item["listing_id"].stringValue ,                               tag: indexPath.row)
            }
        }
    }
    
    @objc func btnAddListingTapped(_ sender: UIButton) {
        let vc = SB.addListing.addListViewController()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension MyDashboardSubViewController
{
    func jumpToMixVC(json:JSON,tag:Int) {
        
        let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
        let headerScroll = eventStoryboard.instantiateViewController(withIdentifier: "NewEventDetail") as! NewEventDetail
        
        var hh:UIViewController!
        hh = headerScroll
        headerScroll.dictData = json
        
        if tag == 0
        {
            headerScroll.favoriteObserver.subscribe(onNext: { (status) in
                if !status { //Event is unfavorite by user so need to remove from list
                    self.items.remove(at: tag)
                    if self.offset != -1 {
                        self.offset -= 1
                    }
                    
                    if self.items.isEmpty {
                        self.errorMessage = "You have not added any listing in your top picks yet."
                    }
                    
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
        }
        
        let child1 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child1.dictEventDetailData = json
        
        let child2 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child2.dictEventDetailData = json
        
        let child3 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child3.dictEventDetailData = json
        child3.selectedIndex = 10
        
        let child4 = eventStoryboard.instantiateViewController(withIdentifier: "BottomEventDetailVC") as! BottomEventDetailVC
        child4.dictEventDetailData = json
        child4.selectedIndex = 11
        
        var headerTitles = ["HEADLINES", "EVENTS", "DESCRIPTION", "RATINGS"]
        switch eventType {
        case .nightClub:
            child1.selectedIndex = 0 // Headline
            
            child2.selectedIndex = 1 //event
            
            break
        case .clubCrawl:
            headerTitles = ["HEADLINES", "DETAILS", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 2 //clubcrawl
            
            child2.selectedIndex = 3 //musiclineup
            break
        case .music:
            headerTitles = ["HEADLINES", "LINEUP", "DESCRIPTION", "RATINGS"]
            child1.selectedIndex = 4 //musicheadline
            
            child2.selectedIndex = 5 //musiclineup
            break
        case .street:
            child1.selectedIndex = 6 //streethealine
            
            child2.selectedIndex = 7 //nightevent
            break
        case .obstacle:
            headerTitles = ["HEADLINES", "RACE DETAILS", "DESCRIPTION", "RATINGS"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        case .local:
//            headerTitles = ["HEADLINES", "DESCRIPTION", "RATINGS"]
            headerTitles = ["HEADLINES", "DESCRIPTION"]
            
            child1.selectedIndex = 8 //obstacle
            
            child2.selectedIndex = 9 //obstalce race
            break
        }
        let segment = MSSegmentControl(sectionTitles: headerTitles)
        setupSegment(segmentView: segment)
        
        let mx = MXViewController<MSSegmentControl>.init(headerViewController: hh, segmentControllers: [child1, child2,child3,child4], segmentView: segment)
        mx.headerViewOffsetHeight = 0
        mx.view.backgroundColor = color.themePurple()
       // mx.title = "Treegala"
        mx.navigationItem.titleView = setMXLogoTitle()
        mx.extendedLayoutIncludesOpaqueBars = true
        mx.shouldScrollToBottomAtFirstTime = false
        mx.setBackButton()
        navigationController?.pushViewController(mx, animated: true)
        
    }
    
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor = color.themePurple()
        segmentView.selectionIndicatorColor = .white
        segmentView.selectionIndicatorHeight = 3
        let Segment_statu_titleNormal = UIFont(name: "UbuntuCondensed-Regular", size: 13)!
        segmentView.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Segment_statu_titleNormal]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 2
        segmentView.selectionStyle = .fullWidth
    }
}

//MARK:- API

extension MyDashboardSubViewController
{
    func getEventDetailData(eventId:String,tag:Int) {
        
        let kURL = webUrls.baseUrl() + webUrls.getEventData()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0",
                     "listing_id" : eventId]
        
        print(param)
        
        startLoader()
        GeneralRequest.makeGetRequest(kURL, param: param, context: self) { (json) in
            self.dismissLoader()
            print(json)
            if json["flag"].stringValue == "1" {
                
                let eventStoryboard : UIStoryboard = UIStoryboard(name: "EventDetail", bundle: nil)
                let headerScroll: MXHomeViewController = eventStoryboard.instantiateViewController(withIdentifier: "MXHomeViewController") as! MXHomeViewController
                headerScroll.json = json["data"]
                headerScroll.eventType = self.eventType
                self.navigationController?.pushViewController(headerScroll, animated: true)
              //  self.jumpToMixVC(json: json["data"],tag:tag)
                //self.dictEventDetailData = json["data"]
                // self.tblEventDetail.reloadData()
            }
        }
    }
}

