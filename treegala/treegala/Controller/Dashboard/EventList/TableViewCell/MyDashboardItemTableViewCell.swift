//
//  MyDashboardItemTableViewCell.swift
//  treegala
//
//  Created by om on 8/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyDashboardItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var btnListType: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func setDetail(_ json: JSON) {
        imgEvent.sd_setImage(with: json["listing_image"].stringValue.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: nil)
        lblEventName.text = json["title"].stringValue
        lblLocation.text = json["location"].stringValue
        
        btnListType.isHidden = !json["is_redirect"].exists()
        
        if json["is_redirect"].stringValue == "0" {
            btnListType.setImage(#imageLiteral(resourceName: "ic_suggestion_new_white"), for: .normal)
            btnListType.setTitle("SUGGESTED", for: .normal)
        } else if json["is_redirect"].stringValue == "1" {
            btnListType.setImage(#imageLiteral(resourceName: "ic_owned_new_white"), for: .normal)
            btnListType.setTitle("OWNED", for: .normal)
        } else if json["is_redirect"].stringValue == "2" {
            btnListType.setImage(#imageLiteral(resourceName: "ic_claimed_new_white"), for: .normal)
            btnListType.setTitle("CLAIMED", for: .normal)
        }
    }
}
