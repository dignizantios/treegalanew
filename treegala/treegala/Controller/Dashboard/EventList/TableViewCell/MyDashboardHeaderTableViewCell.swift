//
//  DashboardHeaderTableViewCell.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

class MyDashboardHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblSectionName: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnAddListig: UIButton!
    
    @IBOutlet weak var btnSection: UIButton!
    
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

   
}
