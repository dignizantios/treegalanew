//
//  MyDashboardViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SwiftyJSON
import CarbonKit

class MyDashboardViewController: UIViewController {

    weak var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
    var isMyListing = false
    
    var headerTitles = ["MY TOP PICKS", "MY COMMENTS", "MY LISTING"]
    
    var isRedirectToProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Dashboard"
        print("Redirect \(isMyListing)")
        setBackButton()
        
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_my_profile_header_white"), style: .plain, target: self, action: #selector(btnEditProfile))
        self.navigationItem.rightBarButtonItem = barButton
        
        setupCarbonView()
        
        if isMyListing {
            carbonTabSwipeNavigation?.currentTabIndex = 2
        }
        
        if isRedirectToProfile {
            btnEditProfile()
        }
    }
    
    @objc func btnEditProfile() {
        let vc = SB.main.profileViewController()!
        vc.isFromSideMenu = isRedirectToProfile
        self.navigationController?.pushViewController(vc, animated: !isRedirectToProfile)
    }
    
    override func btnBackTapped() {
        appDelegate.setupSideMenu()
    }
    
    func setupCarbonView() {
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: headerTitles, delegate: self)
        carbonTabSwipeNavigation?.toolbarHeight.constant = 50.0
        
        carbonTabSwipeNavigation?.setIndicatorHeight(2.0)
        carbonTabSwipeNavigation?.setIndicatorColor(.white)
        
        carbonTabSwipeNavigation?.setNormalColor(.white, font: font.ubuntuCondensedRegular(size: 15.0)!)
        carbonTabSwipeNavigation?.setSelectedColor(.white, font: font.ubuntuCondensedRegular(size: 15.0)!)
        
        let width = UIScreen.main.bounds.width / CGFloat(headerTitles.count)
        
        for i in 0..<headerTitles.count {
            carbonTabSwipeNavigation?.carbonSegmentedControl?.setWidth(width, forSegmentAt: i)
        }
        
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.isScrollEnabled = false
        
        carbonTabSwipeNavigation?.toolbar.barTintColor = color.themePurple()
        carbonTabSwipeNavigation?.toolbar.isTranslucent = false
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        carbonTabSwipeNavigation?.view.layoutIfNeeded()
    }
    
}

extension MyDashboardViewController: CarbonTabSwipeNavigationDelegate {

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let vc = SB.main.myDashboardSubViewController()!
        vc.index = Int(index)
        return vc
    }
    
}

class DashboardNavigationController: UINavigationController {
    
}
