//
//  EventMusicLineUpCell.swift
//  treegala
//
//  Created by Jaydeep on 24/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventMusicLineUpCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgLineUp: UIImageView!
    @IBOutlet weak var btnDescription: UIButton!
    
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        lblDescription.text = ""
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
