//
//  EventHeadlineCell.swift
//  treegala
//
//  Created by Jaydeep on 20/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventHeadlineCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var viewTimeCharge: UIView!
    
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var btnTimeDropDown: UIButton!
    
    @IBOutlet weak var viewCharge: UIView!
    @IBOutlet weak var lblCharge: LabelButton!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewGenres: UIView!
    @IBOutlet weak var lblGenres: UILabel!
    
    @IBOutlet weak var viewDressCode: UIView!
    @IBOutlet weak var lblDressCode : UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    @IBOutlet weak var txtvwDressCode: UITextView!
    @IBOutlet weak var txtOtherResearch: UITextView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    //MARK:- Viewlife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
