//
//  EventClubCrawelCell.swift
//  treegala
//
//  Created by Jaydeep on 24/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventClubCrawelCell: UITableViewCell {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!   

    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewBusStop: UIView!
    @IBOutlet weak var lblBusStop: UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    @IBOutlet weak var vwTicketRange: UIView!
    
    
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnPhone.setTitle("", for: .normal)
        lblFeature.text = ""
        lblBusStop.text = ""
        lblCharge.text = ""
        lblDate.text = ""       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
