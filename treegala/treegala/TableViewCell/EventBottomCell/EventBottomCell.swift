//
//  EventBottomCell.swift
//  treegala
//
//  Created by Jaydeep on 31/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit


class EventBottomCell: UITableViewCell {
    
    //MARK:- Variable Declaration
    
   
    
    //MARK:- Outlet Zone
    
    
    @IBOutlet weak var outerView: UIView!
    
    //MARK:- ViewLife Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
