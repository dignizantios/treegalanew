//
//  EventObstacleCell.swift
//  treegala
//
//  Created by Jaydeep on 24/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventObstacleCell: UITableViewCell {
    
    //MARK:- Outlet zone
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblRegister: LabelButton!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewCourseDetail: UIView!
    @IBOutlet weak var lblCourseDetail: UILabel!
    
    @IBOutlet weak var viewFreeSwag: UIView!
    @IBOutlet weak var lblFreeSwag: UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    @IBOutlet weak var lblSep: UILabel!
    @IBOutlet weak var btnTicketRange: UIButton!
    
    @IBOutlet var viewFacts: UIView!
    @IBOutlet var lblOtherFact: UILabel!
    
    //MARK:- View Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblFeature.text = ""
        lblFreeSwag.text = ""
        lblCourseDetail.text = ""
        lblCharge.text = ""
        lblDate.text = ""
        btnPhone.setTitle("", for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
