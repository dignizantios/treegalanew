//
//  EventStreetHeaderCell.swift
//  treegala
//
//  Created by Jaydeep on 23/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventStreetHeaderCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var lblDates: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewFacts: UIView!
    @IBOutlet weak var lblFacts : UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    
    //MARK:- View LifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblFeature.text = ""
        lblFacts.text = ""
        lblCharge.text = ""
        lblDates.text = ""
        btnPhone.setTitle("", for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
