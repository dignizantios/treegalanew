//
//  EventCell.swift
//  treegala
//
//  Created by Jaydeep on 20/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventDesciptionCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
