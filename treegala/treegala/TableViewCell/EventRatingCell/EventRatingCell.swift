//
//  EventRatingCell.swift
//  treegala
//
//  Created by Jaydeep on 20/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class EventRatingCell: UITableViewCell {
    
    var disposeBag = DisposeBag()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnResetRating: UIButton!
    
    @IBOutlet weak var lblFunVote: UILabel!
    @IBOutlet weak var lblSatisFactoryVote: UILabel!
    @IBOutlet weak var lblBoringVote: UILabel!
    @IBOutlet weak var lblDislikedVote: UILabel!
    
    @IBOutlet weak var lblTotalRate: UILabel!
    
    @IBOutlet var btnRates: [UIButton]!
    @IBOutlet weak var btnViewComments: UIButton!
    
    //MARK:- View Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        disposeBag = DisposeBag()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
