//
//  EventNightCell.swift
//  treegala
//
//  Created by Jaydeep on 23/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class EventNightCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var btnEventCalendar: UIView!
    @IBOutlet weak var btnEventCalendarBottom: xUIButton!
    @IBOutlet weak var btnEventCalendarUppar: xUIButton!
    @IBOutlet weak var heightOfTableview: NSLayoutConstraint!
    
    //MARK:- Variable Declaration
    
    var calendarHeight: CGFloat = 0.0
    var tableHeight: CGFloat = 0.0
    
    var disposeBag = DisposeBag()
    var handlerForTableHeight:(CGFloat) -> Void = {_ in}
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        // Initialization code
    }

    
    deinit {
        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
           
            print("contentSize:= \(tableView.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            print("contentSize:= \(tableView.contentSize.height)")
            heightOfTableview.constant = tableView.contentSize.height
            if tableView.contentSize.height == 0
            {
                heightOfTableview.constant = 100
            }
            handlerForTableHeight(heightOfTableview.constant)
//            self.contentView.layoutSubviews()
//            self.contentView.layoutIfNeeded()
        }
    }
    
}
