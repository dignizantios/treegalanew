//
//  EventFirstCell.swift
//  treegala
//
//  Created by Jaydeep on 19/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventFirstCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgSlider: KIImagePager!
    @IBOutlet weak var lblEventName: UILabel!
    
    @IBOutlet weak var widthOfClaim: NSLayoutConstraint!
    @IBOutlet weak var btnClaim: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var lblEventLocation: UILabel!
    
    @IBOutlet weak var topOfView: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet var headerButtons: [UIButton]!
    @IBOutlet var lblHeader: [UILabel]!
    
    @IBOutlet weak var carbonView: UIView!
    @IBOutlet weak var heightOfCarbon: NSLayoutConstraint!
    
    //MARK:-ViewLifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
