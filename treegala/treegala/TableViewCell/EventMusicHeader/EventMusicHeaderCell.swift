//
//  EventMusicHeaderCell.swift
//  treegala
//
//  Created by Jaydeep on 23/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventMusicHeaderCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnViewMap: UIButton!
    @IBOutlet weak var lblWebSite: LabelButton!
    
    @IBOutlet weak var lblDates: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    
    @IBOutlet weak var lblCharge: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnPhone: UIButton!
    
    @IBOutlet weak var viewGenres: UIView!
    @IBOutlet weak var lblGenres: UILabel!
    
    @IBOutlet weak var viewHeadline: UIView!
    @IBOutlet weak var lblHeadline : UILabel!
    
    @IBOutlet weak var viewFeature: UIView!
    @IBOutlet weak var lblFeature : UILabel!
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblFeature.text = ""
        lblHeadline.text = ""
        lblGenres.text = ""
        lblCharge.text = ""
        lblDates.text = ""
        btnPhone.setTitle("", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
