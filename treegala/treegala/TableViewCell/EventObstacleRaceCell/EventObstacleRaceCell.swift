//
//  EventObstacleRaceCell.swift
//  treegala
//
//  Created by Jaydeep on 24/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventObstacleRaceCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewDifficulty: UIView!
    @IBOutlet var btnDifficulties: [UIButton]!
    
    @IBOutlet weak var viewPrizes: UIView!
    @IBOutlet weak var lblPrize: UILabel!
    
    @IBOutlet weak var viewOtherDetail: UIView!
    @IBOutlet weak var lblOtherDetails: UILabel!
    @IBOutlet weak var lblDisclosed: UILabel!
    
    //MARK:- Viewlife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOtherDetails.text = ""
        lblPrize.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
