//
//  UIView+Extension.swift
//  Extensions
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

extension UIView {
    
    func viewNextPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
    
    func takeSnap() -> UIImage {
        if UIScreen.main.responds(to: #selector(getter: UIScreen.main.scale)) {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        }
        else {
            UIGraphicsBeginImageContext(self.bounds.size)
        }
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = true
    }
    
    func addBorderWithCornerRadius(cornerRadius: CGFloat = 4, borderWidth: CGFloat = 0.5, borderColor: UIColor = UIColor.appThemeLightGrayColor, shouldAddBorder: Bool = true) {
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        
        if shouldAddBorder {
            self.layer.borderWidth = borderWidth
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    func addCornerRadiusOnlyRightSide() {
        
        self.clipsToBounds = true
        var maskPath: UIBezierPath
        maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: self.frame.width / 2, height: self.frame.height / 2))
        let maskLayer = CAShapeLayer.init()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
        
    }
    func addCornerRadiusOnlyLeftSide() {
        
        self.clipsToBounds = true
        var maskPath: UIBezierPath
        maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: self.frame.width / 2, height: self.frame.height / 2))
        let maskLayer = CAShapeLayer.init()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
        
    }
    
    func addDashedBorder() {
        let viewDashedBorder = CAShapeLayer()
        viewDashedBorder.strokeColor = UIColor(red: 206.0 / 255.0, green: 31.0 / 255.0, blue: 49.0 / 255.0, alpha: 1.0).cgColor
        viewDashedBorder.lineDashPattern = [4, 4]
        viewDashedBorder.frame = self.bounds
        viewDashedBorder.cornerRadius = 4.0
        viewDashedBorder.fillColor = nil
        viewDashedBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(viewDashedBorder)
        
        self.clipsToBounds = true
    }
    func setUpViewRadius(radius : Float)
    {
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.masksToBounds = true
        
    }
    func viewOut()
    {
        let height = self.frame.size.height
        let bottom = CGAffineTransform(translationX: 0, y: -height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = bottom
            
        }, completion:  { (finished: Bool) in
            self.alpha = 0
           
        })
    }
    func viewIn()
    {
        let height = self.bounds.height
        let bottom = CGAffineTransform(translationX: 0, y: height)
        self.transform = bottom
        
        self.alpha = 1
        let top = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = top
        }, completion: nil)
    }
}
