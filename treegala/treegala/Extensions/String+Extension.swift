//
//  String+Extension.swift
//  Extensions
//
//  Created by pc.
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

extension String {
    func countLabelLines(width: CGFloat, font:UIFont) -> Int {
        // Call self.layoutIfNeeded() if your view uses auto layout
        let myText = self as NSString
        //        UIScreen.main.bounds.width - 95
        let rect = CGSize(width:width , height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return Int(ceil(CGFloat(labelSize.height) / font.lineHeight))
    }
    
    func StringToDate(Formatter : String) -> Date
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedDate = dateformatter.date(from: self)
        return convertedDate!
    }
    
    func StringToConvertedStringDate(strDateFormat : String,strRequiredFormat:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = strDateFormat
        guard let convertedDate = dateformatter.date(from: self) else {
            return ""
        }
        dateformatter.dateFormat = strRequiredFormat
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    
    func convertDate(format from: String, toFormat: String) -> String {
        let dateFmt = DateFormatter()
        dateFmt.dateFormat = from
        
        guard let date = dateFmt.date(from: self) else {
            return ""
        }
        
        dateFmt.dateFormat = toFormat
        let dateString = dateFmt.string(from: date)
        return dateString
    }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func isValidEmailAddress() -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    var youtubeID: String?
    {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
}

extension String {
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    
}
