//
//  UIButton + RxExtension.swift
//  treegala
//
//  Created by om on 8/21/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base : UIButton {
    public var valid : Binder<Bool> {
        return Binder(self.base) { button, valid in
            button.isEnabled = valid
            button.alpha = valid ? 1 : 0.5
        }
    }
}
