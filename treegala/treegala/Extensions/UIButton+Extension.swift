//
//  UIButton+Extension.swift
//  Extensions
//
//  Created by pc.
//  Copyright © 2018 dignizant. All rights reserved.
//

import Foundation

import UIKit
extension UIButton
{
    func setUpGlow()
    {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = true
    }
    func startBlink() {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
    func setUpRadius(radius : Float)
    {
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.masksToBounds = true

    }
}
