//
//  UIViewController+Extension.swift
//  Extensions
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
    
    func setSideMenu() {
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menubar_white"), style: .plain, target: self, action: #selector(sideMenuOpen))
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
    func setBackButton() {
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(btnBackTapped))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
    func setGradientBar() {
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "bg_header").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .stretch), for: .any, barMetrics: .default)
    }
    
    func setLogoTitleView() -> UIButton {
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        button.setImage(#imageLiteral(resourceName: "ic_logo_header"), for: .normal)
        //button.addTarget(self, action: #selector(self.clickOnButton), for: .touchUpInside)
        navigationItem.titleView = button
        
        //navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "ic_logo_header"))
        return button
    }
    
    @objc func sideMenuOpen() {
        self.slideMenuController()?.toggleLeft()
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ message: String) {
        popupAlert(title: keys.appTitle(), message: message, actionTitles: ["OK"], actions: [ { _ in }])
    }
    
    func startLoader() {
        startAnimating(CGSize(width: 50, height: 50), type: NVActivityIndicatorType(rawValue: 32), color: color.grayAccent())
    }
    
    func dismissLoader() {
        stopAnimating()
    }
    
    func setTableView(_ message: String, color: UIColor = .white, tableView: UITableView) {
        let noDataLabel = UILabel()
        noDataLabel.textAlignment = .center
        noDataLabel.text = message
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = color
        noDataLabel.font = font.ubuntuCondensedRegular(size: 20.0)
        noDataLabel.tag = 1111
        noDataLabel.center = tableView.center
        tableView.backgroundView = noDataLabel
    }
    
    func presentLogin() {
        self.popupAlert(title: keys.appTitle(), message: messages.noLoginPopup(), actionTitles: ["OK", "Cancel"], actions: [{ action1 in
            isLoginPresent = true
            let vc = SB.signin.signinNavigationController()!
            self.present(vc, animated: true, completion: nil)
            }, { action2 in
                
            }])
    }
    
    func setMXLogoTitle() -> UIImageView {
        let image =  UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 68, height: 28)
        image.image = UIImage.init(named: "ic_logo_header")
        return image
    }
    
    //MARK: Indicator in textfield
    func showActivityIndicatory(btn: UIButton) {
        
        loadingView.frame = CGRect(x: btn.frame.width+50, y: 0, width: btn.frame.height-5, height: btn.frame.height-5)
        loadingView.backgroundColor = UIColor.white
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        loadingView.isHidden = false
        
        actInd = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: btn.frame.height, height: btn.frame.height)
        actInd.activityIndicatorViewStyle = UIActivityIndicatorView.Style.gray
        actInd.color = UIColor.lightGray
        actInd.center = CGPoint(x: (loadingView.frame.size.height/2)+3, y: (loadingView.frame.size.height/2)-3)
        loadingView.addSubview(actInd)
        btn.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func stopActivityIndicatory(btn: UIButton) {
        loadingView.isHidden = true
        actInd.stopAnimating()
    }
    
}

