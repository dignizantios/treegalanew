//
//  Date + Extension.swift
//  Extensions
//
//  Created by pc.
//  Copyright © 2017 Dignizant. All rights reserved.
//

import Foundation

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        print("start - ",start)
        print("end - ",end)
        return end - start
    }
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func dateByAddingDays(inDays:NSInteger)->Date{
        let date = self
        return Calendar.current.date(byAdding: .day, value: inDays, to: date)!
    }
    
    func dateToString(Formatter : String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedString = dateformatter.string(from: self)
        return convertedString
    }
    
    func getDayOfWeek() -> Int {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: self)
        return weekDay - 1
    }
}

func generateDatesArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date]
{
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    while startDate <= endDate {
        datesArray.append(startDate)
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
    }
    return datesArray
}

func generateStringArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[String]
{
    print("startDate - ",startDate)
    print("endDate - ",endDate)

    var strDatesArray: [String] =  [String]()
    
    let dateStart = startDate
    let dateEnd = endDate
    
    let calendar = Calendar.current
    let formatter = "d MMM,yyyy"
    
    let strStartDate = dateStart.dateToString(Formatter: formatter)
    var dateStartConverted = strStartDate.StringToDate(Formatter: formatter)
    
    let strEndDate = dateEnd.dateToString(Formatter: formatter)
    let dateEndConverted = strEndDate.StringToDate(Formatter: formatter)

    while dateStartConverted <= dateEndConverted
    {
        let strStartConvertedNew = dateStartConverted.dateToString(Formatter: formatter)
        let dateStartConvertedNew = strStartConvertedNew.StringToDate(Formatter: formatter)
        strDatesArray.append(strStartConvertedNew)
        
        dateStartConverted = calendar.date(byAdding: .day, value: 1, to: dateStartConvertedNew)!
    }
    return strDatesArray
}

func DateToString(Formatter:String,date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Formatter
    //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
    let FinalDate:String = dateFormatter.string(from: date)
    return FinalDate
}
