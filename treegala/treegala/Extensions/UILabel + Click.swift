//
//  UILabel + Click.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation

@IBDesignable class LabelButton: UILabel {
    
    var onClick: () -> Void = {}
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onClick()
    }
}

@IBDesignable class ImageButton: UIImageView {
    
    var onClick: () -> Void = {}
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onClick()
    }
    
}
