//
//  UIFont+Extension.swift
//  Extensions
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case Regular = "HelveticaNeue-Medium"
    
}

extension UIFont
{
    
    

}
func themeFont(size : Float,font : themeFonts) -> UIFont
{
    return UIFont(name: font.rawValue, size: CGFloat(size))!
}
