//
//  UITableViewCell+Extension.swift
//  Extensions
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func cellPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}

