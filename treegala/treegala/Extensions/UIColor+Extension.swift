//
//  UIColor+Extension.swift
//  Extensions
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var appThemeLightGrayColor: UIColor { return UIColor.hexStringToUIColor(hex: "#9A9A9A") }
    static var appThemeBlueColor: UIColor { return UIColor.hexStringToUIColor(hex: "#24A6FF") }
    static var appThemeGrayColor: UIColor { return UIColor.hexStringToUIColor(hex: "#E9EBEE") }
    static var appThemeDarkColor: UIColor { return UIColor.hexStringToUIColor(hex: "#343F4B") }
    static var appThemeGreenColor: UIColor { return UIColor.hexStringToUIColor(hex: "#3B831F") }
    static var appThemeOrangeColor: UIColor { return UIColor.hexStringToUIColor(hex: "#FDBA5B") }
    static var appThemeDarkOrangeColor: UIColor { return UIColor.hexStringToUIColor(hex: "#F69646") }
    static var appThemeDarkBlueColor: UIColor { return UIColor.hexStringToUIColor(hex: "#052A3D") }
    static var appThemeDarkPinkColor: UIColor { return UIColor.hexStringToUIColor(hex: "#FF4081") }
    static var appThemeLightPinkColor: UIColor { return UIColor.hexStringToUIColor(hex: "#86FF4081") }
    static var appThemeGoldenColor: UIColor { return UIColor.hexStringToUIColor(hex: "#FFB134")}

    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
