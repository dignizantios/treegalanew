//
//  LocalEventsVC.swift
//  treegala
//
//  Created by Khushbu on 07/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import DropDown
import CoreLocation
import Alamofire
import SwiftyJSON

class LocalEventsVC: UIViewController {

    //MARK: Controller
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnEnterCity: xUIButton!
    @IBOutlet var btnSelectDateRange: UIButton!
    @IBOutlet var btnSwitchFamilyFriendli: UIButton!
    @IBOutlet var btnSwitchFree: UIButton!
    @IBOutlet var btnSearch: xUIButton!
    
    //MARK: Variables
    var selectedPlace: GMSPlace?
    let locationManager = CLLocationManager()
    var filterData = FilterData()
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    var searchBtn = UIBarButtonItem()
    var isSearchBarHidden = true {
        didSet {
            self.view.endEditing(isSearchBarHidden)
            navigationItem.titleView = isSearchBarHidden ? nil : searchBar
            navigationItem.titleView?.tintColor = UIColor.black
            searchBtn.image = isSearchBarHidden ? #imageLiteral(resourceName: "ic_search_white") : #imageLiteral(resourceName: "ic_cancel_large")
        }
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
}

//MARK: SetUp
extension LocalEventsVC {
    
    func setUpUI() {
        
        setSideMenu()
        setNavigationRightButton()
        
        isSearchBarHidden = true
        //        txtLocation.delegate = self
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        setLocationData()
        
        if let textField = self.searchBar.subviews.first?.subviews.flatMap({ $0 as? UITextField }).first {
            textField.subviews.first?.isHidden = true
            textField.textColor = .black
            textField.layer.backgroundColor = UIColor.white.cgColor
            textField.layer.cornerRadius = 5
            textField.layer.masksToBounds = true
        }
    }
    
    func setNavigationRightButton() {
        searchBtn = UIBarButtonItem(image: UIImage(named: ""), style: .plain, target: self, action: #selector(btnSearchTapped))
        self.navigationItem.rightBarButtonItem = searchBtn
    }
    
    @objc func btnSearchTapped() {
        searchBar.text = ""
        isSearchBarHidden = !isSearchBarHidden
    }
    
//    func setCategoryData(_ flag: Bool = true) {
//        let item4 = Category(id: "3", name: "Endurance Events", isSelected: flag, subCategories: [SubCategories(id: "6", categoryImage:#imageLiteral(resourceName: "ic_street_festivals_home_popup"), name: "Local Events", isSelected: flag)])
//        globalCategories = [item4]
//    }
    
    func setCategoryData() {
        let item1 = Category(id: "", name: "All", isSelected: false, subCategories: [])
        let item2 = Category(id: "1", name: "Nightlife", isSelected: false, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_big_nightclubs_select_category"), name: "Nightclubs", isSelected: false)])
        //        let item2 = Category(id: "1", name: "Nightlife", isSelected: flag, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_nighclub_home_popup"), name: "Nightclubs", isSelected: flag),
        //            SubCategories(id: "2", categoryImage: #imageLiteral(resourceName: "ic_club_crawls_home_popup"), name: "Club Crawls", isSelected: flag)])
        let item3 = Category(id: "2", name: "Festival", isSelected: false, subCategories: [SubCategories(id: "3", categoryImage: #imageLiteral(resourceName: "ic_gitar_mini_select_category"), name: "Music Festival", isSelected: false),
                                                                                          SubCategories(id: "4", categoryImage: #imageLiteral(resourceName: "ic_street_festivals_home_popup"), name: "Street Festival", isSelected: false)])
        let item4 = Category(id: "3", name: "Endurance Events", isSelected: false, subCategories: [SubCategories(id: "5", categoryImage:#imageLiteral(resourceName: "ic_big_endurance_select_category"), name: "Endurance Events", isSelected: false), SubCategories(id: "6", categoryImage:#imageLiteral(resourceName: "ic_big_vocal_events_select_category"), name: "Local Events", isSelected: true)])
        globalCategories = [item1, item2, item3, item4]
    }
}


//MARK: Button Action
extension LocalEventsVC {
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEnterCityAction(_ sender: Any) {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSelectDateRangeAction(_ sender: Any) {
        let vc = R.storyboard.newUpdates.selectDateRangeVC()
        vc?.modalTransitionStyle = .coverVertical
        vc?.modalPresentationStyle = .overCurrentContext
        
        vc?.selectDateHandlor = { first, last in
            self.filterData.toDateStr = last
            self.filterData.fromDateStr = first
            self.btnSelectDateRange.setTitle("  "+first+" / "+last, for: .normal)
        }
        
        self.present(vc!, animated: false, completion: nil)

    }
    
    @IBAction func btnSwitchFamilyFriendlyAction(_ sender: Any) {
        
        if btnSwitchFamilyFriendli.isSelected == true {
            btnSwitchFamilyFriendli.isSelected = false
            self.filterData.isFamilyFriendly = "0"
        }
        else {
            btnSwitchFamilyFriendli.isSelected = true
            self.filterData.isFamilyFriendly = "1"
        }
    }
    
    @IBAction func btnSwitchFreeAction(_ sender: Any) {
        if btnSwitchFree.isSelected == true {
            btnSwitchFree.isSelected = false
            self.filterData.free = "0"
        }
        else {
            btnSwitchFree.isSelected = true
            self.filterData.free = "1"
        }
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        validateLocation()
        if Rechability.isConnectedToNetwork() {
            self.validateLocation()
            let vc = SB.home.eventListViewController()!
            self.filterData.mainCategory = ""
            vc.categoryId = .localEvents
            filterData.selectedPlace = self.selectedPlace
            vc.filterData = self.filterData
            // self.filterData = FilterData()
            self.setLocationData()
            self.setCategoryData()
            setGlobalFilterCheckMark = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            self.showAlert(keys.noInternet())
            return
        }
    }

}

//MARK:
extension LocalEventsVC {
    
}

//MARK: TextField/SearchBar Delegate
extension LocalEventsVC: UITextFieldDelegate, UISearchBarDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchBarHidden = true
        //        self.txtLocation.text = searchBar.text ?? ""
        let vc = SB.home.eventListViewController()!
        filterData.searchText = searchBar.text ?? ""
        self.filterData.latitude = userLocation?.coordinate.latitude
        self.filterData.longitude = userLocation?.coordinate.longitude
        vc.filterData = self.filterData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension LocalEventsVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white
        
        self.selectedPlace = place
        
        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "United States" }) {
            filterData.distanceType = "1"
        } else {
            filterData.distanceType = "0"
        }
        filterData.selectedPlace = place
        self.btnEnterCity.setTitle("  "+"\(place.name ?? "")", for: .normal)
        //        txtLocation.text =  place.name
        filterData.locationCity = place.name ?? ""
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}


//MARK:
extension LocalEventsVC {
    
}


//MARK: Location setUp
extension LocalEventsVC {
    
    func validateLocation() {
        
        print("Location Stategey")
        
        if getUserDetail("is_location_flag") == "3" && self.selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        } else if getUserDetail("is_location_flag") == "2" {
            self.checkLocation()
            //            showAlert(messages.pleaseEnterLocation())
            //            return
        } else if getUserDetail("is_location_flag") == "1" && self.selectedPlace == nil {
            //            showAlert(messages.pleaseEnterLocation())
            //            return
        }
        
        if userLocation != nil {
            self.filterData.latitude = userLocation?.coordinate.latitude
            self.filterData.longitude = userLocation?.coordinate.longitude
        }
        
    }
    
    func checkLocation() {
        if self.selectedPlace == nil && userLocation == nil {
            self.popupAlert(title: "Treegala", message: "Please Turn on the location or select the location", actionTitles: ["Pick Place", "Setting"], actions: [ { action1 in
                //                self.txtLocation.becomeFirstResponder()
                
                let vc = GMSAutocompleteViewController()
                UINavigationBar.appearance().tintColor = color.themePurple()
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                
                }, { action2 in
                    guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(urlGeneral, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(urlGeneral)
                    }
                }])
            return
        }
    }
    
    /*func validateLocation() {
        
        print("Location Stategey")
        
//        if getUserDetail("is_location_flag") == "3" && self.selectedPlace == nil {
//            showAlert(messages.pleaseEnterLocation())
//            return
//        } else if getUserDetail("is_location_flag") == "2" && self.selectedPlace == nil {
//            showAlert(messages.pleaseEnterLocation())
//            return
//        } else if getUserDetail("is_location_flag") == "1" && self.selectedPlace == nil {
//            showAlert(messages.pleaseEnterLocation())
//            return
//        }
        
        if userLocation != nil {
            self.filterData.latitude = userLocation?.coordinate.latitude
            self.filterData.longitude = userLocation?.coordinate.longitude
        }
        
        if self.selectedPlace == nil && userLocation == nil {
            self.popupAlert(title: "Treegala", message: "Please Turn on the location or select the location", actionTitles: ["Pick Place", "Setting"], actions: [ { action1 in
//                self.txtLocation.becomeFirstResponder()
                
                let vc = GMSAutocompleteViewController()
                UINavigationBar.appearance().tintColor = color.themePurple()
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                }, { action2 in
                    guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(urlGeneral, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(urlGeneral)
                    }
                }])
            return
        }
    }*/
    
    func setLocationData() {
        if getUserDetail("is_location_flag") == "1" {
            
            //            print(UserDefaults.standard.value(forKey: "locationName") as? String)
            //            if let name = (UserDefaults.standard.value(forKey: "locationName") as? String) {
            //                txtLocation.text =  name
            //            }
            
            guard let data = UserDefaults.standard.value(forKey: isUserLogin() ? R.string.keys.userDetail() : R.string.keys.guestDetail()) as? Data else { return }
            
            let json = JSON(data)
            
            if userPickedLocation == nil {
                startLoader()
                GMSPlacesClient.shared().lookUpPlaceID(json["place_id"].stringValue, callback: { (place, error) in
                    self.dismissLoader()
                    if let place = place {
                        if self.selectedPlace == nil {
                            self.selectedPlace = place
                            self.filterData.selectedPlace = place
                            //                            self.txtLocation.text =  place.name
                            self.btnEnterCity.setTitle("  "+place.name!, for: .normal)
                        }
                        
                        UserDefaults.standard.set(place.name, forKey: "locationName")
                        userPickedLocation = place
                        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                            self.filterData.distanceType = "1"
                        } else {
                            self.filterData.distanceType = "0"
                        }
                    }
                })
            } else {
                let place = userPickedLocation!
                
                if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                    self.filterData.distanceType = "1"
                } else {
                    self.filterData.distanceType = "0"
                }
                
                if self.selectedPlace == nil {
                    self.selectedPlace = place
                    self.filterData.selectedPlace = place
                    //                    self.txtLocation.text =  place.name
                    filterData.locationCity = place.name ?? ""
                    self.btnEnterCity.setTitle("  "+place.name!, for: .normal)
                }
                //                self.filterData.selectedPlace = place
                //                self.selectedPlace = place
                //                print(userPickedLocation!)
                //                self.txtLocation.text = place.name
            }
        } else if getUserDetail("is_location_flag") == "3" {
            selectedPlace = nil
            self.btnEnterCity.setTitle("  Enter Location", for: .normal)
            //            txtLocation.text = "Enter Location"
        }
    }
}


//MARK: Location Delegate
extension LocalEventsVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .notDetermined || status == .denied {
            locationManager.requestWhenInUseAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("location manager")
        if let location = locations.first {
            userLocation = location
            if getUserDetail("is_location_flag") == "2" || !isUserLogin() {
                getPlaceId(of: location)
            }
            locationManager.stopUpdatingLocation()
        }
    }
    
    func getPlaceId(of location: CLLocation) {
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=\(webUrls.googleAPIKey())"
        startLoader()
        request(url).responseSwiftyJSON { (response) in
            self.dismissLoader()
            if let json = response.result.value {
                print(json)
                if let item = json["results"].arrayValue.first {
                    print("Current Place Id")
                    for i in item["address_components"].arrayValue {
                        if i["types"].arrayValue.contains("country") {
                            if i["long_name"].stringValue == "United Status" {
                                self.filterData.distanceType = "1"
                                print("United State Country")
                            } else {
                                self.filterData.distanceType = "0"
                                print("Other Country")
                            }
                        }
                        
                        if i["types"].arrayValue.contains("locality") {
                            self.filterData.locationCity = i["long_name"].stringValue
                            self.btnEnterCity.setTitle("  "+i["long_name"].stringValue, for: .normal)
                            //                            self.txtLocation.text = i["long_name"].stringValue
                        }
                    }
                    self.filterData.currentPlaceID = item["place_id"].stringValue
                    self.getPlaceFromID(item["place_id"].stringValue)
                }
            }
        }
    }
    
    func getPlaceFromID(_ placeId: String) {
        GMSPlacesClient.shared().lookUpPlaceID(placeId, callback: { (place, error) in
            if let place = place {
                print(place.name)
                self.selectedPlace = place
                self.filterData.selectedPlace = place
            }
        })
    }
}
