//
//  NightlifeViewController.swift
//  treegala
//
//  Created by Khushbu on 06/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import DropDown
import CoreLocation
import Alamofire
import SwiftyJSON

class NightlifeViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnEnterCity: xUIButton!
    @IBOutlet var btnSelectGenre: UIButton!
    @IBOutlet var btnSwitchLGBTQ: UIButton!
    @IBOutlet var btnSearch: UIButton!
    
    //MARK: Varialbes
    var selectedPlace: GMSPlace?
    var filterData = FilterData()
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    var searchBtn = UIBarButtonItem()
    var disposeBag = DisposeBag()
    let locationManager = CLLocationManager()
    var eventTypeStr = ""
    
    
    var isSearchBarHidden = true {
        didSet {
            self.view.endEditing(isSearchBarHidden)
            navigationItem.titleView = isSearchBarHidden ? nil : searchBar
            navigationItem.titleView?.tintColor = UIColor.black
            searchBtn.image = isSearchBarHidden ? #imageLiteral(resourceName: "ic_search_white") : #imageLiteral(resourceName: "ic_cancel_large")
        }
    }
    
    var genresItems: [JSON] = [] {
        didSet {
//            controls.genresDD.dataSource = genresItems.map { $0["genres_name"].stringValue }
//            setFilterData(forType: 2)
        }
    }
    
    var selectedGenreType: [JSON] = [] {
        didSet {
//            controls.genresCollection.reloadData()
        }
    }
    
    //MARK Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        getOptionList()
        self.setUpUI()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
}

//MARK: setUp
extension NightlifeViewController {
    
    func setUpUI() {
        
        self.setSideMenu()
        setNavigationRightButton()
        
        isSearchBarHidden = true
        //        txtLocation.delegate = self
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        setLocationData()
        
        if let textField = self.searchBar.subviews.first?.subviews.flatMap({ $0 as? UITextField }).first {
            textField.subviews.first?.isHidden = true
            textField.textColor = .black
            textField.layer.backgroundColor = UIColor.white.cgColor
            textField.layer.cornerRadius = 5
            textField.layer.masksToBounds = true
        }
    }
    
    func setNavigationRightButton() {
        searchBtn = UIBarButtonItem(image: UIImage(named: ""), style: .plain, target: self, action: #selector(btnSearchTapped))
        self.navigationItem.rightBarButtonItem = searchBtn
    }
    
    @objc func btnSearchTapped() {
        searchBar.text = ""
        isSearchBarHidden = !isSearchBarHidden
    }
}


//MARK: Button action
extension NightlifeViewController {

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEnterCityAction(_ sender: Any) {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSelectGenreAction(_ sender: Any) {
        showOptionSelection(type: 1)
    }
    
    @IBAction func btnSwitchLGBTQAction(_ sender: Any) {
        
        if self.btnSwitchLGBTQ.isSelected == true {
            self.btnSwitchLGBTQ.isSelected = false
            self.filterData.lgbtqType = "0"
        }
        else {
            self.btnSwitchLGBTQ.isSelected = true
            self.filterData.lgbtqType = "1"
        }
    }
    
    @IBAction func btnSearchAllAction(_ sender: Any) {
        validateLocation()
//        btnSearch.rx.tap.subscribe(onNext: {
        
        if Rechability.isConnectedToNetwork() {
            self.validateLocation()
            let vc = SB.home.eventListViewController()!
            self.filterData.mainCategory = ""
            vc.categoryId = .nightclub
            vc.filterData = self.filterData
            vc.selectedGenreType = self.selectedGenreType
            setGlobalFilterCheckMark = true
            // self.filterData = FilterData()
            self.setLocationData()
            setCategoryData()
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            self.showAlert(keys.noInternet())
            return
        }
//        }).disposed(by: disposeBag)
    }

    func showOptionSelection(type: Int) {
        let vc = SB.home.optionSelectionViewController()!
        vc.optionType = type
        if type == 1 {
            vc.item = genresItems
            vc.selectedType = selectedGenreType.map { $0["genres_id"].stringValue }
        }
        vc.itemObserver.subscribe(onNext: { (items) in
            print("items:", items)
            if type == 1 {
                self.selectedGenreType = items
            }
            var str = ""
            for i in 0..<items.count {
                if items.count-1 == i {
                    str += "\(items[i]["genres_name"].stringValue)"
                    self.eventTypeStr += "\(items[i]["genres_id"].stringValue)"
                }
                else {
                    self.eventTypeStr += "\(items[i]["genres_id"].stringValue), "
                    str += "\(items[i]["genres_name"].stringValue), "
                }
            }
            if str != "" {
                self.btnSelectGenre.setTitle("  "+str, for: .normal)
            }
            else {
                self.btnSelectGenre.setTitle("  Select Music Genre(s)", for: .normal)
            }
            self.filterData.eventType = self.eventTypeStr
            print("eventTypeStr:", self.eventTypeStr)
            print("STR:", str)
            
        }).disposed(by: disposeBag)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
    }
    
    func validateLocation() {
        
        print("Location Stategey")
        
        if getUserDetail("is_location_flag") == "3" && self.selectedPlace == nil {
            showAlert(messages.pleaseEnterLocation())
            return
        } else if getUserDetail("is_location_flag") == "2" {
            self.checkLocation()
//            showAlert(messages.pleaseEnterLocation())
//            return
        } else if getUserDetail("is_location_flag") == "1" && self.selectedPlace == nil {
//            showAlert(messages.pleaseEnterLocation())
//            return
        }
        
        if userLocation != nil {
            self.filterData.latitude = userLocation?.coordinate.latitude
            self.filterData.longitude = userLocation?.coordinate.longitude
        }
        
    }
    
    func checkLocation() {
        if self.selectedPlace == nil && userLocation == nil {
            self.popupAlert(title: "Treegala", message: "Please Turn on the location or select the location", actionTitles: ["Pick Place", "Setting"], actions: [ { action1 in
                //                self.txtLocation.becomeFirstResponder()
                
                let vc = GMSAutocompleteViewController()
                UINavigationBar.appearance().tintColor = color.themePurple()
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                
                }, { action2 in
                    guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(urlGeneral, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(urlGeneral)
                    }
                }])
            return
        }
    }
    
    func setCategoryData() {
        let item1 = Category(id: "", name: "All", isSelected: false, subCategories: [])
        let item2 = Category(id: "1", name: "Nightlife", isSelected: true, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_big_nightclubs_select_category"), name: "Nightclubs", isSelected: true)])
        //        let item2 = Category(id: "1", name: "Nightlife", isSelected: flag, subCategories: [SubCategories(id: "1", categoryImage: #imageLiteral(resourceName: "ic_nighclub_home_popup"), name: "Nightclubs", isSelected: flag),
        //            SubCategories(id: "2", categoryImage: #imageLiteral(resourceName: "ic_club_crawls_home_popup"), name: "Club Crawls", isSelected: flag)])
        let item3 = Category(id: "2", name: "Festival", isSelected: false, subCategories: [SubCategories(id: "3", categoryImage: #imageLiteral(resourceName: "ic_gitar_mini_select_category"), name: "Music Festival", isSelected: false),
                                                                                          SubCategories(id: "4", categoryImage: #imageLiteral(resourceName: "ic_street_festivals_home_popup"), name: "Street Festival", isSelected: false)])
        let item4 = Category(id: "3", name: "Endurance Events", isSelected: false, subCategories: [SubCategories(id: "5", categoryImage:#imageLiteral(resourceName: "ic_big_endurance_select_category"), name: "Endurance Events", isSelected: false), SubCategories(id: "6", categoryImage:#imageLiteral(resourceName: "ic_big_vocal_events_select_category"), name: "Local Events", isSelected: false)])
        globalCategories = [item1, item2, item3, item4]
    }
}


//MARK: TextField/SearchBar Delegate
extension NightlifeViewController: UITextFieldDelegate, UISearchBarDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchBarHidden = true
        //        self.txtLocation.text = searchBar.text ?? ""
        let vc = SB.home.eventListViewController()!
        filterData.searchText = searchBar.text ?? ""
        self.filterData.latitude = userLocation?.coordinate.latitude
        self.filterData.longitude = userLocation?.coordinate.longitude
        vc.filterData = self.filterData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension NightlifeViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white
        
        self.selectedPlace = place
        
        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "United States" }) {
            filterData.distanceType = "1"
        } else {
            filterData.distanceType = "0"
        }
        filterData.selectedPlace = place
        self.btnEnterCity.setTitle(" " + "\(place.name ?? "")", for: .normal)
        //        txtLocation.text =  place.name
        filterData.locationCity = place.name ?? ""
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}


//MARK: Location Setup
extension NightlifeViewController {
   
    func setLocationData() {
        if getUserDetail("is_location_flag") == "1" {
            
            //            print(UserDefaults.standard.value(forKey: "locationName") as? String)
            //            if let name = (UserDefaults.standard.value(forKey: "locationName") as? String) {
            //                txtLocation.text =  name
            //            }
            
            guard let data = UserDefaults.standard.value(forKey: isUserLogin() ? R.string.keys.userDetail() : R.string.keys.guestDetail()) as? Data else { return }
            
            let json = JSON(data)
            
            if userPickedLocation == nil {
                startLoader()
                GMSPlacesClient.shared().lookUpPlaceID(json["place_id"].stringValue, callback: { (place, error) in
                    self.dismissLoader()
                    if let place = place {
                        if self.selectedPlace == nil {
                            self.selectedPlace = place
                            self.filterData.selectedPlace = place
//                            self.txtLocation.text =  place.name
                            self.btnEnterCity.setTitle("  "+place.name!, for: .normal)
                        }
                        
                        UserDefaults.standard.set(place.name, forKey: "locationName")
                        userPickedLocation = place
                        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                            self.filterData.distanceType = "1"
                        } else {
                            self.filterData.distanceType = "0"
                        }
                    }
                })
            } else {
                let place = userPickedLocation!
                
                if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "nited States" }) {
                    self.filterData.distanceType = "1"
                } else {
                    self.filterData.distanceType = "0"
                }
                
                if self.selectedPlace == nil {
                    self.selectedPlace = place
                    self.filterData.selectedPlace = place
//                    self.txtLocation.text =  place.name
                    filterData.locationCity = place.name ?? ""
                    self.btnEnterCity.setTitle("  "+place.name!, for: .normal)
                }
                //                self.filterData.selectedPlace = place
                //                self.selectedPlace = place
                //                print(userPickedLocation!)
                //                self.txtLocation.text = place.name
            }
        } else if getUserDetail("is_location_flag") == "3" {
            selectedPlace = nil
            self.btnEnterCity.setTitle("  Enter Location", for: .normal)
//            txtLocation.text = "Enter Location"
        }
    }
}

//MARK: API Setup
extension NightlifeViewController {
    func getOptionList() {
        
        let kURL = webUrls.baseUrl() + webUrls.getGenreList()
        print("URL:", kURL)
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "timezone" : TimeZone.current.identifier,
                     "lang" : "0"]
        print("PARAM:", param)
        
        showActivityIndicatory(btn: self.btnSelectGenre)
        self.btnSelectGenre.isUserInteractionEnabled = false
        GeneralRequest.makeGetRequest(kURL, param: param, context: self, isShowLoader: false) { (json) in
            print(json)
            self.stopActivityIndicatory(btn: self.btnSelectGenre)
            if json["flag"].stringValue == "1" {
                self.btnSelectGenre.isUserInteractionEnabled = true
                print("Music list:", json["data"]["genres"].arrayValue)
//                self.runTypes = json["data"]["event_type"].arrayValue
                self.genresItems = json["data"]["genres"].arrayValue
//                self.festivalItems = json["data"]["festival_type"].arrayValue
            }
        }
    }
}

//MARK: Location Delegate
extension NightlifeViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .notDetermined || status == .denied {
            locationManager.requestWhenInUseAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("location manager")
        if let location = locations.first {
            userLocation = location
            if getUserDetail("is_location_flag") == "2" || !isUserLogin() {
                getPlaceId(of: location)
            }
            locationManager.stopUpdatingLocation()
        }
    }
    
    func getPlaceId(of location: CLLocation) {
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&key=\(webUrls.googleAPIKey())"
        startLoader()
        request(url).responseSwiftyJSON { (response) in
            self.dismissLoader()
            if let json = response.result.value {
                print(json)
                if let item = json["results"].arrayValue.first {
                    print("Current Place Id")
                    for i in item["address_components"].arrayValue {
                        if i["types"].arrayValue.contains("country") {
                            if i["long_name"].stringValue == "United Status" {
                                self.filterData.distanceType = "1"
                                print("United State Country")
                            } else {
                                self.filterData.distanceType = "0"
                                print("Other Country")
                            }
                        }
                        
                        if i["types"].arrayValue.contains("locality") {
                            self.filterData.locationCity = i["long_name"].stringValue
                            self.btnEnterCity.setTitle("  "+i["long_name"].stringValue, for: .normal)
//                            self.txtLocation.text = i["long_name"].stringValue
                        }
                    }
                    self.filterData.currentPlaceID = item["place_id"].stringValue
                    self.getPlaceFromID(item["place_id"].stringValue)
                }
            }
        }
    }
    
    func getPlaceFromID(_ placeId: String) {
        GMSPlacesClient.shared().lookUpPlaceID(placeId, callback: { (place, error) in
            if let place = place {
                print(place.name)
                self.selectedPlace = place
                self.filterData.selectedPlace = place
            }
        })
    }
}
