//
//  NewFestivalsViewController.swift
//  treegala
//
//  Created by Khushbu on 06/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import UIKit
import RxSwift
import RxCocoa
import DropDown
import CoreLocation
import Alamofire
import SwiftyJSON


class NewFestivalsViewController: UIViewController {
    
    //MARK: Outlest
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet weak var imgAllFestival: UIImageView!
    @IBOutlet weak var imgCheckMusic: UIImageView!
    @IBOutlet weak var imgCheckStreet: UIImageView!
    
    //MARK: Variables
    var selectedPlace: GMSPlace?
    var filterData = FilterData()
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    var searchBtn = UIBarButtonItem()
    var isSearchBarHidden = true {
        didSet {
            self.view.endEditing(isSearchBarHidden)
            navigationItem.titleView = isSearchBarHidden ? nil : searchBar
            navigationItem.titleView?.tintColor = UIColor.black
            searchBtn.image = isSearchBarHidden ? UIImage(named: "ic_search_white") : UIImage(named: "ic_cancel_large")
        }
    }
    
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpUI()
    }
    
    
    
}

//MARK: setup UI
extension NewFestivalsViewController {
    
    func setUpUI() {
        self.setSideMenu()
        self.setNavigationRightButton()
        
        [imgCheckStreet,imgCheckMusic,imgAllFestival].forEach { (img) in
            img?.isHidden = true
        }
        
        isSearchBarHidden = true
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        //        setLocationData()
        
        if let textField = self.searchBar.subviews.first?.subviews.flatMap({ $0 as? UITextField }).first {
            textField.subviews.first?.isHidden = true
            textField.textColor = .black
            textField.layer.backgroundColor = UIColor.white.cgColor
            textField.layer.cornerRadius = 5
            textField.layer.masksToBounds = true
        }
        
        
    }
}

//MARK: Button Action
extension NewFestivalsViewController {
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAllFestivalsTapped(_ sender: UIButton) {
        
        imgCheckMusic.isHidden = true
        imgCheckStreet.isHidden = true
        imgAllFestival.isHidden = false
        
    }
    
    @IBAction func btnMusicFestivalsTapped(_ sender: UIButton) {
        imgCheckMusic.isHidden = false
        imgCheckStreet.isHidden = true
        imgAllFestival.isHidden = true
    }
    
    @IBAction func btnStreetFestivales(_ sender: UIButton) {
        imgCheckMusic.isHidden = true
        imgCheckStreet.isHidden = false
        imgAllFestival.isHidden = true
    }
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        
        if imgCheckStreet.isHidden && imgCheckMusic.isHidden && imgAllFestival.isHidden{
            showAlert(messages.selectEvent())
        }
        else
        {
            if !imgCheckMusic.isHidden{
                let vc = R.storyboard.newUpdates.newMusicFestivalVc()
                vc?.selectedController = .music
                self.navigationController?.pushViewController(vc!, animated: false)
            }else if !imgCheckStreet.isHidden{
                let vc = R.storyboard.newUpdates.newMusicFestivalVc()
                vc?.selectedController = .street
                self.navigationController?.pushViewController(vc!, animated: false)
            }else if !imgAllFestival.isHidden{
                let vc = R.storyboard.newUpdates.newAllFestivalsVc()
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
    }
    
    func setNavigationRightButton() {
        searchBtn = UIBarButtonItem(image: UIImage(named: ""), style: .plain, target: self, action: #selector(btnSearchTapped))
        self.navigationItem.rightBarButtonItem = searchBtn
    }
    
    @objc func btnSearchTapped() {
        searchBar.text = ""
        isSearchBarHidden = !isSearchBarHidden
    }
    
}


//MARK: TextField/SearchBar Delegate
extension NewFestivalsViewController: UITextFieldDelegate, UISearchBarDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = GMSAutocompleteViewController()
        UINavigationBar.appearance().tintColor = color.themePurple()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchBarHidden = true
        //        self.txtLocation.text = searchBar.text ?? ""
        let vc = SB.home.eventListViewController()!
        filterData.searchText = searchBar.text ?? ""
        self.filterData.latitude = userLocation?.coordinate.latitude
        self.filterData.longitude = userLocation?.coordinate.longitude
        vc.filterData = self.filterData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension NewFestivalsViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        UINavigationBar.appearance().tintColor = .white
        
        self.selectedPlace = place
        
        if place.addressComponents!.contains(where: { $0.type == "country" && $0.name == "United States" }) {
            filterData.distanceType = "1"
        } else {
            filterData.distanceType = "0"
        }
        filterData.selectedPlace = place
        //        txtLocation.text =  place.name
        filterData.locationCity = place.name ?? ""
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().tintColor = .white
        viewController.dismiss(animated: true, completion: nil)
    }
}
