//
//  FrequentlyAskedQuestionVC.swift
//  treegala
//
//  Created by Khushbu on 08/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

class FrequentlyAskedQuestionVC: UIViewController {

    //MARK: Outlest
    @IBOutlet var tblFrequntlyAskedQuestion: UITableView!
    
    
    //MARK: Variables
    var offset = 0
    var errorMessage = ""
    var arrayQuestionData : [JSON] = []
    
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
    }
}

//MARK: SetUpUI
extension FrequentlyAskedQuestionVC {
    
    func setUpUI() {
        
        registerXib()
        self.frequntlyAskQuestinApiData()
        
        self.title = "Frequently asked questions"
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_arrow_back_white"), style: .plain, target: self, action: #selector(logoClick))
        navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
    func registerXib() {
        self.tblFrequntlyAskedQuestion.register(UINib(nibName: "FrequntlyQuestionTableCell", bundle: nil), forCellReuseIdentifier: "FrequntlyQuestionTableCell")
    }
    
    @objc func logoClick() {
        NotificationCenter.default.post(name: NSNotification.Name.init("loadHome"), object: nil)
    }
}

//MARK: TableView Datasorce/Delegate
extension FrequentlyAskedQuestionVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrayQuestionData.isEmpty {
            setTableView(errorMessage, tableView: tableView)
            return 0
        } else {
            tableView.backgroundView = nil
            return self.arrayQuestionData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FrequntlyQuestionTableCell", for: indexPath) as! FrequntlyQuestionTableCell
        
        cell.selectionStyle = .none
        cell.setDetail(self.arrayQuestionData[indexPath.row])
        
        
        return cell
    }
}


//MARK: ApiSetUp
extension FrequentlyAskedQuestionVC {
    
    func frequntlyAskQuestinApiData() {
        let kURL = webUrls.baseUrl() + webUrls.getFaqList()
        
        let param = ["user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "lang" : "0",
                     "timezone" : TimeZone.current.identifier,
                     "offset" : "\(offset)"]
        
        GeneralRequest.makeRequest(kURL, param: param, context: self) { (json) in
            if json["flag"].stringValue == "1" {
                print("JSON:", json)
                self.offset = json["next_offset"].intValue
                self.arrayQuestionData = json["data"].arrayValue
            } else {
                self.errorMessage = json["msg"].stringValue
            }
            self.tblFrequntlyAskedQuestion.reloadData()
        }
    }
}

//MARK:
extension FrequentlyAskedQuestionVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.tblFrequntlyAskedQuestion {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                if offset == -1 {
                    tblFrequntlyAskedQuestion.tableFooterView = nil
                    return
                } else {
                    frequntlyAskQuestinApiData()
                }
            }
        }
    }
}

class FrequentlyAskedQuestionNavigationController: UINavigationController {
    
}
