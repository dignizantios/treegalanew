//
//  FrequntlyQuestionTableCell.swift
//  treegala
//
//  Created by Khushbu on 08/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import SwiftyJSON

class FrequntlyQuestionTableCell: UITableViewCell {

    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblAnswer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDetail(_ json: JSON) {
        
        lblQuestion.text = json["question"].stringValue
        lblAnswer.text = json["answer"].stringValue
    }
}
