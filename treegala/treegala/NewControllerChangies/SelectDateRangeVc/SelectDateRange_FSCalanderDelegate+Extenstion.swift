//
//  SelectDateRange_FSCalanderDelegate+Extenstion.swift
//  treegala
//
//  Created by Abhay on 24/06/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import FSCalendar
import UIKit

//MARK: Calendar SetUp Delegate/Datasource

extension SelectDateRangeVC : FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
// MARK:- FSCalendarDataSource
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    /*
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        if self.gregorian.isDateInToday(date) {
            return "A"
        }
        return nil
    }*/
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
            let todayDate = Date()
            let tDate = minimumDate(for: calendar, yourDate:todayDate)
            if date == tDate {
                return UIColor.black
            }
            else if date.compare(Date()) == .orderedAscending {
                return UIColor.appThemeLightGrayColor
            }
            else {
                return UIColor.black
            }
            /*if (date < todayDate) {
                return UIColor.appLightGrayColor
            } else {
                return UIColor.appBlackColor
            }*/
    }



    // MARK:- FSCalendarDelegate

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        let todayDate = Date()
        
        ///----- Check today date to less date
    let tDate = minimumDate(for: calendar, yourDate:todayDate)
    let pDate = minimumDate(for: calendar,yourDate:date)
    print("tDate-->",tDate)
    print("date-->",date)
    print("todayDate-->",todayDate)
        if date == tDate {
            return true
        }
        else if date.compare(Date()) == .orderedAscending {
            return false
        }
        else {
            return true
        }
        /*
         if (date <= todayDate) {
            return false
        } else {
            return true
        }*/
        //return monthPosition == .current
    }
    func minimumDate(for calendar: FSCalendar,yourDate:Date) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: yourDate!)
        return formatter.date(from: strCurrentDate)!
    }
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    print("did select date \(self.formatter.string(from: date))")
//self.configureVisibleCells()
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        //nothing selection
        if self.firstDate == nil {
            self.firstDate = date
            self.datesRange = [self.firstDate!]
            
            print("datesRange contains: \(self.datesRange!)")
            //TODO: - Yash Changes
          //  self.lblStartDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
            
//            if self.mainView.lblEndDate.text == getCommonString(key: "End_date_key") {
//                self.mainView.lblEndDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
//            }
            
            self.configureVisibleCells()
            return
        }
        
        
        //Only first date selected
        if (self.firstDate != nil && self.lastDate == nil) {
            // handle the case of if the last date is less than the first date:
            if (date <= self.firstDate!) {
                calendar.deselect(self.firstDate!)
                self.firstDate = date
                self.datesRange = [self.firstDate!]
                print("datesRange Contains: \(self.datesRange!)")
//                self.mainView.lblStartDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
                
                //TODO: - Yash Changes
            //    self.lblStartDate.text = "\(self.mainView.FLDateFormatter.string(from: self.firstDate!))"
                self.configureVisibleCells()
                return
            }
            let range = datesRange(from: self.firstDate!, to: date)
            self.lastDate = range.last
            for d in range {
                calendar.select(d)
            }
            self.datesRange = range
            print("datesRange contains: \(self.datesRange!)")
            
            //TODO: - Yash Changes
//            self.mainView.lblStartDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
//            self.mainView.lblEndDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.lastDate!))"
            self.configureVisibleCells()
            return
        }
        
        //Both are selected:
        if self.firstDate != nil && self.lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            //TODO: - Yash Changes
//            self.mainView.lblStartDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
//            self.mainView.lblEndDate.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.lastDate!))"
            
            self.lastDate = nil
            self.firstDate = nil
            self.datesRange = []
            
            if self.datesRange == [] {
                //TODO: - Yash Changes
//                self.mainView.lblStartDate.text = getCommonString(key: "Start_date_key")
//                self.mainView.lblEndDate.text = getCommonString(key: "End_date_key")
            }
            
            print("datesRange contains: \(self.datesRange!)")
            self.configureVisibleCells()
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
         print("did deselect date \(self.formatter.string(from: date))")
        //self.configureVisibleCells()

        //Both are selected:
        //NOTE: the is a REDUANDENT CODE:
        if self.firstDate != nil && self.lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
//            self.mainView.lblStartTimeValue.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
//            self.mainView.lblEndDateValue.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.lastDate!))"
            self.lastDate = nil
            self.firstDate = nil
            self.datesRange = []
            
            //Start_date_key        End_date_key
            
            //TODO: - Yash Changes
//            self.mainView.lblStartDate.text = getCommonString(key: "Start_date_key")
//            self.mainView.lblEndDate.text = getCommonString(key: "End_date_key")
            print("datesRange contains: \(self.datesRange!)")
            self.configureVisibleCells()
        }

        if self.firstDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            //            self.mainView.lblStartTimeValue.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.firstDate!))"
            //            self.mainView.lblEndDateValue.text = "\(self.mainView.FLDateFormatter.string(from: self.mainView.lastDate!))"
            self.lastDate = nil
            self.firstDate = nil
            self.datesRange = []
            
            //Start_date_key        End_date_key
            //TODO: - Yash Changes
//            self.mainView.lblStartDate.text = getCommonString(key: "Start_date_key")
//            self.mainView.lblEndDate.text = getCommonString(key: "End_date_key")
            print("datesRange contains: \(self.datesRange!)")
            self.configureVisibleCells()
        }
        
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    // MARK: - Private functions
    
    private func configureVisibleCells() {

        self.vwCalendar.visibleCells().forEach { (cell) in
            let date = self.vwCalendar.date(for: cell)
            let position = self.vwCalendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        // Custom today circle Abhay
       // diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current {
            
            var selectionType = SelectionType.none
            
            if self.vwCalendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if self.vwCalendar.selectedDates.contains(date) {
                    if self.vwCalendar.selectedDates.contains(previousDate) && self.vwCalendar.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if self.vwCalendar.selectedDates.contains(previousDate) && self.vwCalendar.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if self.vwCalendar.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
            diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
        }
    }
    
}


/*
//MARK: Textfield delegate/datasource
extension SelectDateRangeVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.mainView.txtStartTime {
            
            self.mainView.startDate = true
            
            let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 0
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle   = .coverVertical
            
            self.present(obj, animated: true, completion: nil)
            
            return false
        }
        else if textField == self.mainView.txtEndTime {
            
            self.mainView.startDate = false
            
            let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 0
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle   = .coverVertical
            
            self.present(obj, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
}

//MARK: Datepicker Delegate
extension TripCalanderVC : DateTimePickerDelegate {
    
    func setDateandTime(dateValue: Date, type: Int) {
        
        if type == 0 {
            
            
            let dateFormatter = DateFormatter()
            
            //            dateFormatter.dateFormat = "EEEE,MMM d"
            //            let date = dateFormatter.string(from: dateValue)
            
            dateFormatter.dateFormat = "h:mm a"
            let time = dateFormatter.string(from: dateValue)
            
            if self.mainView.startDate {
                
                self.mainView.txtStartTime.text = "\(time)"
                self.mainView.lblStartTimeValue.text = "\(time)"
                
                self.mainView.startTimeValue    = time
                
            }
            else {
                self.mainView.txtEndTime.text   = "\(time)"
                self.mainView.lblEndDateValue.text   = "\(time)"
                
                self.mainView.endTimeValue      = time
            }
        }
    }
}
*/
