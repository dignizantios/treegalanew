//
//  SelectDateRangeVC.swift
//  treegala
//
//  Created by YASH on 08/11/19.
//  Copyright © 2019 PC. All rights reserved.
//

import UIKit
import FSCalendar

class SelectDateRangeVC: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var vwCalendar: FSCalendar!
    
    //MARK: - Variable
    
    var currentPage: Date?
    
    let gregorian = Calendar(identifier: .gregorian)
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    var firstDate : Date? //First Date Selection
    var lastDate  : Date?  //Last Date Selection
    var datesRange: [Date]?
    let FLDateFormatter = DateFormatter()
    var selectDateHandlor:(String,String) -> Void = { _,_ in}
    

    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }
   
    func setupUI() {
        
        self.setupCalendar()
    }
    
    func setupCalendar() {
        //setup Calander
        let width = self.vwCalendar.frame.width
        
        vwCalendar.allowsMultipleSelection = true
        vwCalendar.appearance.eventSelectionColor = UIColor.green
        vwCalendar.firstWeekday = 1
        vwCalendar.placeholderType = FSCalendarPlaceholderType(rawValue: 0)!
        vwCalendar.allowsMultipleSelection = true
        vwCalendar.appearance.eventDefaultColor = UIColor.black
        vwCalendar.appearance.eventSelectionColor = UIColor.white
        vwCalendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        FLDateFormatter.dateFormat = "EEE,d MMMM"
        
    }
}


//MARK: - IBAction

extension SelectDateRangeVC {
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        if firstDate == nil || lastDate == nil {
            self.showAlert("Please show to date")
            return
        }
        
        let FD = DateToString(Formatter: "yyyy-MM-dd", date: firstDate!)
        let LD = DateToString(Formatter: "yyyy-MM-dd", date: lastDate!)
        
        self.selectDateHandlor(FD,LD)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnDismissTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func btnPreviousTapped(_ sender: UIButton) {
        self.moveCurrentPage(moveUp: false)
    }
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        self.moveCurrentPage(moveUp: true)
    }
    
    func moveCurrentPage(moveUp: Bool) {
        
        let calendar = Calendar.current
        var dateComponents = DateComponents()
        
        dateComponents.month = moveUp ? 1 : -1
        
        self.currentPage = calendar.date(byAdding: dateComponents, to: self.currentPage ?? Date())
        self.vwCalendar.setCurrentPage(self.currentPage!, animated: true)
        
    }

}
