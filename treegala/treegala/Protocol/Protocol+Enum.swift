//
//  Protocol+Enum.swift
//  treegala
//
//  Created by om on 8/6/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation

//MARK: - Protocol


protocol OffsetDelegate {
    func offsetValueChanged(_ offset: CGFloat)
}


enum EventType: Int {
    case nightClub = 0
    case clubCrawl
    case music
    case street
    case obstacle
    case local
    
    static func type(index: Int) -> EventType {
        return EventType.init(rawValue: index)!
    }
}

enum SortType: Int {
    case distance = 0
//    case cost = 1
    case rating = 2
    case random = 3
}
