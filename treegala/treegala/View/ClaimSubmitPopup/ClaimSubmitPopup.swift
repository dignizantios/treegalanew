//
//  ClaimSubmitPopup.swift
//  treegala
//
//  Created by om on 9/5/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

class ClaimSubmitPopup: UIViewController {

    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var dictData = JSON()
    
    var claimStatusObserver = PublishSubject<Bool>()
    
    var disposeBag = DisposeBag()
    
    convenience init() {
        self.init(nibName: nib.claimSubmitPopup.name, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnClose.rx.tap.subscribe(onNext: {
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        btnSubmit.rx.tap.subscribe(onNext: {
            let kURL = webUrls.baseUrl() + webUrls.eventClaim()
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "message" : self.txtMessage.text!,
                         "listing_id" :self.dictData["listing_id"].stringValue,
                         "lang" : "0",
                         "timezone" : TimeZone.current.identifier ]
            
            GeneralRequest.makeRequest(kURL, param: param, context: self, completion: { (json) in
                print(json)
                if json["flag"].stringValue == "1" {
                    self.popupAlert(title: keys.appTitle(), message: json["msg"].stringValue, actionTitles: ["OK"], actions: [{ action in
                            self.dismiss(animated: true, completion: nil)
                            self.claimStatusObserver.onNext(true)
                        }])
                    
                } else {
                    self.claimStatusObserver.onNext(false)
                    self.showAlert(json["msg"].stringValue)
                }
            })
        }).disposed(by: disposeBag)
    }
    
}
