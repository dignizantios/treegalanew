//
//  ImagePreviewPopup.swift
//  treegala
//
//  Created by om on 8/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class ImagePreviewPopup: UIViewController {
    
    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgPreview: UIImageView!
    
    var imageURL = ""
    var disposeBag = DisposeBag()
    
    convenience init() {
        self.init(nibName: nib.imagePreviewPopup.name, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgPreview.sd_setImage(with: imageURL.toURL(), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: .highPriority, completed: nil)
        
        scrolView.delegate = self
        scrolView.minimumZoomScale = 1.0
        scrolView.maximumZoomScale = 10.0
        
        btnClose.rx.tap.subscribe(onNext: {
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)

        // Do any additional setup after loading the view.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgPreview
    }

}
