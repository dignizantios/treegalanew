//
//  InfoPopupViewController.swift
//  treegala
//
//  Created by om on 8/9/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import RxSwift

class InfoPopupViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    var strTitle = ""
    var message = ""
    
    var dismissObserver = PublishSubject<()>()
    
    convenience init(_ title: String, message: String) {
        self.init(nibName: nib.infoPopupView.name, bundle: nil)
        
        self.strTitle = title
        self.message = message
     
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = strTitle
        lblMessage.text = message
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        dismissObserver.onNext(())
    }

}
