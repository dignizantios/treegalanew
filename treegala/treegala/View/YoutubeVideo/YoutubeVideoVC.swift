//
//  YoutubeVideoVC.swift
//  treegala
//
//  Created by Jaydeep on 27/11/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
//import YouTubePlayer
import AVKit
import XCDYouTubeKit

class YoutubeVideoVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var strVideoURL = String()
    
    //MARK:- Outlet Zone
    
//    @IBOutlet weak var viewYoutubePlayer: YouTubePlayerView!

    override func viewDidLoad() {
        super.viewDidLoad()

//        viewYoutubePlayer.delegate = self
//        viewYoutubePlayer.loadVideoURL(URL(string: strVideoURL)!)
        playVideoInView(strVideoURL: strVideoURL)
        
    }
    
    func playVideoInView(strVideoURL:String) {
        let playerViewController = AVPlayerViewController()
        
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:self.view.frame.height)
        
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
//        weakPlayerViewController?.delegate = self
        
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { [weak self] (video, error) in
            /*if let obj = self {
             obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
             }*/
            if video != nil {
                var streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                    weakPlayerViewController?.player?.play()
                }
                //
            } else {
            }
        }
        
        self.present(weakPlayerViewController!, animated: true, completion: nil)
    }
  

}

//MARK:- Delegate

/*extension YoutubeVideoVC: YouTubePlayerDelegate
{
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState) {
         print("playerState \(playerState)")
        switch playerState {
        case .Ended:
            self.dismiss(animated: true, completion: nil)
        case .Paused:
            self.dismiss(animated: true, completion: nil)
            break
        case .Unstarted:
            self.dismiss(animated: true, completion: nil)
            break
        case .Playing:
            break
        case .Buffering:
            break
        case .Queued:
            break
        }
    }
    
    
    
    
}*/
