//
//  EventDetailHeaderView.swift
//  treegala
//
//  Created by Jaydeep on 20/10/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

class EventDetailHeaderView: UIView {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnHeadlinesOutlet: UIButton!
    @IBOutlet weak var btnEventOutlet: UIButton!
    @IBOutlet weak var btnDesciptionOutlet: UIButton!
    @IBOutlet weak var btnRatingOutlet: UIButton!
    @IBOutlet weak var lblBottomRatting: UILabel!
    @IBOutlet weak var lblBottomDescription: UILabel!
    @IBOutlet weak var lblBottomEvent: UILabel!
    @IBOutlet weak var lblBottomHeadlines: UILabel!
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        btnHeadlinesOutlet.tag = 0
        btnEventOutlet.tag = 1
        btnDesciptionOutlet.tag = 2
        btnRatingOutlet.tag = 3
    }

}
