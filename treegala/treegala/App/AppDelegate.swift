//
//  AppDelegate.swift
//  treegala
//
//  Created by PC on 8/6/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import IQKeyboardManager
import FBSDKLoginKit
import FacebookCore
import TwitterKit
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
     static let shared:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
//        Messaging.messaging().delegate = self
        
        IQKeyboardManager.shared().isEnabled = true
        
        GMSServices.provideAPIKey(webUrls.googleAPIKey())
        GMSPlacesClient.provideAPIKey(webUrls.googleAPIKey())
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)      
        
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:kTwitterConsumerKey, consumerSecret:kTwitterConsumerSecret)
        
        /*if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let option = UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound], completionHandler: { (_, _) in
                
            })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()*/
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        
        print("--------------------")
        
        UINavigationBar.appearance().titleTextAttributes =  [NSAttributedStringKey.foregroundColor : UIColor.white,
                                                            NSAttributedStringKey.font: UIFont(name: "UbuntuCondensed-Regular", size: 22.0)!]
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        connectToFcm()
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        return ApplicationDelegate.shared.application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if ApplicationDelegate.shared.application(app, open: url, options: options) {
            return true
        }
        
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    func setupSideMenu(){

        let leftViewController = SideMenuViewController.instantiateFromAppStoryboard(appStoryboard: .main)
        
        let HomeNavigation: UINavigationController = HomeNavigationController.instantiateFromAppStoryboard(appStoryboard: .home)
     
        let slideMenuController = SlideMenuController(mainViewController: HomeNavigation, leftMenuViewController: leftViewController)
        slideMenuController.leftPanGesture?.isEnabled = false
        slideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width - 73)
        
        SlideMenuOptions.contentViewScale = 1
        SlideMenuOptions.shadowRadius = 0.5
        SlideMenuOptions.shadowOpacity = 0.2
        SlideMenuOptions.contentViewOpacity = 0.4
        SlideMenuOptions.hideStatusBar = false
        configure(slideMenuController)
    }

    func configure(_ vc: UIViewController) {
        guard let window = window else { return }
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
}

/*extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
    
        UserDefaults.standard.set(token, forKey: R.string.keys.deviceToken())
        
        Messaging.messaging().apnsToken = deviceToken   
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Remote Notification Error \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Firebase")
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
}*/
