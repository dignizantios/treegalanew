//
//  Global.swift
//  treegala
//
//  Created by om on 8/6/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RxCocoa
import RxSwift

let appDelegate = UIApplication.shared.delegate as! AppDelegate

typealias SB = R.storyboard
typealias font = R.font
typealias nib = R.nib
typealias color = R.clr.treegala
typealias webUrls = R.string.webUrls
typealias messages = R.string.message
typealias keys = R.string.keys

var isLoginPresent = false

var userLocation: CLLocation?

var userPickedLocation: GMSPlace?
var globalCategories: [Category] = []
var setGlobalFilterCheckMark = false
var selectedGlobalCategory = PublishSubject<[Category]>()

var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
var loadingView: UIView = UIView()

func getUserDetail(_ forKey: String) -> String {
    guard let data = UserDefaults.standard.value(forKey: isUserLogin() ? R.string.keys.userDetail() : R.string.keys.guestDetail()) as? Data else { return "" }
    
    let json = JSON(data)
//    print("User Detail ------------")
//    print(json)
    return json[forKey].stringValue
}

func isUserLogin() -> Bool {
    return UserDefaults.standard.bool(forKey: R.string.keys.isUserLogin())
}


//MARK:- Twitter

var kTwitterConsumerKey = "Fh2h7pqp8lcgXYnCKrVZe1km0"
var kTwitterConsumerSecret = "17oOLGONljj98vp7HvoSI7FC1b2WmBfJ9ZkilMHeAJGRdblOaU"
