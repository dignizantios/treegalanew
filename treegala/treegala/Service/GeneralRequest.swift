//
//  GeneralRequest.swift
//  treegala
//
//  Created by om on 8/14/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class GeneralRequest {
    
    class func makeRequest(_ url: String, param: [String : String], context: UIViewController, isShowLoader: Bool = true, completion: @escaping (JSON) -> ()) {

        if Rechability.isConnectedToNetwork() {
            if isShowLoader {
                context.startLoader()
            }
            request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: webUrls.authUserName(), password: webUrls.authPassword()).responseSwiftyJSON { (response) in
                
                print(response.response?.statusCode)
                
                if isShowLoader {
                    context.dismissLoader()
                }
                if response.result.isSuccess, let json = response.result.value {
                    if json["flag"].stringValue == "-1" {
                        UserDefaults.standard.set(false, forKey: R.string.keys.isUserLogin())
                        appDelegate.setupSideMenu()
                    } else {
                        completion(json)
                    }
                } else {
                    completion(JSON())
                    context.showAlert(R.string.keys.somethingWentWrong())
                }
            }
        } else {
            context.showAlert(R.string.keys.noInternet())
        }
    }
    
    class func makeGetRequest(_ url: String, param: [String : String], context: UIViewController, isShowLoader: Bool = true, completion: @escaping (JSON) -> ()) {

        let kURL =  GeneralRequest.queryString(url, params: param) ?? ""
        
        print(kURL)
        
        if Rechability.isConnectedToNetwork() {
            if isShowLoader {
                context.startLoader()
            }
            request(kURL).authenticate(user: webUrls.authUserName(), password: webUrls.authPassword()).responseSwiftyJSON { (response) in
                if isShowLoader {
                    context.dismissLoader()
                }
                
                if response.result.isSuccess, let json = response.result.value {
                    if json["flag"].stringValue == "-1" {
                        UserDefaults.standard.set(false, forKey: R.string.keys.isUserLogin())
                        appDelegate.setupSideMenu()
                    } else {
                        completion(json)
                    }
                } else {
                    context.showAlert(R.string.keys.somethingWentWrong())
                }
            }
        } else {
            context.showAlert(R.string.keys.noInternet())
        }
    }
    
    class func queryString(_ value: String, params: [String: String]) -> String? {
        var components = URLComponents(string: value)
        components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }
        
        return components?.url?.absoluteString
    }
    
    class func makeGetNewRequest(_ url: String, param: [String : String], context: UIViewController, isShowLoader: Bool = true, completion: @escaping (JSON) -> ()) {
        
        let kURL =  GeneralRequest.queryString(url, params: param) ?? ""
        
        print(kURL)
        
        if Rechability.isConnectedToNetwork() {
            if isShowLoader {
                context.startLoader()
            }
            request(kURL).authenticate(user: webUrls.authUserName(), password: webUrls.authPassword()).responseSwiftyJSON { (response) in
                if isShowLoader {
                    context.dismissLoader()
                }
                
                if response.result.isSuccess, let json = response.result.value {
                    if json["flag"].stringValue == "-1" {
                        UserDefaults.standard.set(false, forKey: R.string.keys.isUserLogin())
                        appDelegate.setupSideMenu()
                    } else {
                        completion(json)
                    }
                } else {
                    context.showAlert(R.string.keys.somethingWentWrong())
                }
            }
        } else {
            context.showAlert(R.string.keys.noInternet())
        }
    }
}
