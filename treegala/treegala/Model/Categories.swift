//
//  Categories.swift
//  treegala
//
//  Created by om on 8/7/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation
import UIKit

struct Category {
    var id = ""
    var name = ""
    var isSelected = false
    var subCategories: [SubCategories] = []
}

struct SubCategories {
    var id = ""
    var categoryImage: UIImage
    var name = ""
    var isSelected = false
}
