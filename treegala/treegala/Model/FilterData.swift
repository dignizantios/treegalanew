//
//  FilterData.swift
//  treegala
//
//  Created by om on 8/15/18.
//  Copyright © 2018 PC. All rights reserved.
//

import Foundation
import SwiftyJSON

class FilterData {
    
    var searchText = ""
    
    var selectedPlace: GMSPlace?
    var locationCity = ""
    var distanceType = "0"
    
    var currentPlaceID = ""
    
    var latitude: Double? = nil
    var longitude: Double? = nil
    
    var isMainCategorySelected = false
    var mainCategory = ""
    var category: [Category] = []
    
    var isUpcomingEvent = ""
    
    var sortType: SortType?
    var isAscending = false
    
    var isFamilyFriend = false
    var isFree = false
    var isLGBTQ = false
    
    var difficultyLevel = 1
    
    var runType = ""
    var isPrizeAward = false
    
    var genreType = ""
    var festivalType = ""
    
    //MARK: NewSetup
    var eventType = ""
    var lgbtqType = "0"
    
    var free = "0"
    var isFamilyFriendly = "0"
    var prizeAward = "0"
    var isDifficultyLevel = 0
    
    
    var fromDateStr = ""
    var toDateStr = ""
    
    
    
    /*  filter_data: required (string)
     passed json {
     "lgbtq":"0",
     "difficulty_level":"1",
     "event_type":"1,2",
     "prize_award":"1",
     "genres":"1,2",
     "festival_type":"1,2"
     
     } passed genres, event type and festival type id comma saparated.
     difficulty_level 1=easy,2-moderate,3-difficult.
     lgbtq 0=off,1-on     */
}
