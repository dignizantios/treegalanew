//
//  Validation.swift
//  App Utility
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

struct Validation {
    func isConnectedToInternet() -> Bool {
        return NetworkConnectivity().isInternetConnection()
    }
    static func isValidUserName(strUserName: String?) -> Bool {
        if strUserName == nil {
            return false
        }
        
        let usernameRegEx = "[A-Z0-9a-z._]"
        let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        
        return usernameTest.evaluate(with: strUserName)
    }
    
    static func isNumericNumber(strNumber: String) -> Bool {
        let numberRegEx = "\\d+(?:\\.\\d+)?"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        return numberTest.evaluate(with:strNumber)
    }
    
    static func isValidEmailAddress(strEmail: String?) -> Bool {
        if strEmail == nil {
            return false
        }
        
        //        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailRegEx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: strEmail)
    }
    
    static func isStringEmpty(_ theString: String?) -> Bool {
        if theString != nil {
            let str = theString!.trimmingCharacters(in: .whitespaces)
            
            if str.count == 0 {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    static func isPasswordValid(_ theString: String?) -> Bool {
        
        if theString != nil {
            let str = theString ?? ""
            print(str)
            if str.count > 5 {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    static func isValidURL(_ theString: String?) -> Bool {
        if theString == nil {
            return false
        }
        
        if let url = URL(string: theString!) {
            return UIApplication.shared.canOpenURL(url)
        }
        
        return false
    }
    
    static func isValidPhoneNumber(_ theString: String?) -> Bool {
        if theString != nil {
            if theString!.count > 7 {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
        
        /*
         if theString != nil {
         let PHONE_REGEX = "[0-9]{10}"
         let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
         let result =  phoneTest.evaluate(with: theString!)
         return result
         } else {
         return false
         }*/
    }
    
    static func isConfirmPasswordMatches(strPassword: String?, strConfirmPassword: String?) -> Bool {
        if strPassword == nil || strConfirmPassword == nil {
            return false
        }
        
        if strPassword! == strConfirmPassword! {
            return true
        } else {
            return false
        }
    }
    
    static func stringValueForTheObject(theValue: Any?) -> String {
        var strValue: String = ""
        
        guard theValue != nil else {
            return strValue
        }
        
        guard !(theValue is NSNull) else {
            return strValue
        }
        
        if ((theValue as? NSNumber) != nil) {
            let theInt = Int(truncating: theValue as! NSNumber)
            strValue  = String(theInt)
        } else {
            if ((theValue as? String) != nil) {
                strValue = theValue as! String
            }
        }
        
        return strValue
    }
    /*
    static func theLanguageKey() -> String {
        return (MOLHLanguage.isArabic() ? "1" : "0")
    }*/
    
    static func theTimeZone() -> String {
        return TimeZone.current.identifier
    }
    
    static func convertAnyToString(obj:Any??) -> String {
        
        if let converted = obj as? String {
            return converted
        } else if let converted = obj as? Int {
            return String(converted)
        } else if let converted = obj as? Float {
            return String(converted)
        } else if let converted = obj as? Double {
            return String(converted)
        }
        return ""
    }
}

struct ValidationTexts {
    
    static let NO_INTERNET_CONNECTION   = "No internet connection. Please try again later."
    
    
}
