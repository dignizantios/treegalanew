//
//  GlobalStoryboards.swift
//  App Utility
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit

enum AppStoryBoardString: String
{
    //Storyboards
    case main     = "Main"
    case signin   = "Signin"
    case home     = "Home"

    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController
{
    // Storyboard
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func instantiateFromAppStoryboard(appStoryboard: AppStoryBoardString) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
}
