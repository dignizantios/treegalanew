//
//  xUILabel.swift
//  treegala
//
//  Created by PC on 7/25/18.
//  Copyright © 2018 PC. All rights reserved.
//

import UIKit

@IBDesignable
open class xUILabel:UILabel {
    
    @IBInspectable
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.clipsToBounds = true
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0.0 {
        didSet {
            self.clipsToBounds = true
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    
}
