//
//  CustomImagePicker.swift
//  Custom Classes
//
//  Created by pc
//  Copyright © 22018 dignizant. Ltd. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

enum PickerType: Int {
    case onlyPhoto = 0
    case onlyVideo = 1
    case both = 2
}

class CustomImagePicker: NSObject {
    
    var typeOfPicker: PickerType = PickerType.onlyPhoto
    
    fileprivate var theNavigationColor: UIColor? = nil
    fileprivate var theImagePicked: (([String: AnyObject]) -> Void)? = nil
    fileprivate var theImageCanceled: (() -> Void)? = nil
    fileprivate var theImageRemoved: (() -> Void)? = nil
    fileprivate var theDelegate: UIViewController? = nil
    
    func showImagePicker(fromViewController: UIViewController,
                         navigationColor: UIColor?,
                         imagePicked: @escaping ([String: AnyObject]) -> Void,
                         imageCanceled: @escaping () -> Void,
                         imageRemoved: (() -> Void)?) {
        
        theDelegate = fromViewController
        theNavigationColor = navigationColor
        theImagePicked = imagePicked
        theImageCanceled = imageCanceled
        theImageRemoved = imageRemoved
        
        var strTitle = "Choose/Capture a Photo"
        
        if typeOfPicker == PickerType.both {
            strTitle = "Choose/Capture a Photo or a Video"
        } else if typeOfPicker == PickerType.onlyVideo {
            strTitle = "Choose/Capture a Video"
        }
        
        let theAlertController = UIAlertController(title: strTitle,
                                                   message: "",
                                                   preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Photo Album",
                                        style: .default,
                                        handler: { (alertAction) in
                                            self.showGallery()
                                        })
        
        theAlertController.addAction(photoAction)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraAction = UIAlertAction(title: "Camera",
                                             style: .default,
                                             handler: { (alertAction) in
                                                self.showCamera()
                                             })
        
            theAlertController.addAction(cameraAction)
        }
        
        if theImageRemoved != nil {
            var strRemoveTitle = "Remove a Photo"
            
            if typeOfPicker == PickerType.both {
                strRemoveTitle = "Remove a Photo or a Video"
            } else if typeOfPicker == PickerType.onlyVideo {
                strRemoveTitle = "Remove a Video"
            }
            
            let removeAction = UIAlertAction(title: strRemoveTitle,
                                             style: .destructive,
                                             handler: { (alertAction) in
                                                if self.theImageRemoved != nil {
                                                    self.theImageRemoved!()
                                                }
                                            })
            
            theAlertController.addAction(removeAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel,
                                         handler: { (alertAction) in
                                            if self.theImageCanceled != nil {
                                                self.theImageCanceled!()
                                            }
                                        })
        
        theAlertController.addAction(cancelAction)
        
        fromViewController.present(theAlertController, animated: true, completion: nil)
    }
    
    private func showGallery() {
        obtainPermissionForMediaSourceType(sourceType: .photoLibrary,
                                           success: {
                                                self.prepareForImagePicker(isForGallery: true)
                                            }, failed: {
                                                self.showNotAuthorizedAlert(isForGallery: true)
                                            })
    }
    
    private func showCamera() {
        obtainPermissionForMediaSourceType(sourceType: .camera,
                                           success: {
                                                self.prepareForImagePicker(isForGallery: false)
                                           }, failed: {
                                                self.showNotAuthorizedAlert(isForGallery: false)
                                           })
    }
    
    private func obtainPermissionForMediaSourceType(sourceType: UIImagePickerControllerSourceType,
                                                    success: @escaping () -> Void,
                                                    failed: @escaping () -> Void) {
        if sourceType == .photoLibrary || sourceType == .savedPhotosAlbum {
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                switch authorizationStatus {
                    case .restricted:
                        failed()
                    
                    case .denied:
                        failed()
                    
                    case .authorized:
                        success()
                    
                    default:
                        break
                }
            })
        } else if sourceType == .camera {
            let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch status {
                case .authorized:
                    success()
                
                case .notDetermined:
                    AVCaptureDevice .requestAccess(for: AVMediaType.video,
                                                   completionHandler: { (isGranted) in
                                                                            if isGranted {
                                                                                success()
                                                                            } else {
                                                                                failed()
                                                                            }
                                                                      })
                
                default: // .denied .restricted
                    failed()
            }
        } else {
            assert(false, "no permission type found")
        }
    }
    
    
    private func prepareForImagePicker(isForGallery: Bool) {
        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = (isForGallery ? .photoLibrary : .camera)
        
        switch self.typeOfPicker {
            case .both:
                mediaUI.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                
            case .onlyVideo:
                mediaUI.mediaTypes = [kUTTypeMovie as String]
                
            default:
                mediaUI.mediaTypes = [kUTTypeImage as String]
        }
        
        mediaUI.delegate = self
        
        self.theDelegate?.present(mediaUI, animated: true, completion: nil)
    }
    
    private func showNotAuthorizedAlert(isForGallery: Bool) {
        let msg = "You have disabled X access?"
        
        let theMessage = NSString(format: msg as NSString, "\(isForGallery ? "Photos" : "Camera")")
        
//        let theMessage = "You have disabled \(isForGallery ? "Photos" : "Camera") access"
        
        let theAlertController = UIAlertController(title: "", message: theMessage as String, preferredStyle: .actionSheet)
        
        let openSettingsAction = UIAlertAction(title: "Open Settings",
                                               style: .default,
                                               handler: { (alertAction) in
                                                    let theURL = URL(string: UIApplicationOpenSettingsURLString)
                                                if #available(iOS 10.0, *) {
                                                    UIApplication.shared.open(theURL!, options: [:], completionHandler: { (isOpened) in })
                                                } else {
                                                    // Fallback on earlier versions
                                                }
                                               })
        
        theAlertController.addAction(openSettingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default,
                                         handler: { (alertAction) in })
        
        theAlertController.addAction(cancelAction)
        
        self.theDelegate?.present(theAlertController, animated: true, completion: nil)
    }
}

extension CustomImagePicker: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        theDelegate?.dismiss(animated: true, completion: nil)
        
        if info[UIImagePickerControllerMediaURL] != nil, theImagePicked != nil {
            theImagePicked!([UIImagePickerControllerMediaURL: info[UIImagePickerControllerMediaURL] as AnyObject])
        }
        
        if info[UIImagePickerControllerOriginalImage] != nil, theImagePicked != nil {
            theImagePicked!([UIImagePickerControllerOriginalImage: info[UIImagePickerControllerOriginalImage] as AnyObject])
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        theDelegate?.dismiss(animated: true, completion: nil)
        
        if theImageCanceled != nil {
            theImageCanceled!()
        }
    }
}

extension CustomImagePicker: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {
        if theNavigationColor != nil {
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.barStyle = .blackOpaque
            navigationController.navigationBar.barTintColor = theNavigationColor!
            navigationController.navigationBar.tintColor = UIColor.white
        }
    }
}
