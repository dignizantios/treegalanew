//
//  ParentViewController.swift
//  Custom Classes
//
//  Created by pc
//  Copyright © 2018 dignizant. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

//import PKHUD
//import SwiftMessages
//import LinearProgressBarMaterial

class ParentViewController: UIViewController {

    // MARK: - Properties
    private lazy var arrTasks: [DispatchWorkItem] = {
        return []
    }()
    var progressView: UIProgressView?
    var timer: Timer?
//    var linearBar: LinearProgressBar?
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manageTabBarNormalImage()
        //self.tabBarController?.delegate = self
        
        //HUD.dimsBackground = false
       // HUD.allowsInteraction = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        for theTask: DispatchWorkItem in arrTasks {
            if !theTask.isCancelled {
                theTask.cancel()
            }
        }
        
        arrTasks.removeAll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("\(self) DEALLOCATED")
    }
    
    // MARK: - Status Bar Style
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK:- UI Methods
    private func manageTabBarNormalImage() {
        if self.tabBarController != nil {
            for theItem in self.tabBarController!.tabBar.items! {
                if theItem.title == "HOME" {
                    theItem.image = UIImage(named:"TabHome")?.withRenderingMode(.alwaysOriginal)
                    theItem.selectedImage = UIImage(named:"TabHome")?.withRenderingMode(.alwaysOriginal)
                } else if theItem.title == "EXPLORE" {
                    theItem.image = UIImage(named:"TabExplore")?.withRenderingMode(.alwaysOriginal)
                    theItem.selectedImage = UIImage(named:"TabExplore")?.withRenderingMode(.alwaysOriginal)
                } else if theItem.title == "DIP" {
                    theItem.image = UIImage(named:"TabDip")?.withRenderingMode(.alwaysOriginal)
                    theItem.selectedImage = UIImage(named:"TabDip")?.withRenderingMode(.alwaysOriginal)
                } else if theItem.title == "ACTIVITY" {
                    theItem.image = UIImage(named:"TabActivity")?.withRenderingMode(.alwaysOriginal)
                    theItem.selectedImage = UIImage(named:"TabActivity")?.withRenderingMode(.alwaysOriginal)
                } else if theItem.title == "PROFILE" {
                    theItem.image = UIImage(named:"TabProfile")?.withRenderingMode(.alwaysOriginal)
                    theItem.selectedImage = UIImage(named:"TabProfile")?.withRenderingMode(.alwaysOriginal)
                }
            }
        }
    }
    
    // MARK: - Other Methods
    func addDispatchWorkItem(workItem: DispatchWorkItem, after: Double) {
        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + after), execute: workItem)
        arrTasks.append(workItem)
    }
    
    func buttonTappedWithObject(theObject: Any) {
        
    }
    
    func setupDelegateAndDataSourceForCollectionView(theObject: UICollectionView) {
        
    }
    
    func setupDelegateAndDataSourceForTableView(theObject: UITableView) {
        
    }
    
    func loadSeeMoreController(theTag: Int) {
        
    }
    
    final func removeDispatchWorkItem(workItem: DispatchWorkItem) {
        if !workItem.isCancelled {
            workItem.cancel()
        }
        
        let isContaining = arrTasks.contains(where: { $0 === workItem })
        
        if isContaining {
            let theIndex = arrTasks.index(where: { $0 === workItem })
            arrTasks.remove(at: theIndex!)
        }
    }
    /*
    func showLoader()
    {
        /*progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.bar)
         progressView?.progressTintColor = UIColor.red
         
         progressView?.frame = CGRect(x: 0, y: 1, width: UIScreen.main.bounds.size.width+100, height: 10)
         timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(timerValueChanged), userInfo: nil, repeats: true)
         view.addSubview(progressView!)*/
        //configureLinearProgressBar()
        
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        configureLinearProgressBar()
        self.view.addSubview(linearBar!)        
        linearBar?.startAnimation()

    }
    func configureLinearProgressBar()
    {
        linearBar?.backgroundColor = UIColor.white
        linearBar?.progressBarColor = UIColor.appRedColor
        linearBar?.heightForLinearBar = 5
    }
    
    func hideLoader()
    {
        //progressView?.progress = 0.0
        //timer?.invalidate()
          linearBar?.stopAnimation()
    }
     
    @objc func timerValueChanged(){
        progressView?.progress += 0.10
        let progressValue:Float = (self.progressView?.progress)!
        print(progressValue)
        print("\(progressValue * 100) %")
        if(progressValue >= 1)
        {
            progressView?.progress = 0.0
        }
    }
    
    */
    /*
    // MARK: - IQKeyboardManager
    final func enableIQKeyboardManager() {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = true
    }
    
    final func disableIQKeyboardManager() {
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
    }
     */
    
    // MARK:- setupSideMenu
    func setupSideMenu(){
        
        //        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let leftViewController = SideMenuViewController.instantiateFromAppStoryboard(appStoryboard: .main)
        
        let HomeNavigation: UINavigationController = HomeNavigationController.instantiateFromAppStoryboard(appStoryboard: .home)
        //        leftViewController.mainViewController = HomeNavigation
        let slideMenuController = SlideMenuController(mainViewController: HomeNavigation, leftMenuViewController: leftViewController)
        slideMenuController.leftPanGesture?.isEnabled = false
        slideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width - 73)
        //nvc.navigationItem.hidesBackButton = true
        //nvc.interactivePopGestureRecognizer?.isEnabled = false
        
        SlideMenuOptions.contentViewScale = 1
        SlideMenuOptions.shadowRadius = 0.5
        SlideMenuOptions.shadowOpacity = 0.2
        SlideMenuOptions.contentViewOpacity = 0.4
        SlideMenuOptions.hideStatusBar = false
        AppDelegate.shared.window?.rootViewController = slideMenuController
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setSideMenuButton() {
        let menuBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menubar_white"), style: .plain, target: self, action: #selector(sideMenuOpen))
        self.navigationItem.leftBarButtonItem = menuBarButton
    }
    
}

extension UIViewController {
    
    /*
    func showPopup(strMessage: String) {
        let view: MessageView = MessageView.viewFromNib(layout: .cardView)
        view.configureContent(title: "", body:strMessage, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "", buttonTapHandler: { _ in SwiftMessages.hide() })
        let _: IconStyle = .default
        view.configureTheme(backgroundColor: UIColor.appRedColor, foregroundColor: UIColor.white, iconImage: nil, iconText: nil)
        view.button?.isHidden = true
        view.iconImageView?.isHidden = true
        view.iconLabel?.isHidden = true
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .bottom
        config.presentationContext = .window(windowLevel: UIWindowLevelNormal)
        config.duration = .seconds(seconds: 2)
        config.shouldAutorotate = true
        config.interactiveHide = true
        SwiftMessages.show(config: config, view: view)
    }
    */
    
    func showOKAlert(strMessage: String) {
        let actionOK = UIAlertAction(title: "OK", style: .default) { (theAction) in
        
        } //LanguageManager.sharedInstance.getTranslationForKey("OK", value: "OK")
        
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        theAlertController.addAction(actionOK)
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func showOKAlertWithCompletionBlock(strMessage: String, completionBlock: @escaping (Bool) -> Void) {
        let actionOK = UIAlertAction(title: "OK", style: .default) { (theAction) in
            completionBlock(true)
        } //LanguageManager.sharedInstance.getTranslationForKey("OK", value: "OK")
        
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        theAlertController.addAction(actionOK)
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    //success: @escaping (AnyObject!) -> Void
    func showYesOrNoAlert(strMessage: String, completionBlock: @escaping (Bool) -> Void) {
        let actionYes = UIAlertAction(title: "YES", style: .default) { (theAction) in
            completionBlock(true)
        }
        
        let actionNo = UIAlertAction(title: "NO", style: .default) { (theAction) in
            completionBlock(false)
        }
        
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        theAlertController.addAction(actionYes)
        theAlertController.addAction(actionNo)
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func showOKOrCancelAlert(strMessage: String, completionBlock: @escaping (Bool) -> Void) {
        let actionYes = UIAlertAction(title: "OK", style: .default) { (theAction) in
            completionBlock(true)
        }
        
        let actionNo = UIAlertAction(title: "Cancel", style: .default) { (theAction) in
            completionBlock(false)
        }
        
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        theAlertController.addAction(actionYes)
        theAlertController.addAction(actionNo)
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func showActionSheetWithOptions(strMessage: String, arrOptions: [String], completionBlock: @escaping (String) -> Void) {
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .actionSheet)
        
        for theValue in arrOptions {
            let theAction = UIAlertAction(title: theValue, style: .default) { (theAction) in
                completionBlock(theValue)
            }
            
            theAlertController.addAction(theAction)
        }
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptions(strMessage: String, arrOptions: [String], completionBlock: @escaping (String) -> Void) {
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        
        for theValue in arrOptions {
            let theAction = UIAlertAction(title: theValue, style: .default) { (theAction) in
                completionBlock(theValue)
            }
            
            theAlertController.addAction(theAction)
        }
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        var objectsToShare = [AnyObject]()
        
        if let url = url {
            objectsToShare.append(url as AnyObject)
        }
        if let image = image {
            objectsToShare.append(image as AnyObject)
        }
        if let msg = msg {
            objectsToShare.append(msg as AnyObject)
        }
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.modalPresentationStyle = .popover
        activityVC.popoverPresentationController?.sourceView = self.view
        if let sourceRect = sourceRect {
            activityVC.popoverPresentationController?.sourceRect = sourceRect
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    func showLocationPermission(strMessage: String, completionBlock: @escaping (Bool) -> Void) {
        
        let actionYes = UIAlertAction(title: "Cancel", style: .default) { (theAction) in
            completionBlock(false)
        }
        
        let actionNo = UIAlertAction(title: "Settings", style: .default) { (theAction) in
            completionBlock(true)
        }
        
        let theAlertController = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        theAlertController.addAction(actionYes)
        theAlertController.addAction(actionNo)
        
        self.present(theAlertController, animated: true, completion: nil)
    }
    
    func redirectToSettingForLocationEnable(toGPS:Bool) {
        let strMsg = toGPS ? "Location is not enabled. Do you want to go to settings menu?" : "You must allow location permission to know you current location and distance.\n\nif you deny before and alert are not showing for allow permission then follow below steps.\n\nTap Setting > Apps > Dipicious > Permissions, and turn on."
        let strUrl = toGPS ? "App-Prefs:root=Privacy&path=LOCATION" : UIApplicationOpenSettingsURLString
        
        self.showLocationPermission(strMessage: strMsg, completionBlock: { (enable) in
            if enable {
                if let url = URL(string: strUrl) {
                    // If general location settings are disabled then open general location settings
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        })
        
    }
    
}
/*
extension UIViewController {
    func showLoader() {
        DispatchQueue.main.async {
            HUD.show(.progress)
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            HUD.hide()
        }
    }
}
*/
extension UIViewController: UIScrollViewDelegate {
    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        return
        if velocity.y > 0 {
            //Code will work without the animation block. I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                
//                if self.tabBarController != nil {
//                    //self.navigationController?.setToolbarHidden(true, animated: true)
//                    self.changeTabBar(hidden: true, animated: true)
//                    self.view.layoutIfNeeded()
//                }
                print("Hide")
            }, completion: nil)
            
            if self.tabBarController != nil {
                //self.navigationController?.setToolbarHidden(true, animated: true)
                self.changeTabBar(hidden: true, animated: true)
                self.view.layoutIfNeeded()
            }
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                
//                if self.tabBarController != nil {
//                    //self.navigationController?.setToolbarHidden(false, animated: true)
//                    self.changeTabBar(hidden: false, animated: true)
//                    self.view.layoutIfNeeded()
//                }
                print("Unhide")
            }, completion: nil)
            
            if self.tabBarController != nil {
                //self.navigationController?.setToolbarHidden(false, animated: true)
                self.changeTabBar(hidden: false, animated: true)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func changeTabBar(hidden:Bool, animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = hidden
        
        // self.parent?.tabBarController?.hideTabBarAnimated(hide: hidden)
        
//        UIView.animate(withDuration: 0.1,
//                         animations: {
//                            if hidden {
//                                self.parent?.tabBarController?.tabBar.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 0)
//                            } else {
//                                self.parent?.tabBarController?.tabBar.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 49, width: UIScreen.main.bounds.size.width, height: 49)
//                            }
//        }, completion: { (isCompleted) in
//            self.parent?.tabBarController?.tabBar.isHidden = hidden
//        })
        
        
        //self.parent?.tabBarController?.tabBar.isHidden = hidden
        
        
//        var tabBar = self.tabBarController?.tabBar
//        if tabBar!.isHidden == hidden{ return }
//        let frame = tabBar?.frame
//        let offset = (hidden ? (frame?.size.height)! : -(frame?.size.height)!)
//        let duration: TimeInterval = (animated ? 0.3 : 0.0)
//        tabBar?.isHidden = false
//        if frame != nil
//        {
//            //CGRectOffset(frame!, 0, offset)
//            UIView.animate(withDuration: duration,
//                           animations: {tabBar!.frame = (frame?.offsetBy(dx: 0, dy: offset))! },
//                                       completion: {
//                                        print($0)
//                                        if $0 {tabBar?.isHidden = hidden}
//            })
//        }
    }
}
/*
extension UITabBarController {
    func hideTabBarAnimated(hide:Bool) {
        UIView.animate(withDuration: 0.3, animations: {
            if hide {
                self.tabBar.transform = CGAffineTransform(translationX: 0, y: 50)
            } else {
                self.tabBar.transform = CGAffineTransform.identity
            }
        })
    }
}

extension ParentViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if (viewController as? UINavigationController)?.topViewController != nil {
            if ((viewController as? UINavigationController)?.topViewController as? HomeViewController) != nil {
                ((viewController as? UINavigationController)?.topViewController as? HomeViewController)?.scrollToTop()
            } else if ((viewController as? UINavigationController)?.topViewController as? ExploreViewController) != nil {
                ((viewController as? UINavigationController)?.topViewController as? ExploreViewController)?.scrollToTop()
            }
        }
    }
}
*/





